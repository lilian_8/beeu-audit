<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditHistory extends Model
{
    public $timestamps  = true;

	protected $table    = 'audit_histories';

	protected $fillable = ['name_company','init_date','finish_date','sector','norm','roll','user_id'];   

	protected $guarded  = ['id'];


}
