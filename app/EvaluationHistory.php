<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluationHistory extends Model
{
    protected $table    = 'evaluation_histories';
	
	protected $fillable = ['company','date','type','period','qualification','evaluation','client_qualification','user_id','qualificationfile','client_qualificationfile'];

	public $timestamps  = true;
}
