<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
     
class Audit extends Model
{
    public $timestamps  = true;

	protected $table    = 'audits';

	protected $fillable = ['type','init_date','finish_date','status','certification_id','finished','expedient','end_expedient'];   

	protected $guarded  = ['id'];

	public function teams()
	{
		return $this->hasMany('App\AuditTeam');
  }
  public function auditsector()
  {
	return $this->hasMany('App\AuditSectorNorm','audit_id','id');
  }
  public function sections()
  {
		return $this->hasMany('App\ResultSection');
  }
  public function certification()
  {
    return $this->hasOne('App\Certification','id','certification_id');
  }
  public function statuscheck()
  {
    return $this->hasMany('App\StatusChecklist','audit_id','id');
  }
  public function certificates()
  {
    return $this->hasMany('App\Certificate','audit_id','id');
  }

}
