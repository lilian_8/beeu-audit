<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultSection extends Model
{
    public $timestamps  = true;

	protected $table    = 'result_section';

	protected $fillable = ['audit_sector_norm_id','observation','code','description','audit_id'];   

	protected $guarded  = ['id'];

	public function resultChecklist()
	{
  		return $this->hasMany('App\ResultChecklist');
    }
	
	public function sectorNorm()
	{
		return $this->belongsTo('App\AuditSectorNorm','audit_sector_norm_id','id');
	}
}
