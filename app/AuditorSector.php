<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditorSector extends Model
{
    public $timestamps  = true;

	protected $table    = 'auditor_sectors';

	protected $fillable = ['company_sector_id','user_id','sector','norma'];   

	protected $guarded  = ['id'];

	public function company()
	{
  		return $this->belongsTo('App\CompanySector','company_sector_id');
    }

}
