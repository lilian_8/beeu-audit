<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanySector;
use App\Organization;
use App\Direction;
use App\OrganizationSectorNorm;
use App\Sector;
use App\Norm;
use Auth;

class OrganizationController extends Controller
{
   public function index(Request $request)
   {

        if (!in_array(Auth::user()->profile, ['Administrador', 'Comercial','Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }
        $companysectors  = CompanySector::all();
        $sectors         = Sector::all();
        $norms           = Norm::all();

        $nombre          = $request->get('search');
        $sector          = $request->get('sectors');
        $norm            = $request->get('norms');
        $organizations   = Organization::where('id', '>', 0);
        
        if($nombre){
            $organizations->Where('name','like',"%$nombre%")->orWhere('rfc','like',"%$nombre%")->orWhere('code','like',"%$nombre%");
        }

        if($sector){
            $organizations->whereHas('organizationSectorNorm',function($q) use($sector) {
                $q->whereHas('companies', function($query) use($sector) {
                    $query->Where('sector_id',$sector);
                });
            });
        }


        if($norm){
            $organizations->whereHas('organizationSectorNorm',function($q) use($norm) {
                $q->whereHas('companies', function($query) use($norm) {
                    $query->Where('norm_id',$norm);
                });
            });
        }

        $organizations = $organizations->paginate(10);


        return view('organization.index',compact('companysectors','organizations','sectors','norms'));
   }
   public function store(Request $request)
   {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Comercial','Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }
        $validate=$this->validate(request(),[
            'calle'         =>'required',
            'exterior'      =>'required',
            'email'         =>'required',
            'cp'            =>'required',
            'colonia'       =>'required',
            'ciudad'        =>'required',
            'estado'        =>'required',
            'municipio'     =>'required',
            'pais'          =>'required',
            'contacto'      =>'required',
            'puesto'        =>'required',
            'email'         =>'required',
            'celular'       =>'required',
            'sitios'        =>'required',
            'empleados'     =>'required',
            'sector'        =>'required',
            'norma'         =>'required',
            'codigo'        =>'required',
        ]);
        $invoiceId = '';

        if($request['invoice']==1){

            $direction=Direction::Create([
                'street'          =>$request['calle'],
                'internal_number' =>$request['interior'],
                'external_number' =>$request['exterior'],
                'zip_code'        =>$request['cp'],
                'suburb'          =>$request['colonia'],
                'city'            =>$request['ciudad'],
                'state'           =>$request['estado'],
                'municipality'    =>$request['municipio'],
                'country'         =>$request['pais'],
                'reference'       =>$request['reference'],
                'phone'           =>$request['celular'],
                'extension'       =>$request['extension'],
                'email'           =>$request['email'],
                'contact_name'    =>$request['contacto'],
                'contact_job'     =>$request['puesto'],

            ]); 
            $company=Organization::create([
                'code'                     =>$request['codigo'],
                'name'                     =>$request['nombre'],
                'rfc'                      =>$request['rfc'],
                'sites'                    =>$request['sitios'],
                'employees'                =>$request['empleados'],
                'certification_address_id' =>$direction->id,
                'invoice_address_id'       =>$direction->id,
            ]);

            if($request->norma  && $request->sector){
                for ($i=0; $i < count($request['norma']) ; $i++) { 
                    for ($x=0; $x < count( $request['sector']); $x++) { 
                        $companysector = CompanySector::where('sector_id',$request['sector'][$x])->where('norm_id',$request['norma'][$i])->first();

                        if($companysector){
                            $normandsectors=OrganizationSectorNorm::create([
                                'company_sector_id' => $companysector->id,
                                'organization_id'   => $company->id,
                            ]);
                        }else{
                            $companysector = CompanySector::create([
                                'sector_id'     =>$request['sector'][$x],
                                'norm_id'       =>$request['norma'][$i],
                                'companies_id'  =>1
                            ]);

                            $normandsectors=OrganizationSectorNorm::create([
                                'company_sector_id' => $companysector->id,
                                'organization_id'   => $company->id,
                            ]);
                        }

                    }
                }
            }

        }else{

            $direction=Direction::Create([
                'street'          =>$request['calle'],
                'internal_number' =>$request['interior'],
                'external_number' =>$request['exterior'],
                'zip_code'        =>$request['cp'],
                'suburb'          =>$request['colonia'],
                'city'            =>$request['ciudad'],
                'state'           =>$request['estado'],
                'municipality'    =>$request['municipio'],
                'country'         =>$request['pais'],
                'reference'       =>$request['reference'],
                'phone'           =>$request['celular'],
                'extension'       =>$request['extension'],
                'email'           =>$request['email'],
                'contact_name'    =>$request['contacto'],
                'contact_job'     =>$request['puesto'],
            ]); 
            $directionInvoice=Direction::Create([
                'street'          =>$request['steets'],
                'internal_number' =>$request['numberInts'],
                'external_number' =>$request['numberOuts'],
                'zip_code'        =>$request['cps'],
                'suburb'          =>$request['suburbs'],
                'city'            =>$request['citys'],
                'state'           =>$request['states'],
                'municipality'    =>$request['municipalitys'],
                'country'         =>$request['countrys'],
                'reference'       =>$request['references'],
                'phone'           =>$request['celular'],
                'extension'       =>$request['extension'],
                'email'           =>$request['email'],
                'contact_name'    =>$request['contacto'],
                'contact_job'     =>$request['puesto'],
            ]); 
            $company=Organization::create([
                'name'                     =>$request['nombre'],
                'rfc'                      =>$request['rfc'],
                'sites'                    =>$request['sitios'],
                'employees'                =>$request['empleados'],
                'certification_address_id' =>$direction->id,
                'invoice_address_id'       =>$directionInvoice->id,
            ]);


            if($request->companynorm  && $request->companysector){
                for ($i=0; $i < count($request['companynorm']) ; $i++) { 
                    for ($x=0; $x < count( $request['companysector']); $x++) { 
                        $companysector = CompanySector::where('sector_id',$request['companysector'][$x])->where('norm_id',$request['companynorm'][$i])->first();

                        if($companysector){
                            $normandsectors=OrganizationSectorNorm::create([
                                'company_sector_id' => $companysector->id,
                                'organization_id'   => $company->id,
                            ]);
                        }else{
                            $companysector = CompanySector::create([
                                'sector_id'     =>$request['companysector'][$x],
                                'norm_id'       =>$request['companynorm'][$i],
                                'companies_id'  =>1
                            ]);

                            $normandsectors=OrganizationSectorNorm::create([
                                'company_sector_id' => $companysector->id,
                                'organization_id'   => $company->id,
                            ]);
                        }

                    }
                }
            }
        }



        if($company){
            return back()->with('success',"Dato guardado");
        }
        else{
            return back()->with('error',"Ocurrio un error");
        }
   }
   public function edit(Request $request,$id)
   {    
        if (!in_array(Auth::user()->profile, ['Administrador', 'Comercial','Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }
        $companysectors      = CompanySector::all();
        $organization        = Organization::Where('id',$id)->get();
        $organizationSectors = OrganizationSectorNorm::Where('organization_id',$id)->get();
        $sectors             = Sector::all();
        $norms               = Norm::all();
        $sectores            = [];
        $normas              = [];
        $buscadorsector      = [];
        $buscadornorma       = [];
        $sectorIds           = [];
        $normIds             = [];

        foreach ($organizationSectors as $organizationSector) {
            if($organizationSector->companies->sector_id && $organizationSector->companies->norm_id){
                $sectores[$organizationSector->companies->sector_id]=$organizationSector->companies->sector_id;
                $normas[$organizationSector->companies->norm_id]=$organizationSector->companies->norm_id;
            }
        }


        foreach ($sectores as $sector) {
            $buscadorsector[$sector] = Sector::find($sector);
            $sectorIds[]=$buscadorsector[$sector]->id;
        }

        foreach ($normas as $norma) {
           $buscadornorma[$norma] =  Norm::find($norma);
           $normIds[]=$buscadornorma[$norma]->id;
        }

        return view('organization.editorganization',compact('organization','organizationSectors','companysectors','buscadorsector','sectorIds','buscadornorma','normIds','sectors','norms'));        
   }

   public function update(Request $request)
   {

        if (!in_array(Auth::user()->profile, ['Administrador', 'Comercial','Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }

        $company=Organization::find($request['id']);
        $company->update([
            'code'               =>$request['codigo'],
            'name'               =>$request['name'],
            'rfc'                =>$request['rfc'],
            'sites'              =>$request['sitios'],
            'employees'          =>$request['empleados'],
        ]);

        if($request['invoice']==1){
            $direction = Direction::find($request['idcertification']);
            $direction->update([
                'street'          =>$request['steet'],
                'internal_number' =>$request['numberInt'],
                'external_number' =>$request['numberOut'],
                'zip_code'        =>$request['cp'],
                'suburb'          =>$request['suburb'],
                'city'            =>$request['city'],
                'state'           =>$request['state'],
                'municipality'    =>$request['municipality'],
                'country'         =>$request['country'],
                'reference'       =>$request['reference'],
                'phone'           =>$request['phone'],
                'extension'       =>$request['extension'],
                'email'           =>$request['email'],
                'contact_name'    =>$request['contact_name'],
                'contact_job'     =>$request['contact_job'],

            ]);
        }else{

            $direction = Direction::find($request['idcertification']);
            $direction->update([
                'street'          =>$request['steet'],
                'internal_number' =>$request['numberInt'],
                'external_number' =>$request['numberOut'],
                'zip_code'        =>$request['cp'],
                'suburb'          =>$request['suburb'],
                'city'            =>$request['city'],
                'state'           =>$request['state'],
                'municipality'    =>$request['municipality'],
                'country'         =>$request['country'],
                'reference'       =>$request['reference'],
                'phone'           =>$request['phone'],
                'extension'       =>$request['extension'],
                'email'           =>$request['email'],
                'contact_name'    =>$request['contact_name'],
                'contact_job'     =>$request['contact_job'],

            ]);

            $directionInvoice=Direction::Create([
                'street'          =>$request['steets'],
                'internal_number' =>$request['numberInts'],
                'external_number' =>$request['numberOuts'],
                'zip_code'        =>$request['cps'],
                'suburb'          =>$request['suburbs'],
                'city'            =>$request['citys'],
                'state'           =>$request['states'],
                'municipality'    =>$request['municipalitys'],
                'country'         =>$request['countrys'],
                'reference'       =>$request['references'],
                'phone'           =>$request['celular'],
                'extension'       =>$request['extension'],
                'email'           =>$request['email'],
                'contact_name'    =>$request['contacto'],
                'contact_job'     =>$request['puesto'],
            ]); 
            $company->update([
                'invoice_address_id' =>$directionInvoice->id
            ]);

        }
        $idorganization=$request['id'];

        
        if($request->companysector && $request->companynorm){
            for ($i=0; $i < count($request['companysector']) ; $i++) { 
                for ($x=0; $x < count( $request['companynorm']); $x++) { 
                    $companysector = CompanySector::where('sector_id',$request['companysector'][$i])->where('norm_id',$request['companynorm'][$x])->first();

                    if($companysector){
                        $normandsectors=OrganizationSectorNorm::create([
                            'company_sector_id' => $companysector->id,
                            'organization_id'   => $company->id,
                        ]);
                    }else{
                        $companysector = CompanySector::create([
                            'sector_id'     =>$request['companynorm'][$x],
                            'norm_id'       =>$request['companysector'][$i],
                            'companies_id'  =>1
                        ]);

                        $normandsectors=OrganizationSectorNorm::create([
                            'company_sector_id' => $companysector->id,
                            'organization_id'   => $company->id,
                        ]);
                    }
                }
            }
        }elseif($request->companysector && $request->companynormSector ){
            for ($i=0; $i < count($request['companynormSector']) ; $i++) { 
                for ($x=0; $x < count( $request['companysector']); $x++) { 
                    $companysector = CompanySector::where('sector_id',$request['companysector'][$x])->where('norm_id',$request['companynormSector'][$i])->first();

                    if($companysector){
                        $normandsectors=OrganizationSectorNorm::create([
                            'company_sector_id' => $companysector->id,
                            'organization_id'   => $idorganization,
                        ]);
                    }else{
                        $companysector = CompanySector::create([
                            'sector_id'     =>$request['companynormSector'][$x],
                            'norm_id'       =>$request['companysector'][$i],
                            'companies_id'  =>1
                        ]);

                        $normandsectors=OrganizationSectorNorm::create([
                            'company_sector_id' => $companysector->id,
                            'organization_id'   => $idorganization,
                        ]);
                    }
                }
            }
        }elseif($request->companynorm && $request->companysectorNorm){
            for ($i=0; $i < count($request['companynorm']) ; $i++) { 
                for ($x=0; $x < count( $request['companysectorNorm']); $x++) { 
                    $companysector = CompanySector::where('sector_id',$request['companysectorNorm'][$x])->where('norm_id',$request['companynorm'][$i])->first();

                    if($companysector){
                        $normandsectors=OrganizationSectorNorm::create([
                            'company_sector_id' => $companysector->id,
                            'organization_id'   => $idorganization,
                        ]);
                    }else{
                        $companysector = CompanySector::create([
                            'sector_id'     =>$request['companysectorNorm'][$x],
                            'norm_id'       =>$request['companynorm'][$i],
                            'companies_id'  =>1
                        ]);

                        $normandsectors=OrganizationSectorNorm::create([
                            'company_sector_id' => $companysector->id,
                            'organization_id'   => $idorganization,
                        ]);
                    }
                }
            }
        }


        if($direction){
            return back()->with('updates',"Dato guardado");
        }
        else{
            return back()->with('error',"Ocurrio un error");
        }
   }

   public function sectorUpdate(Request $request)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Comercial','Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }
        if($request->companysector){

            $idorganization=$request['id'];
            for ($i = 0; $i < count($request->companysector); $i++) {
                $normandsectors=OrganizationSectorNorm::create([
                    'company_sector_id' => $request['companysector'][$i],
                    'organization_id'   => $idorganization,

                ]);
            }
            
            if($normandsectors){
                return back()->with('updates',"Dato guardado");
            }
            else{
                return back()->with('error',"Ocurrio un error");
            }
        }else{
            return back()->with('updates',"Dato guardado");
        }
    }

    public function normUpdate(Request $request)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Comercial','Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }

        $idorganization=$request['id'];

        if($request->companynorm  && $request->companysector){
            for ($i=0; $i < count($request['companynorm']) ; $i++) { 
                for ($x=0; $x < count( $request['companysector']); $x++) { 
                    $companysector = CompanySector::where('sector_id',$request['companysector'][$x])->where('norm_id',$request['companynorm'][$i])->first();

                    if($companysector){
                        $normandsectors=OrganizationSectorNorm::create([
                            'company_sector_id' => $companysector->id,
                            'organization_id'   => $idorganization,
                        ]);
                    }else{
                        $companysector = CompanySector::create([
                            'sector_id'     =>$request['companysector'][$x],
                            'norm_id'       =>$request['companynorm'][$i],
                            'companies_id'  =>1
                        ]);

                        $normandsectors=OrganizationSectorNorm::create([
                            'company_sector_id' => $companysector->id,
                            'organization_id'   => $idorganization,
                        ]);
                    }

                }
            }
        }
            
        return back()->with('updates',"Datos guardados");
    }

   public function delete(Request $request)
   {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Comercial','Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }
        $orgaanization=Organization::find($request['id']);

        try { 
            $orgaanization->delete();
            return back()->with('delete',"Dato eliminado");
        
        } catch (\Illuminate\Database\QueryException $e) { 
            if($e->getCode() == "23000"){
            return back()->with('dangerdelete',"Dato no eliminado ");
        }
         
        }

   }
    public function deleteSectorOrganization(Request $request)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Comercial','Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }
        $organizarionSector = OrganizationSectorNorm::where('organization_id',$request['organization_id'])->get();

        foreach ($organizarionSector as $sectorcompany) {
            if($sectorcompany->companies->sector_id==$request['id']){
                $eliminar = OrganizationSectorNorm::find($sectorcompany->id);
                $eliminar->delete();
            }
        }
        
        return back()->with('delete',"Dato guardado");
    }

    public function deleteNormOrganization(Request $request)
    {
        if (!in_array(Auth::user()->profile, ['Administrador', 'Comercial','Planeador'])) {
            return redirect()->route('home')->with('permisos',"No tienes permisos para ingrear a esta opción");
        }
        $organizarionSector = OrganizationSectorNorm::where('organization_id',$request['organization_id'])->get();

        foreach ($organizarionSector as $sectorcompany) {
            if($sectorcompany->companies->norm_id==$request['id']){
                $eliminar = OrganizationSectorNorm::find($sectorcompany->id);
                $eliminar->delete();
            }
        }
        
        return back()->with('delete',"Dato guardado");

    }
}
