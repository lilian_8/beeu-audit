<?php

namespace App\Classes;

use Illuminate\Database\Eloquent\Model;
use App\SectionChecklist;
use App\Checklist;

class Norm45 extends Model
{
	public static function check($id)
    {
	    $section4  = SectionChecklist::where('code','4')->get()->first();
		$section5  = SectionChecklist::where('code','5')->get()->first();
		$section6  = SectionChecklist::where('code','6')->get()->first();
		$section7  = SectionChecklist::where('code','7')->get()->first();
		$section8  = SectionChecklist::where('code','8')->get()->first();
		$section9  = SectionChecklist::where('code','9')->get()->first();
		$section10 = SectionChecklist::where('code','10')->get()->first();	

		$checklist = Checklist::create([
		    'code'                 => '4.1',
			'description'          => 'Comprensión de la organización y de su contexto',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section4->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización ha determinado las cuestiones externas e internas que son pertinentes para su propósito y que afectan a su capacidad para alcanzar los resultados previstos de su sistema de gestión de la SST?',
		]);


		$checklist = Checklist::create([
		    'code'                 => '4.2',
			'description'          => 'Comprensión de las necesidades y expectativas de los trabajadores y de otras partes interesadas',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section4->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => ' ¿La organización ha determinado las otras partes interesadas, las necesidades y expectativas pertinentes de los trabajadores y de otras partes interesadas y cuáles de estas necesidades y expectativas son, o podrían convertirse, en requisitos legales y otros requisitos?',
		]);
		$checklist = Checklist::create([
		    'code'                 => '4.3',
			'description'          => 'Determinación del alcance del sistema de gestión de la SST',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section4->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización ha determinado los límites y la aplicabilidad del sistema de gestión de la SST para establecer su alcance?, teniendo en cuenta las actividades relacionadas con el trabajo, planificadas o realizadas?',
		]);
		$checklist = Checklist::create([
		    'code'                 => '4.4',
			'description'          => 'Sistema de gestión de la SST',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section4->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización establece, implementa, mantiene y mejora continuamente su sistema de gestión de la SST, incluidos los procesos necesarios y sus interacciones, de acuerdo con los requisitos de la norma ISO 45001:2018?',
		]);
		// -----------------------------------------------------
		$checklist = Checklist::create([
		    'code'                 => '5.1',
			'description'          => 'Liderazgo y compromiso',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section5->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La alta dirección ha demostrado liderazgo y compromiso con respecto al sistema de gestión de la SST?:                                                                              
			a) asumiendo la total responsabilidad y rendición de cuentas para la prevención de las lesiones y el deterioro de la salud relacionados con el trabajo, así como la provisión de actividades y lugares de trabajo seguros y saludables.                                       
			b) asegurándose de que se establezcan la política de la SST y los objetivos relacionados de la SST y sean compatibles con la dirección estratégica de la organización.                                                                    
			c) asegurándose de la integración de los requisitos del sistema de gestión de la SST en los procesos de negocio de la organización.                                        
			d) asegurándose de que los recursos necesarios para establecer, implementar, mantener y mejorar el sistema de gestión de la SST estén disponibles.                                                                          
			e) comunicando la importancia de una gestión de la SST eficaz y conforme con los requisitos del sistema de gestión de la SST.                                         
			f) asegurándose de que el sistema de gestión de la SST alcance los resultados previstos.                       
			g) dirigiendo y apoyando a las personas, para contribuir a la eficacia del sistema de gestión de la SST.                                                                                            
			h) asegurando y promoviendo la mejora continua. i) apoyando otros roles pertinentes de la dirección, para demostrar su liderazgo aplicado a sus áreas de responsabilidad.                                                       
			j) desarrollando, liderando y promoviendo una cultura en la organización que apoye los resultados previstos del sistema de gestión de la SST.                                                                                           
			k) protegiendo a los trabajadores de represalias al informar de incidentes, peligros, riesgos y oportunidades',
		]);
		$checklist = Checklist::create([
		    'code'                 => '5.2',
			'description'          => 'Política de la SST',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section5->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La alta dirección comunica, documenta, establece, implementa y mantiene una política de la SST que?:                                                                      
			 a) incluya un compromiso para proporcionar condiciones de trabajo seguras y saludables para la prevención de lesiones y deterioro de la salud relacionados con el trabajo y que sea apropiada al propósito, tamaño y contexto de la organización y a la naturaleza específica de sus riesgos para la SST y sus oportunidades para la SST;                                                                                           
			 b) proporcione un marco de referencia para el establecimiento de los objetivos de la SST;                
			 c) incluya un compromiso para cumplir los requisitos legales y otros requisitos;                         
			 d) incluya un compromiso para eliminar los peligros y reducir los riesgos para la SST;                 
			 e) incluya un compromiso para la mejora continua del sistema de gestión de la SST;                                  
			 f) incluya un compromiso para la consulta y la participación de los trabajadores, y cuando existan, de los representantes de los trabajadores.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '5.3',
			'description'          => 'Roles, responsabilidades y autoridades en la organización',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section5->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La alta dirección se asegura de que las responsabilidades y autoridades para los roles pertinentes dentro del sistema de gestión de la SST se asignen y comuniquen a todos los niveles dentro de la organización, y se mantengan como información documentada. Los trabajadores en cada nivel de la organización deben asumir la responsabilidad de aquellos aspectos del sistema de gestión de la SST sobre los que tengan control?',
		]);
		$checklist = Checklist::create([
		    'code'                 => '5.4',
			'description'          => 'Consulta y participación de los trabajadores',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section5->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización establece, implementa y mantiene procesos para la consulta y la participación de los trabajadores a todos los niveles y funciones aplicables, y cuando existan, de los representantes de los trabajadores en el desarrollo, la planificación, la implementación, la evaluación del desempeño y las acciones para la mejora del sistema de gestión de la SST y proporciona los mecanismos, el tiempo, la formación y los recursos necesarios para la consulta y la participación?',
		]);

		// -----------------------------------------------------
		$checklist = Checklist::create([
		    'code'                 => '6.1.1',
			'description'          => 'Generalidades  Al planificar el sistema de gestión de la SST',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿la organización considera las cuestiones referidas en el apartado 4.1 (contexto), los requisitos referidos en los apartados 4.2 (partes interesadas) y 4.3 (el alcance de su sistema de gestión de la SST) y determina los riesgos y oportunidades?, que es necesario abordar con el fin de:
			a) asegurar que el sistema de gestión de la SST pueda alcanzar sus resultados previstos;
			b) prevenir o reducir efectos no deseados;
			c) lograr la mejora continua.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '6.1.2.1',
			'description'          => 'Identificación de peligros',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización establece, implementa y mantiene procesos de identificación continua y proactiva de los peligros? Los procesos deben tener en cuenta, pero no limitarse a:                                                       a) cómo se organiza el trabajo, los factores sociales [incluyendo la carga de trabajo, horas de trabajo, victimización y acoso (bullying) e intimidación], el liderazgo y la cultura de la organización;                                                                  b) las actividades y las situaciones rutinarias y no rutinarias, incluyendo los peligros que surjan de:
				1) la infraestructura, los equipos, los materiales, las sustancias y las condiciones físicas del lugar
				de trabajo; 2) el diseño de productos y servicios, la investigación, el desarrollo, los ensayos, la producción, el montaje, la construcción, la prestación de servicios, el mantenimiento y la disposición; 3) los factores humanos; 4) cómo se realiza el trabajo;                                                           c) los incidentes pasados pertinentes internos o externos a la organización, incluyendo emergencias, y sus causas;
				d) las situaciones de emergencia potenciales;
				e) las personas, incluyendo la consideración de:
				1) aquéllas con acceso al lugar de trabajo y sus actividades, incluyendo trabajadores, contratistas,
				visitantes y otras personas; 2) aquéllas en las inmediaciones del lugar de trabajo que pueden verse afectadas por las actividades de la organización; 3) los trabajadores en una ubicación que no está bajo el control directo de la organización;
				f) otras cuestiones, incluyendo la consideración de: 1) el diseño de las áreas de trabajo, los procesos, las instalaciones, la maquinaria equipos, los procedimientos operativos y la organización del trabajo, incluyendo su adaptación a las necesidades y capacidades de los trabajadores involucrados; 2) las situaciones que ocurren en las inmediaciones del lugar de trabajo causadas por actividades relacionadas con el trabajo bajo el control de la organización; 3) las situaciones no controladas por la organización y que ocurren en las inmediaciones del lugar
				de trabajo que pueden causar lesiones y deterioro de la salud a personas en el lugar de trabajo;
				g) los cambios reales o propuestos en la organización, operaciones, procesos, actividades y el sistema de gestión de la SST (véase 8.1.3);
				h) los cambios en el conocimiento y la información sobre los peligros.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '6.1.2.2',
			'description'          => 'Evaluación de los riesgos para la SST y otros riesgos para el sistema de gestión de la SST',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => ' La organización establece, implementa y mantiene procesos para:
			a) evaluar los riesgos para la SST a partir de los peligros identificados, teniendo en cuenta la eficacia de los controles existentes;
			b) determinar y evaluar los otros riesgos relacionados con el establecimiento, implementación, operación y mantenimiento del sistema de gestión de la SST.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '6.1.2.3',
			'description'          => 'Evaluación de los riesgos para la SST y otros riesgos para el sistema de gestión de la SST',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización establece, implementa y mantiene procesos para evaluar?:                                                 a) las oportunidades para la SST que permitan mejorar el desempeño de la SST, teniendo en cuenta los cambios planificados en la organización, sus políticas, sus procesos o sus actividades, y:
				1) las oportunidades para adaptar el trabajo, la organización del trabajo y el ambiente de trabajo alos trabajadores;
				2) las oportunidades de eliminar los peligros y reducir los riesgos para la SST;
				b) otras oportunidades para mejorar el sistema de gestión de la SST.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '6.1.3',
			'description'          => 'Determinación de los requisitos legales y otros requisitos.',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización establece, implementa y mantiene procesos para?:
				a) determinar y tener acceso a los requisitos legales y otros requisitos actualizados que sean
				aplicables a sus peligros, sus riesgos para la SST y su sistema de gestión de la SST;
				b) determinar cómo estos requisitos legales y otros requisitos aplican a la organización y qué necesita comunicarse;
				c) tener en cuenta estos requisitos legales y otros requisitos al establecer, implementar, mantener y
				mejorar de manera continua su sistema de gestión de la SST',
		]);
		$checklist = Checklist::create([
		    'code'                 => '6.1.4',
			'description'          => 'Planificación de las acciones ',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización planifica?:
				a) las acciones para:
				1) abordar estos riesgos y oportunidades;
				2) abordar los requisitos legales y otros requisitos
				3) prepararse y responder ante situaciones de emergencia;                                                                     
				b) la manera de:
				1) integrar e implementar las acciones en sus procesos del sistema de gestión de la SST o en otrosprocesos de negocio;
				2) evaluar la eficacia de estas acciones.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '6.2.1',
			'description'          => 'Objetivos de la SST',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización establece objetivos de la SST para las funciones y niveles pertinentes para
			mantener y mejorar continuamente el sistema de gestión de la SST y el desempeño de la SST y los objetivos cumplen con?:
			a) ser coherentes con la política de la SST;
			b) ser medibles (si es posible) o evaluables en términos de desempeño;
			c) tener en cuenta:
			1) los requisitos aplicables;
			2) los resultados de la evaluación de los riesgos y oportunidades;
			3) los resultados de la consulta con los trabajadores y, cuando existan, con los
			representantes de los trabajadores;
			d) ser objeto de seguimiento;
			e) comunicarse;
			f) actualizarse, según sea apropiado.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '6.2.2',
			'description'          => 'Planificación para lograr objetivos de la SST',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => ' Al planificar cómo lograr sus objetivos de la SST, ¿la organización ha determinado?:
				a) qué se va a hacer;
				b) qué recursos se requerirán;
				c) quién será responsable;
				d) cuándo se finalizará;                                                   
				e) cómo se evaluarán los resultados, incluyendo los indicadores de seguimiento;
				f) cómo se integrarán las acciones para lograr los objetivos de la SST en los procesos de negocio de la organización.',
		]);

		// ------------------------------------------------------
		$checklist = Checklist::create([
		    'code'                 => '7.1',
			'description'          => 'Recursos',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización determina y proporciona los recursos necesarios para el establecimiento, implementación, mantenimiento y mejora continua del sistema de gestión de la SST?',
		]);
		$checklist = Checklist::create([
		    'code'                 => '7.2',
			'description'          => '¿La organización?:',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => 'a) Determina la competencia necesaria de los trabajadores que afecta o puede afectar a su desempeño de la SST;
				b) asegura de que los trabajadores sean competentes (incluyendo la capacidad de identificar los peligros), basándose en la educación, formación o experiencia apropiadas;
				c) cuando sea aplicable, toma acciones para adquirir y mantener la competencia necesaria y evaluar la eficacia de las acciones tomadas;
				d) conserva la información documentada apropiada, como evidencia de la competencia.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '7.3',
			'description'          => 'Toma de conciencia',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Los trabajadores son sensibilizados sobre y tienen conciencia de?:
			a) la política de la SST y los objetivos de la SST;
			b) su contribución a la eficacia del sistema de gestión de la SST, incluidos los beneficios de una mejora del desempeño de la SST;
			c) las implicaciones y las consecuencias potenciales de no cumplir los requisitos del sistema de gestión de la SST;
			d) los incidentes, y los resultados de investigaciones, que sean pertinentes para ellos;
			e) los peligros, los riesgos para la SST y las acciones determinadas, que sean pertinentes para ellos;
			f) la capacidad de alejarse de situaciones de trabajo que consideren que presentan un peligro inminente y serio para su vida o su salud, así como las disposiciones para protegerles de las consecuencias indebidas de hacerlo.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '7.4.1',
			'description'          => 'Generalidades',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización establece, implementa y mantiene los procesos necesarios para las comunicaciones internas y externas pertinentes al sistema de gestión de la SST, incluyendo la determinación de?:
				a) qué comunicar;
				b) cuándo comunicar;
				c) a quién comunicar:
				1) internamente entre los diversos niveles y funciones de la organización;
				2) entre contratistas y visitantes al lugar de trabajo;
				3) entre otras partes interesadas;
				d) cómo comunicar.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '7.4.2',
			'description'          => 'Comunicación interna',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización comunica internamente la información pertinente para el sistema de gestión de la SST entre los diversos niveles y funciones de la organización, incluyendo los cambios en el sistema de gestión de la SST, según sea apropiado y asegura de que sus procesos de comunicación permitan a los trabajadores contribuir a la mejora continua?',
		]);
		$checklist = Checklist::create([
		    'code'                 => '7.4.3',
			'description'          => 'Comunicación externa',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización comunica externamente la información pertinente para el sistema de gestión de la SST, según se establece en los procesos de comunicación de la organización y teniendo en cuenta sus requisitos legales y otros requisitos?',
		]);
		$checklist = Checklist::create([
		    'code'                 => '7.5.1',
			'description'          => 'Generalidades',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿El sistema de gestión de la SST de la organización incluye?:
				a) la información documentada requerida por este documento;
				b) la información documentada que la organización determina como necesaria para la eficacia del sistema de gestión de la SST.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '7.5.2',
			'description'          => 'Creación y actualización ',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => 'Al crear y actualizar la información documentada, la organización debe asegurarse de que lo siguiente sea apropiado:
				a) la identificación y descripción (por ejemplo, título, fecha, autor o número de referencia);
				b) el formato (por ejemplo, idioma, versión del software, gráficos) y los medios de soporte (por ejemplo, papel, electrónico);
				c) la revisión y aprobación con respecto a la conveniencia y adecuación.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '7.5.3',
			'description'          => 'Control de la información documentada',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => 'La información documentada requerida por el sistema de gestión de la SST y por la norma ISO 45001:2018 ¿se encuentra controlada para asegurarse de que?:
				a) esté disponible y sea idónea para su uso, dónde y cuándo se necesite;
				b) esté protegida adecuadamente (por ejemplo, contra pérdida de la confidencialidad, uso inadecuado, o pérdida de integridad).',
		]);
		// -----------------------------------------------------
		$checklist = Checklist::create([
		    'code'                 => '8.1.1',
			'description'          => 'Generalidades',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización planifica, implementa, controla y mantiene los procesos necesarios para cumplir los requisitos del sistema de gestión de la SST y para implementar las acciones determinadas mediante?:
				a) el establecimiento de criterios para los procesos;
				b) la implementación del control de los procesos de acuerdo con los criterios;
				c) el mantenimiento y la conservación de información documentada en la medida necesaria para confiar en que los procesos se han llevado a cabo según lo planificado;
				d) la adaptación del trabajo a los trabajadores.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '8.1.2',
			'description'          => 'Eliminar peligros y reducir riesgos para la SST',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización establece, implementa y mantiene procesos para la eliminación de los peligros y la reducción de los riesgos para la SST?, utilizando la siguiente jerarquía de los controles:
				a) eliminar el peligro;
				b) sustituir con procesos, operaciones, materiales o equipos menos peligrosos;
				c) utilizar controles de ingeniería y reorganización del trabajo;
				d) utilizar controles administrativos, incluyendo la formación;
				e) utilizar equipos de protección personal adecuados.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '8.1.3',
			'description'          => 'Gestión del cambio',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización establece procesos para la implementación y el control de los cambios planificados temporales y permanentes que impactan en el desempeño de la SST?, incluyendo:
			a) los nuevos productos, servicios y procesos o los cambios de productos, servicios y procesos existentes, incluyendo:
			— las ubicaciones de los lugares de trabajo y sus alrededores;
			— la organización del trabajo;
			— las condiciones de trabajo;
			— los equipos;
			— la fuerza de trabajo;
			b) cambios en los requisitos legales y otros requisitos;
			c) cambios en el conocimiento o la información sobre los peligros y riesgos para la SST;
			d) desarrollos en conocimiento y tecnología.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '8.1.4.1',
			'description'          => 'Generalidades',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización establece, implementa y mantiene procesos para controlar la compra de productos y servicios de forma que se asegure su conformidad con su sistema de gestión de la SST?',
		]);
		$checklist = Checklist::create([
		    'code'                 => '8.1.4.2',
			'description'          => 'Contratistas',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización coordina sus procesos de compras con sus contratistas, para identificar los peligros y para evaluar y controlar los riesgos para la SST?, que surjan de:
				a) las actividades y operaciones de los contratistas que impactan en la organización;
				b) las actividades y operaciones de la organización que impactan en los trabajadores de los contratistas;
				c) las actividades y operaciones de los contratistas que impactan en otras partes interesadas en el
				lugar de trabajo.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '8.1.4.3',
			'description'          => 'Contratación externa',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización se asegura de que las funciones y los procesos contratados externamente estén controlados y se asegura de que sus acuerdos en materia de contratación externa son coherentes con los requisitos legales y otros requisitos y con alcanzar los resultados previstos del sistema de gestión de la SST y definido el tipo y el grado de control a aplicar a estas funciones y procesos deben definirse dentro del sistema de gestión de la SST?',
		]);
		$checklist = Checklist::create([
		    'code'                 => '8.2',
			'description'          => 'Preparación y respuesta ante emergencias',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización establece, implementa y mantiene procesos necesarios para prepararse y para responder ante situaciones de emergencia potenciales?, incluyendo:
				a) el establecimiento de una respuesta planificada a las situaciones de emergencia, incluyendo la prestación de primeros auxilios;
				b) la provisión de formación para la respuesta planificada;
				c) las pruebas periódicas y el ejercicio de la capacidad de respuesta planificada;
				d) la evaluación del desempeño y, cuando sea necesario, la revisión de la respuesta planificada, incluso después de las pruebas y, en particular, después de que ocurran situaciones de emergencia;
				e) la comunicación y provisión de la información pertinente a todos los trabajadores sobre sus deberes y responsabilidades;
				f) la comunicación de la información pertinente a los contratistas, visitantes, servicios de respuesta ante emergencias, autoridades gubernamentales y, según sea apropiado, a la comunidad local;
				g) tener en cuenta las necesidades y capacidades de todas las partes interesadas pertinentes y asegurándose que se involucran, según sea apropiado, en el desarrollo de la respuesta planificada.',
		]);
		// -----------------------------------------------------
		$checklist = Checklist::create([
		    'code'                 => '9.1.1',
			'description'          => 'Generalidades',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización establece, implementa y mantiene procesos para el seguimiento, la medición, el análisis y la evaluación del desempeño?, determinando:
				a) qué necesita seguimiento y medición, incluyendo:
				1) el grado en que se cumplen los requisitos legales y otros requisitos;                                           
				2) sus actividades y operaciones relacionadas con los peligros, los riesgos y oportunidades identificados;
				3) el progreso en el logro de los objetivos de la SST de la organización;
				4) la eficacia de los controles operacionales y de otros controles;
				b) los métodos de seguimiento, medición, análisis y evaluación del desempeño, según sea aplicable, para asegurar resultados válidos;
				c) los criterios frente a los que la organización evaluará su desempeño de la SST;
				d) cuándo se debe realizar el seguimiento y la medición;
				e) cuándo se deben analizar, evaluar y comunicar los resultados del seguimiento y la medición.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '9.1.2',
			'description'          => 'Evaluación del cumplimiento',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización establece, implementa y mantiene procesos para evaluar el cumplimiento con los requisitos legales y otros requisitos?, incluyendo:
				a) determinar la frecuencia y los métodos para la evaluación del cumplimiento;
				b) evaluar el cumplimiento y tomar acciones si es necesario.
				c) mantener el conocimiento y la comprensión de su estado de cumplimiento con los requisitos legales y otros requisitos;
				d) conservar la información documentada de los resultados de la evaluación del cumplimiento.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '9.2.1',
			'description'          => 'Generalidades',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización lleva a cabo auditorías internas a intervalos planificados, para proporcionar información acerca de si el sistema de gestión de la SST es conforme con los requisitos propios de la organización para su sistema de gestión de la SST, incluyendo la política de la SST y los objetivos de la SST, y se implementa y mantiene eficazmente?',
		]);
		$checklist = Checklist::create([
		    'code'                 => '9.2.2',
			'description'          => 'Programa de auditoria interna',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización planifica, establece, implementa y mantiene programas de auditoría que incluyan la frecuencia, los métodos, las responsabilidades, la consulta, los requisitos de planificación, y la elaboración de informes, que deben tener en consideración la importancia de los procesos involucrados y los resultados de las auditorías previas?, incluyendo:
				- definir los criterios de la auditoría y el alcance para cada auditoría;
				- seleccionar auditores y llevar a cabo auditorías para asegurarse de la objetividad y la imparcialidad del proceso de auditoría;
				- asegurarse de que los resultados de las auditorías se informan a los directivos pertinentes;
				- asegurarse de que se informa de los hallazgos de la auditoría pertinentes a los trabajadores, y cuando existan, a los representantes de los trabajadores, y a otras partes interesadas pertinentes;
				- tomar acciones para abordar las no conformidades y mejorar continuamente su desempeño de la SST;
				- conservar información documentada como evidencia de la implementación del programa de auditoría y de los resultados de las auditorías.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '9.3',
			'description'          => 'Revisión por la dirección',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La alta dirección revisa el sistema de gestión de la SST de la organización a intervalos planificados, para asegurarse de su conveniencia, adecuación y eficacia continuas? considerando:
				a) el estado de las acciones de las revisiones por la dirección previas;
				b) los cambios en las cuestiones externas e internas que sean pertinentes al sistema de gestión de la SST, incluyendo:
				1) las necesidades y expectativas de las partes interesadas;
				2) los requisitos legales y otros requisitos;
				3) los riesgos y oportunidades;                                   c) el grado en el que se han cumplido la política de la SST y los objetivos de la SST;
				d) la información sobre el desempeño de la SST, incluidas las tendencias relativas a:
				1) los incidentes, no conformidades, acciones correctivas y mejora continua;
				2) los resultados de seguimiento y medición;
				3) los resultados de la evaluación del cumplimiento con los requisitos legales y otros requisitos;
				4) los resultados de la auditoría;
				5) la consulta y la participación de los trabajadores;
				6) los riesgos y oportunidades;
				e) la adecuación de los recursos para mantener un sistema de gestión de la SST eficaz;
				f) las comunicaciones pertinentes con las partes interesadas;
				g) las oportunidades de mejora continua.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '10.1',
			'description'          => 'Generalidades',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section10->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización determina las oportunidades de mejora e implementa las acciones necesarias para alcanzar los resultados previstos de su sistema de gestión de la SST?',
		]);
		$checklist = Checklist::create([
		    'code'                 => '10.2',
			'description'          => 'Incidentes, no conformidades y acciones correctivas',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section10->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿La organización establece, implementa y mantiene procesos, incluyendo informar, investigar y tomar acciones para determinar y gestionar los incidentes y las no conformidades?
				Incluyendo:
				a) reaccionar de manera oportuna ante el incidente o la no conformidad y, según sea aplicable:
				1) tomar acciones para controlar y corregir el incidente o la no conformidad;
				2) hacer frente a las consecuencias;
				b) evaluar, con la participación de los trabajadores  e involucrando a otras partes interesadas pertinentes, la necesidad de acciones correctivas para eliminar las causas raíz del incidente o la no conformidad, con el fin de que no vuelva a ocurrir ni ocurra en otra parte, mediante:
				1) la investigación del incidente o la revisión de la no conformidad;
				2) la determinación de las causas del incidente o la no conformidad;
				3) la determinación de si han ocurrido incidentes similares, si existen no conformidades, o si potencialmente podrían ocurrir;
				c) revisar las evaluaciones existentes de los riesgos para la SST y otros riesgos, según sea apropiado;
				d) determinar e implementar cualquier acción necesaria, incluyendo acciones correctivas, de acuerdo con la jerarquía de los controles y la gestión del cambio;
				e) evaluar los riesgos de la SST que se relacionan con los peligros nuevos o modificados, antes de tomar acciones;
				f) revisar la eficacia de cualquier acción tomada, incluyendo las acciones correctivas;
				g) si fuera necesario, hacer cambios al sistema de gestión de la SST.',
		]);
		$checklist = Checklist::create([
		    'code'                 => '10.3',
			'description'          => 'Mejora continua',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section10->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => ' ¿La organización mejora continuamente la conveniencia, adecuación y eficacia del sistema de gestión de la SST?, para:
				a) mejorar el desempeño de la SST;
				b) promover una cultura que apoye al sistema de gestión de la SST;
				c) promover la participación de los trabajadores en la implementación de acciones para la mejora
				continua del sistema de gestión de la SST;
				d) comunicar los resultados pertinentes de la mejora continua a sus trabajadores, y cuando existan, a los representantes de los trabajadores;
				e) mantener y conservar información documentada como evidencia de la mejora continua.',
		]);

	}	
}
