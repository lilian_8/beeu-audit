<?php

namespace App\Classes;

use Illuminate\Database\Eloquent\Model;
use App\SectionChecklist;
use App\Checklist;

class Norm14 extends Model
{
	public static function check($id)
    {
	    $section4  = SectionChecklist::where('code','4')->get()->first();
		$section5  = SectionChecklist::where('code','5')->get()->first();
		$section6  = SectionChecklist::where('code','6')->get()->first();
		$section7  = SectionChecklist::where('code','7')->get()->first();
		$section8  = SectionChecklist::where('code','8')->get()->first();
		$section9  = SectionChecklist::where('code','9')->get()->first();
		$section10 = SectionChecklist::where('code','10')->get()->first();	


		$checklist = Checklist::create([
		    'code'                 => '4.1',
			'description'          => 'Comprension de la organización y su contexto ',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section4->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'   			   => '¿Cómo ha determinado la organización las cuestiones externas e internas que son pertinentes para su propósito y que afectan a su capacidad para alcanzar los resultados previstos de su sistema de gestión ambiental? ',
		]);
		
		$checklist = Checklist::create([
		    'code'                 => '4.2',
			'description'          => 'Comprensión de las  necesidades y expectativas de las partes interesadas',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section4->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'   			   => '¿Cómo ha determinado las partes interesadas que son pertinentes al sistema de gestión ambiental?                                 ¿Cómo ha determinado las necesidades y espectativas pertinentes de las partes interesadas?                                                  ¿Cómo se determinó que estas necesidades y expectativas se convierten en requisitos legales y otros requisitos?',
		]);	

		$checklist = Checklist::create([
		    'code'                 => '4.3',
			'description'          => 'Determinación del alcance del sistema de gestión ambiental',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section4->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'   			   => '¿Cómo ha determinado la organización los límites y la aplicabilidad del sistema de gestión ambiental para establecer su alcance considerando los siguientes puntos?:             a) las cuestiones internas y externas a que se hacen referencia en el apartado 4.1;                b) los requisitos legales y otros requisitos a los que se hace referencia en el apartado 4.2; c) las unidades, funciones y límites físicos de la organización;                                                    d) sus actividades, productos y servicios;         e) su autoridad y capacidad para ejercer control e influencia.',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '4.4',
			'description'          => 'Sistema de gestión ambiental ',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section4->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'   			   => '¿Cómo logra los resultados previstos, incluida la mejora de su desempeño ambiental, la organización debe establecer, implementar, mantener y mejorar continuamente un sistema de gestión ambiental, que incluya los procesos necesarios y sus interacciones, de acuerdo con los requisitos de esta norma mexicana.',
		]);	
		// -----------------------------------------
		$checklist = Checklist::create([
		    'code'                 => '5.1',
			'description'          => 'Liderazgo y compromiso',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section5->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo demuestra el liderazgo y compromiso con respecto al sistema de gestión ambiental en la Alta Dirección?:
			a) asumiendo la responsabilidad y la rendición de cuentas con relación a la eficacia del sistema de gestión ambiental;
			b) asegurándose de que se establezcan la política ambiental y los objetivos ambientales, y que éstos sean compatibles con la dirección estratégica y el contexto de la organización;
			c) asegurándose de la integración de los requisitos del sistema de gestión ambiental en los procesos de negocio de la organización;
			d) asegurándose de que los recursos necesarios para el sistema de gestión ambiental estén disponibles;
			e) comunicando la importancia de una gestión ambiental eficaz y conforme con los requisitos del sistema de gestión ambiental;
			f) asegurándose de que el sistema de gestión ambiental logre los resultados previstos;
			g) dirigiendo y apoyando a las personas, para contribuir a la eficacia del sistema de gestión ambiental;',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '5.2',
			'description'          => 'Política Ambiental',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section5->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '5.3',
			'description'          => 'Roles, responsabilidades y autoridades en la organización',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section5->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo se asegura la Alta Dirección de que las responsabilidades y autoridades para los roles pertinentes dentro del sistema de gestión ambiental se asignen y comuniquen a todos los niveles dentro de la organización?',
		]);	

		// -----------------------------------------------------
		$checklist = Checklist::create([
		    'code'                 => '6.1.1',
			'description'          => '¿Cómo se debe establecer, implementar y mantener los procesos necesarios para cumplir los requisitos de los apartados 6.1.1 a 6.1.4.?',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => 'Al planificar el sistema de gestión ambiental, la organización debe considerar:
				a) las cuestiones referidas en el apartado 4.1;
				b) los requisitos referidos en el apartado 4.2;
				c) el alcance de su sistema de gestión ambiental;',
		]);	

		$checklist = Checklist::create([
		    'code'                 => '6.1.2',
			'description'          => 'Aspectos ambientales ',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo se determinadel alcance definido del sistema de gestión ambiental, la organización debe determinar los aspectos ambientales de sus actividades, productos y servicios que puede controlar y de aquellos en los que puede influir, y sus impactos ambientales asociados, desde una perspectiva de ciclo de vida.',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '6.1.3',
			'description'          => 'Requisitos legales y otros requisitos',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo se determina con un nivel de detalle suficiente los requisitos legales y otros requisitos que ha identificado en el apartado 4.2, que son aplicables a sus aspectos ambientales y cómo se aplican a la organización. Los requisitos legales y otros requisitos incluyen los requisitos legales que la organización debe cumplir, y los demás requisitos que la organización tiene que cumplir o que decide cumplr?
			Los requisitos legales obligatorios relacionados con los aspectos ambientales de una organización pueden incluir, si es aplicable:                                                                                     a) requisitos de entidades gubernamentales u otras autoridades pertinentes;
			b) leyes y reglamentaciones internacionales, nacionales y locales;
			c) requisitos especificados en permisos, licencias u otras formas de autorización;
			d) órdenes, reglas u orientaciones emitidas por los organismos de reglamentación;
			e) sentencias de cortes de justicia o tribunales administrativos.',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '6.1.4',
			'description'          => 'Planificación de acciones',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo planifica la organización?:                                    a) la toma de acciones para abordar sus:
				1) aspectos ambientales significativos;
				2) requisitos legales y otros requisitos;
				3) riesgos y oportunidades identificados en el apartado 6.1.1.
				b) la manera de:
				1) integrar e implementar las acciones en los procesos de su sistema de gestión ambiental (ver 6.2, 7, 8 y 9.1) o en otros procesos de negocio;
				2) evaluar la eficacia de estas acciones (ver 9.1).',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '6.2.1',
			'description'          => 'Objetivos ambientales',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo se debe establecer los objetivos ambientales para las funciones y niveles pertinentes, teniendo en cuenta los aspectos ambientales significativos de la organización y sus requisitos legales y otros requisitos asociados, y considerando sus riesgos y oportunidades.
			Los objetivos ambientales deben:
			a) ser coherentes con la política ambiental;
			b) ser medibles (si es factible);
			c) ser objeto de seguimiento;
			d) comunicarse;
			e) actualizarse, según corresponda.',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '6.2.2.',
			'description'          => 'Planificación de acciones para lograr los objetivos ambientales',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section6->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo lograr Los objetivos ambientales, la organización debe determinar:
			a) qué se va a hacer;
			b) qué recursos se requerirán;
			c) quién será responsable;
			d) cuándo se finalizará;
			e) cómo se evaluarán los resultados, incluidos los indicadores',
		]);	
		// -------------------------------------------------------
		$checklist = Checklist::create([
		    'code'                 => '7.1',
			'description'          => 'Recursos',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo determina y proporciona la organización  los recursos necesarios para el establecimiento,implementación, mantenimiento y mejora continua del sistema de gestión ambiental?',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '7.2',
			'description'          => 'Competencia',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo se debe determinar la competencia dentro de una organización?                                                                
			a) determinar la competencia necesaria de las personas que realizan trabajos bajo su control, que afecte a su desempeño ambiental y su capacidad para cumplir sus requisitos legales y otros requisitos;
			b) asegurarse de que estas personas sean competentes, con base en su educación, formación o experiencia apropiadas;
			NMX-SAA-14001-IMNC-2015 ISO 14001:2015
			12 Derechos reservados © IMNC 2015
			c) determinar las necesidades de formación asociadas con sus aspectos ambientales y su sistema de gestión ambiental;
			d) cuando sea aplicable.',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '7.3',
			'description'          => 'Toma de conciencia',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo ejerce la organización la toma de conciencia de la política ambiental para entender que los compromisos se deben memorizar o que las personas que realicen trabajo bajo el control de la organización tengan una copia de la política ambiental documentada. Preferentemente, estas personas deberían conocer su existencia, su propósito y su función para el logro de los compromisos, que incluya cómo su trabajo puede afectar a la capacidad de la organización para cumplir sus requisitos legales y otros requisitos?',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '7.4.1',
			'description'          => 'Genralidades',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿De qué debe asegurarse la organización para la toma de conciencia para que todas las personas que realicen el trabajo bajo el control de la organización tomen conciencia de:
			a) la política ambiental;
			b) los aspectos ambientales significativos y los impactos ambientales reales o potenciales relacionados, asociados con su trabajo;
			c) su contribución a la eficacia del sistema de gestión ambiental, incluidos los beneficios de una mejora del desempeño ambiental;
			d) las implicaciones de no satisfacer los requisitos del sistema de gestión',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '7.4.2',
			'description'          => 'Comunicación interna',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo se comunica internamente la información pertinente para el sistema de gestión ambiental entre los diversos niveles y funciones de la organización, incluyendo los cambios en el sistema de gestión ambiental, según sea apropiado y asegura de que sus procesos de comunicación permitan a los trabajadores contribuir a la mejora continua?',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '7.4.3',
			'description'          => 'Comunicación externa',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo se comunica externamente la información pertinente para el sistema de gestión ambiental, según se establece en los procesos de comunicación de la organización y teniendo en cuenta sus requisitos legales y otros requisitos?',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '7.5.1',
			'description'          => '¿Cómo se mantiene y se crea la información documentada suficiente, de manera que asegure un sistema de gestión ambiental conveniente, adecuado y eficaz. Así como  el enfoque principal que deberá centrarse en la implementación del sistema de gestión ambiental y el desempeño ambiental?',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '7.5.2',
			'description'          => 'Creación y actualización:',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section7->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo crear y actualizar la información documentada, la organización debe asegurarse de que lo siguiente sea apropiado?',
		]);	
		// -------------------------------------------------------
		$checklist = Checklist::create([
		    'code'                 => '8.1',
			'description'          => 'Planificación y control operacional:',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => ' ¿Cómo se debe establecer, implementar, controlar y mantener los procesos necesarios para satisfacer los requisitos del sistema de gestión ambiental y para implementar las acciones determinadas en los apartados 6.1 y 6.2, mediante:                                                          el establecimiento de criterios de operación para los procesos;
			 la implementación del control de los procesos de acuerdo con los criterios de operación.',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '8.2',
			'description'          => 'Preparación y respuesta ante emergencias',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section8->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo se establece, implementa y mantiene procesos necesarios para prepararse y para responder ante situaciones de emergencia potenciales?, incluyendo:
			a) prepararse para responder, mediante la planificación de acciones para prevenir o mitigar los impactos ambientales adversos provocados por situaciones de emergencia;
			b) responder a situaciones de emergencia reales;
			c) tomar acciones para prevenir o mitigar las consecuencias de las situaciones de emergencia, apropiadas a la magnitud de la emergencia y al impacto ambiental potencial;
			d) poner a prueba periódicamente las acciones de respuesta planificadas, cuando sea factible;
			e) evaluar y revisar periódicamente los procesos y las acciones de respuesta planificadas, en particular, después de que hayan ocurrido situaciones de emergencia o de que se hayan realizado pruebas;
			f) proporcionar información y formación pertinentes, con relación a la preparación y respuesta ante emergencias,',
		]);	

		// -----------------------------------------------------
		$checklist = Checklist::create([
		    'code'                 => '9.1.2',
			'description'          => 'Evaluación del cumplimiento',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo se debe establecer, implementar y mantener los procesos necesarios para evaluar el cumplimiento de sus requisitos legales y otros requisitos.,                                         Incluyendo:
			a) determinar la frecuencia con la que se evaluará el cumplimiento;
			b) evaluar el cumplimiento y emprender las acciones que fueran necesarias;
			c) mantener el conocimiento y la comprensión de su estado de cumplimiento.',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '9.2',
			'description'          => 'Generalidades ',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cada cuanto tiempo organización lleva a cabo auditorías internas a intervalos planificados, para proporcionar información acerca de si el sistema de gestión ambiental es conforme con los requisitos propios de la organización para su sistema de gestión ambiental, incluyendo la política ambiental y los objetivos ambiental, y se implementa y mantiene eficazmente?',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '9.2.2',
			'description'          => 'Programa de auditoría interna: ',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo se debe establecer, implementar y mantener uno o varios programas de auditoría interna que incluyan la frecuencia, los métodos, las responsabilidades, los requisitos de planificación y la elaboración de informes de sus auditorías internas?',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '9.3',
			'description'          => 'Revisión por la Dirección:',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section9->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo debe revisar la Alta Dirección el sistema de gestión ambiental de la organización a intervalos planificados, para asegurarse de su conveniencia, adecuación y eficacia continuas?',
		]);	

		// ------------------------------------------------------
		$checklist = Checklist::create([
		    'code'                 => '10.1',
			'description'          => 'Generalidades',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section10->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cómo se deben determinar las oportunidades de mejora (véanse 9.1, 9.2 y 9.3) e implementar las acciones necesarias para lograr los resultados previstos en su sistema de gestión ambiental?',

		]);	
		$checklist = Checklist::create([
		    'code'                 => '10.2',
			'description'          => 'No conformidad y acción correctiva ',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section10->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => '¿Cuál es el procedimiento a realizar en caso de una No Conformidad y las medidas de acción correctivas con el minimizar el riesgo y mitigar el impacto ambiental?',
		]);	
		$checklist = Checklist::create([
		    'code'                 => '10.3',
			'description'          => 'Mejora continua',
			'order'                => '1',
			'company_sector_id'    => $id,
			'section_checklist_id' => $section10->id,
			'type'                 => 'Mayor',
			'created_at'		   => '2020-12-15 22:44:16.0',
			'updated_at'		   => '2020-12-15 22:44:16.0',
			'help'				   => ' ¿Cómo se debe mejorar continuamente la conveniencia, adecuación y eficacia del sistema de gestión ambiental para mejorar el desempeño ambiental?',
		]);	

	}

}
