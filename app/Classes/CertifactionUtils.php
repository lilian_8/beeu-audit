<?php

namespace App\Classes;

use App;
use Illuminate\Database\Eloquent\Model;

class CertifactionUtils extends Model
{
    public function validTeam($certification,$idUser)
    {
        $teamUsers = [];
        foreach ($certification->audit as $audit) {
            foreach ($audit->teams as $team) {
                $teamUsers[$team->user_id] = $team->user_id;
            }
        }

        return in_array($idUser,$teamUsers);

    }
}
