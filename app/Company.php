<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	protected $table    = 'companies';
	
	protected $fillable = [ 'name','website','rfc','acreditation_body','avatar'];
	
	protected $guarded  = [ 'id' ];

	public function direction()
	{
  		return $this->hasOne('App\Direction','company_organization_id');
    }
}
