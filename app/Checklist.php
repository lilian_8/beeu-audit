<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
    public $timestamps  = true;

	protected $table    = 'checklists';

	protected $fillable = ['code','description','help','order','company_sector_id','section_checklist_id','type'];   

	protected $guarded  = ['id'];

	public function sectionCheklist()
	{
		return $this->belongsTo('App\SectionChecklist','section_checklist_id','id');
	}

	public function companySector()
	{
		return $this->belongsTo('App\CompanySector','company_sector_id','id');
	}

}
