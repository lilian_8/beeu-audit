<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobHistory extends Model
{
    protected $table    = 'job_histories';
	
	protected $fillable = ['period','company','position','developed_activity','user_id'];
 
	public $timestamps  = true;
}
