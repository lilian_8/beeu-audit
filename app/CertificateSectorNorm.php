<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CertificateSectorNorm extends Model
{
  	protected $table    = 'certificate_sector_norms';
	
	protected $fillable = [ 'sector_id','norm_id','certification_id'];
	
	protected $guarded  = [ 'id' ];

	public function sector()
	{
  		return $this->belongsTo('App\Sector');
    }

    public function norm()
	{
  		return $this->belongsTo('App\Norm');
    }

   	public function OrganizationSector()
	{
		return $this->belongsTo('App\Certification','certification_id','id');
	}


}
