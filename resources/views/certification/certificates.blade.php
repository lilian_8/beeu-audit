@extends('layouts.app')
    @section('css')
        <style type="text/css">

        </style>
    @endsection

@section('content')
       <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-0">
            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Certificados</li>
          </ol>
          <br>
        </nav>
        
        <div class="col-sm-12 col-lg-12 col-md-12">
          <div class="card card-body">
            
            <div class="container-fluid row">
              <h6 class="tx-uppercase tx-20 tx-spacing-1 tx-color-02 tx-semibold mg-b-8 col-lg-10"><i data-feather="inbox"></i> Certificados</h6>
            </div>

            <hr>

            <div class="container-fluid row"><h1 class="tx-20 tx-spacing-1 tx-color-03 tx-semibold mg-b-8 col-lg-10">Filtros</h1></div>
            
            <form>
            <div class="container-fluid row">

              <div class="col-lg-6 col-sm-12 col-md-12">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">TIPO</label>
                <select class="custom-select" name="type">
                  <option selected disabled>Seleccionar...</option>
                  <option value="Fase 2">Fase 2</option>
                  <option value="Recertificacion">Recertificacion</option>
                </select>
              </div>

              <div class="col-lg-6 col-sm-12 col-md-12">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NOMBRE ORGANIZACIÓN O CÓDIGO</label>
                    <input name="search" class="form-control" type="search" placeholder="Buscar por nombre,rfc o código" aria-label="Search">
              </div>

            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-md-12">
                        <br>
                        <button class="btn bg-dark text-white" type="submit"><i class="fa fa-search"></i> Buscar</button>
                        <button class="btn bg-dark text-white" type="submit"> Ver todos</button>
                    </div>
                </div>
            </div>

            <hr>

            </form>
            <div class="container-fluid ">
                
                <div class="table-responsive ">
                  <table class="table table-striped table-sm  table-bordered ">
                    <thead>
                     <tr>
                      <th>Código</th>
                      <th>Organizacion</th>
                      <th class="d-none d-md-table-cell">Tipo</th>
                      <th class="d-none d-md-table-cell">Fecha de inicio</th>
                      <th class="d-none d-md-table-cell">Fecha de término</th>
                      <th class="d-none d-md-table-cell">Status</th>
                      <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($certificates as $certificate)
                        <tr>
                          <td>{{$certificate->Audit->certification->certification->code}}</td>
                          <td>{{$certificate->Audit->certification->certification->name}}</td>
                          <td>{{$certificate->type}}</td>
                          <td>{{$certificate->init_date}}</td>
                          <td>{{$certificate->end_date}}</td>
                          <td>
                              @if($certificate->status==='Valido')
                                <span class="badge badge-warning">Vigente</span>
                                @elseif($certificate->status==='Suspendido')
                                <span class="badge badge-success">Suspendido</span>
                                @else
                                <span class="badge badge-danger">Cancelado</span>
                                @endif
                          </td>
                          <td>@if($certificate->Audit->status=="Terminado")  
                                <a class="btn btn-light danger-btn btn-icon "  target="_blank" href="{{Storage::url($certificate->file_path)}}" title="Descargar"><i class="fas fa-file-download text-primary"></i></a>

                                <!-- <button type="button" class="btn btn-light danger-btn btn-icon" data-toggle="modal" data-target="#editcertification{{$certificate->id}}" title="Editar">
                                    <i class="fas fa-edit text-success"></i>
                                </button> -->
                              @else
                                <a class="text-danger">Para descargar o editar, finalizar la auditoria</a>
                              @endif
                          </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>

          </div>
        </div>
     </div>

@extends('certification.editcertification')
@endsection

