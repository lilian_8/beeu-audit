<!-- Modal -->
<div class="modal fade" id="starAuditoryForAlone" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Iniciar nueva auditoria</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form class="form-horizontal" method="post" action="{{route('newaudit')}}" >
      <div class="modal-body">
          {{ csrf_field() }}
            <div class="form-group">
              <label >Sectores y Normas</label>
              <br>
                <table class="table table-bordered ">
                  <thead class="thead-primary">
                    <tr>
                      <td>Sector</td>
                      <td>Norma</td>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($audit->auditsector as $ad)
                      <tr>
                        <th scope="row">{{$ad->sector->name}}</th>
                        <th scope="row">{{$ad->norm->name}}</th>
                      </tr>
                      <input type="hidden" name="sectores[]" value="{{$ad->sector->id}}">
                      <input type="hidden" name="norms[]" value="{{$ad->norm->id}}">
                  @endforeach
                  </tbody>
                </table>
            </div>

            <div class="form-group">
              <label >Tipo de auditoria</label>
                <select class="form-control" style="width: 100%" id="type" name="type" required="required">
                  @if($audit->type=="Fase 1")
                    <option selected disabled>Elige ...</option>
                    <option value="Fase 2">Fase 2</option>
                    <option value="Especial">Especial</option>
                  @elseif($audit->type=="Fase 2")
                    <option selected disabled>Elige ...</option>
                    <option value="Seguimiento 1">Seguimiento 1</option>
                    <option value="Especial">Especial</option>
                  @elseif($audit->type=="Seguimiento 1")
                    <option selected disabled>Elige ...</option>
                    <option value="Seguimiento 2">Seguimiento 2</option>
                    <option value="Especial">Especial</option>
                  @elseif($audit->type=="Seguimiento 2")
                    <option selected disabled>Elige ...</option>
                    <option value="Recertificacion">Recertificacion</option>
                    <option value="Especial">Especial</option>
                  @elseif($audit->type=="Recertificacion")
                    <option selected disabled>Elige ...</option>
                    <option value="Seguimiento 1">Seguimiento 1</option>
                  @elseif($audit->type=="Especial")
                    <option selected disabled>Elige ...</option>
                    <option value="Fase 1">Fase 1</option>
                    <option value="Fase 2">Fase 2</option>
                    <option value="Seguimiento 1">Seguimiento 1</option>
                    <option value="Seguimiento 2">Seguimiento 2</option>
                    <option value="Recertificacion">Recertificacion</option>
                  @endif
                </select>
            </div>
            
            <input type="hidden" name="auditId" value="{{$audit->id}}">
            
            <div class="form-group">
              <label >Fecha de inicio</label>
              <input type="date" name="date_start" class="form-control">
            </div>

            <div class="form-group">
              <label >Fecha de termino</label>
              <input type="date" name="date_finish" class="form-control">
            </div>
            </div>
            <div class="modal-footer nodisabled">
            </div>
        </form>
    </div>
  </div>
</div>