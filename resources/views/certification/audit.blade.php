@extends('layouts.app')
    @section('css')
        <style type="text/css">

          .selected{
            cursor: pointer;
          }
          .selected:hover{
            background-color: #0585C0;
            color: white;
          }
          .seleccionada{
            background-color: #0585C0;
            color: white;
          }
          .card-header .fa {
            transition: .3s transform ease-in-out;
          }
          .card-header .collapsed .fa {
            transform: rotate(90deg);
          }

          #lbl{
             cursor: pointer;
             background-color: red
             /* Style as you please, it will become the visible UI component. */
          }

          #file {
             opacity: 0;
             position: absolute;
             z-index: -1;
          }
        </style>

    @endsection

@section('content')

      @if(in_array(Auth::user()->profile, ['Registro', 'Tecnico']))
        <input type="hidden" name="validacion" class="validacionbotones" value="1">
      @endif

       <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-0">
            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Progreso</li>
          </ol>
          <br>
        </nav>

        <div class="col-sm-6 col-lg-12">
          <div class="card card-body">
            <div class="container-fluid row">
              <h6 class="tx-uppercase tx-20 tx-spacing-1 tx-color-02 tx-semibold mg-b-8 col-lg-10"><i data-feather="flag"></i> Progreso de certificacion </h6>
            </div>
            <hr>
            <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                  <div class="card">
                      <div class="card-header bg-dark text-white"> <i data-feather="home"></i> Organización</div>
                      <div class="card-body">
                          <strong>Código</strong>: {{$organizations->code}}
                          <br>
                          <strong>Organización</strong>: {{$organizations->name}}
                          <br>
                          <strong>Rfc</strong>: {{$organizations->rfc}}
                          <br>
                          <strong>Sitios</strong>: {{$organizations->sites}}
                          <br>
                          <strong>Empleados</strong>: {{$organizations->employees}}
                      </div>
                  </div>
                </div>

                <div class="col-sm-3">
                  <div class="card">
                      <div class="card-header bg-dark text-white"> <i data-feather="phone"></i> Contacto</div>
                      <div class="card-body">
                          <strong>Nombre</strong>: {{$organizations->directionCertification->contact_name}}
                          <br>
                          <strong>Puesto</strong>: {{$organizations->directionCertification->contact_job}}
                          <br>
                          <strong>Teléfono</strong>: {{$organizations->directionCertification->extension}}  {{$organizations->directionCertification->phone}}
                          <br>
                          <strong>Email</strong>:{{$organizations->directionCertification->email}}
                      </div>
                  </div>
                </div>

                <div class="col-sm-3">
                    <div class="card">
                        <div class="card-header bg-dark text-white"> <i data-feather="map-pin"></i> Direccion</div>
                        <div class="card-body">
                            <strong>Estado</strong>: {{$organizations->directionCertification->state}}, {{$organizations->directionCertification->municipality}}
                            <br>
                            <strong>Ciudad</strong>: {{$organizations->directionCertification->city}}, {{$organizations->directionCertification->suburb}}
                            <br>
                            <strong>Calle</strong>: {{$organizations->directionCertification->street}} #{{$organizations->directionCertification->external_number}} #int:{{$organizations->directionCertification->internal_number}}
                            <br>
                            <strong>CP</strong>: {{$organizations->directionCertification->zip_code}}
                        </div>
                    </div>
                </div>
                <input type="hidden" name="validacion" class="validacionbotones" value="{{$audits->finished}}">
                <div class="col-sm-3">
                  <div class="card">
                      <div class="card-header bg-dark text-white"> <i data-feather="file-text"></i> Datos de auditoria</div>
                      <div class="card-body">
                          <strong>Inicio de la auditoria</strong>: <br> {{$audits->init_date}}
                          <br>
                          <strong>Término de la auditoria</strong>: <br> {{$audits->finish_date}}
                      </div>
                  </div>
                </div>
                <div class="col-sm-6 mt-2">
                    <div class="card">
                        <div class="card-header bg-dark text-white"> <i data-feather="file-text"></i>Sectores</div>
                        <table class="table  table-bordered table-sm ">
                            <tbody>
                                @foreach($filterSector as $ad)
                                    <tr>
                                        <th >{{$ad}}</th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-sm-6 mt-2">
                    <div class="card">
                        <div class="card-header bg-dark text-white"> <i data-feather="file-text"></i>Normas</div>
                        <table class="table  table-bordered table-sm ">
                            <tbody>
                                @foreach($filterNorm as $ad)
                                    <tr>
                                        <th >{{$ad}}</th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <h3 class="mt-5 row">
              <hr>
                @if($audits->type=='Especial')
                  {{$audits->type}} <i class="fas fa-star"></i>
                @else
                    @if($audits->status=='En progreso')
                      <span class="step-icon mr-2 text-success"><i class="fas fa-dice-d6"></i></span> {{$audits->type}}
                    @elseif($audits->status=='Terminado')
                      <span class="step-icon mr-2 text-primary"><i class="fas fa-dice-d6"></i></span> {{$audits->type}}
                    @endif
                @endif
              <hr>
            </h3>
            <div class="content">
                <div class="container-fluid pd-x-0 pd-lg-x-10 pd-xl-x-0">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row row-xs mg-b-25">
                                <ul class="steps mb-5">
                                    <a class="d-none"> {{$validationbutton=0}}</a>
                                    @foreach($auditscomplete as $auditcmd )
										<li class="step-item  @if($auditcmd->status=='En progreso') complete @elseif($auditcmd->status=='Terminado') active @endif">
												<a href="{{route('auditdetails', $auditcmd->id)}}" class="step-link">
													<span class="step-icon"><i class="fas fa-dice-d6"></i></span>
													<div>
														<span class="step-title">
															@if($auditcmd->type=='Especial')
																{{$auditcmd->type}} <i class="fas fa-star"></i>
															@else
																{{$auditcmd->type}}
															@endif
														</span>
														<span class="step-desc">{{$auditcmd->status}}</span>
													</div>
												</a>
                                        	</li>
                                        @if($auditcmd->status=='Terminado')
                                            <a class="d-none">{{$validationbutton+=1}}</a>
                                        @endif
                                        
                                     @endforeach
                                </ul>

                                @if($validationbutton==count($auditscomplete))
                                    <div style="float: right !important;">
                                        <a class="btn btn-warning btn-block" href="#starAuditoryForAlone" data-toggle="modal"> <i class="fas fa-flag-checkered"></i> Iniciar siguiente fase</a>
                                    </div>
                                    @extends('certification.modalendaudit')
                                @endif

                                <div class="col-12">
                                    <div class="mg-t-5 col-12" id="audit{{$audit->id}}">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            @if($validationUser || in_array(Auth::user()->profile, ['Administrador']))

                                                <li class="nav-item">
                                                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home{{$audit->id}}" role="tab" aria-controls="home" aria-selected="true"> <i class="fas fa-users" style="margin-right: 5px"></i> Equipo</a>
                                                </li>

                                                <li class="nav-item">
                                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile{{$audit->id}}" role="tab" aria-controls="profile" aria-selected="false"><i class="fas fa-clipboard-check" style="margin-right: 5px"></i> Checklist</a>
                                                </li>

                                                <li class="nav-item resultadotab">
                                                  <a class="nav-link " id="contact-tab" data-toggle="tab" href="#contact{{$audit->id}}" role="tab" aria-controls="contact" aria-selected="false"><i class="fas fa-file-signature" style="margin-right: 5px"></i> Resultados checklist</a>
                                                </li>

                                            @endif
                                            <!-- --------------------Profiles-------------- -->

                                            @if($validationUser)
                                                @if($validationUser->function=="Registro")
                                                    @if($audit->end_expedient!=1)
                                                        <li class="nav-item">
                                                          <a class="nav-link" data-toggle="tab" href="#expediente" role="tab" aria-controls="contact" aria-selected="false"><i class="fas fa-file-archive" style="margin-right: 5px"></i> Expediente</a>
                                                        </li>
                                                    @endif
                                                @elseif($validationUser->function=="Registro" || $validationUser->function=="Auditor lider" || $validationUser->function=="Auditor tecnico" || $validationUser->function=="Testificador"|| $validationUser->function=="Auditor entrenamiento")
                                                        <li class="nav-item">
                                                            <a class="nav-link" data-toggle="tab" href="#expedientetecnico" role="tab" aria-controls="contact" aria-selected="false"><i class="fas fa-file-archive" style="margin-right: 5px"></i> Expediente</a>
                                                        </li>
                                                @endif
                                            @endif
                                            <!-- --------------------Admin-------------- -->
                                            @if(in_array(Auth::user()->profile, ['Administrador']))
                                                @if($audit->end_expedient!=1)
                                                    <li class="nav-item">
                                                      <a class="nav-link" data-toggle="tab" href="#expediente" role="tab" aria-controls="contact" aria-selected="false"><i class="fas fa-file-archive" style="margin-right: 5px"></i> Expediente</a>
                                                    </li>
                                                @elseif($audit->end_expedient=="1")
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#expedientetecnico" role="tab" aria-controls="contact" aria-selected="false"><i class="fas fa-file-archive" style="margin-right: 5px"></i> Expediente</a>
                                                    </li>
                                                @endif
                                            @endif
                                            <!-- --------------------Profiles-------------- -->

                                            @if($validationUser)
                                                @if($validationUser->function=="Auditor lider" || $validationUser->function=="Auditor tecnico" || $validationUser->function=="Testificador" || $validationUser->function=="Auditor entrenamiento" || $validationUser->function=="Experto tecnico")
                                                    @if($audit->type=="Fase 2" || $audit->type=="Recertificacion")
                                                        <li class="nav-item">
                                                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#certificate{{$audit->id}}" role="tab" aria-controls="contact" aria-selected="false"><i class="far fa-file-pdf" style="margin-right: 5px"></i> Certificado</a>
                                                        </li>
                                                    @endif
                                                @endif

                                                @if($validationUser->function=="Auditor tecnico")
                                                    <a class="d-none">{{$contadorvalidation=0}}</a>

                                                    @foreach($audit->statuscheck as $validationEnd)
                                                        @if($validationEnd->status=="Cerrado")
                                                            @if($validationEnd->code)
                                                                <a class="d-none">{{$contadorvalidation += 1}}</a>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                      
                                                    @if(count($audit->statuscheck)==$contadorvalidation)
                                                      @if($audit->end_expedient==1)
                                                        <li class="nav-item finalizar" style="position: absolute; right: 16px ">
                                                          <button class="btn btn-success " type="button" href="#new" data-toggle="modal" data-animation="effect-sign" ><i class="fas fa-flag-checkered"></i> Finalizar auditoria</button>
                                                      	</li>
                                                     
                                                      @foreach($organizations->certification as $organizationdetail)

                                                        <div class="modal fade" id="start{{$audit->certification->id}}"  role="dialog" aria-hidden="true">
                                                          <div class="modal-dialog-centered modal-dialog moda" role="document">
                                                            <div class="modal-content">
                                                              <div class="modal-header pd-y-20 pd-x-20 pd-sm-x-30">
                                                                <a href="" role="button" class="close pos-absolute t-15 r-15" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                                                <div class="media align-items-center">
                                                                  <div id="preview" class="imagen text-center">
                                                                    <span class="tx-color-03 d-none d-sm-block"><i data-feather="flag" class="fas fa-user-plus wd-60 ht-60"></i></span>
                                                                  </div>
                                                                  <div class="media-body mg-sm-l-20">
                                                                    <h4 class="tx-18 tx-sm-20 mg-b-2">INICIAR AUDITORIA</h4>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                              <form class="form-horizontal" method="post" action="{{ route('saveauditrestart') }}" required="required">
                                                                {{ csrf_field() }}
                                                              <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30">
                                                                  <div class="form-group">
                                                                    <label >Organizacion</label>
                                                                    <input type="hidden"   class="form-control startValueCertification" name="id" value="{{$audit->certification->id}}">
                                                                    <input type="hidden"   class="form-control" name="idAudit" value="{{$audit->id}}">
                                                                    <input type="text" name="organization" id="organization" readonly disabled class="form-control" value="{{$organizations->name}}">
                                                                  </div>
                                                                  <div class="form-group">
                                                                    <label >Tipo de auditoria</label>
                                                                      <select class="form-control custom-select" style="width: 100%" id="type" name="type" required="required">
                                                                          <option selected value="{{$audits->type}}">{{$audits->type}}</option>
                                                                      </select>
                                                                  </div>
                                                                  <div class="form-group">
                                                                    <label >Sectores y Normas</label>
                                                                    <br>
                                                                      <table class="table table-bordered ">
                                                                        <tr>
                                                                          <td>Sector</td>
                                                                          <td>Norma</td>
                                                                        </tr>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                  @foreach($organizationdetail->sectorsandnorms as $sectorsandnorms)
                                                                                    @if($organizationdetail)
                                                                                      <li class="{{$organizationdetail->id}}{{$sectorsandnorms->sector->id}}SectorAgregarNombre">{{$sectorsandnorms->sector->name}}</li>
                                                                                  @endif
                                                                                @endforeach
                                                                                </td>
                                                                                <td>
                                                                                @foreach($organizationdetail->sectorsandnorms as $sectorsandnorms)
                                                                                  @if($organizationdetail)
                                                                                    <li class="{{$organizationdetail->id}}{{$sectorsandnorms->norm->id}}NormaAgregarNombre">{{$sectorsandnorms->norm->name}}</li>
                                                                                  @endif
                                                                                  <input type="hidden" name="sectores[]" value="{{$sectorsandnorms->sector->id}}">
                                                                                  <input type="hidden" name="norms[]" value="{{$sectorsandnorms->norm->id}}">
                                                                                @endforeach
                                                                                </td>
                                                                            </tr>
                                                                      </tbody>
                                                                    </table>
                                                                  </div>

                                                                  <div class="validation">
                                                                    <div class="form-group">
                                                                      <label >Fecha de inicio</label>
                                                                      <input type="date" name="date_start" class="form-control date_start fechas" required="required" >
                                                                    </div>
                                                                    <div class="form-group">
                                                                      <label >Fecha de termino</label>
                                                                      <input type="date" name="date_finish" class="form-control date_finish fechas" required="required" >
                                                                    </div>  
                                                                  </div>
                                                              </div>

                                                              <div class="modal-footer pd-x-20 pd-y-15">
                                                                <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fas fa-undo"></i> Cancelar</button>
                                                                <button type="submit" class="btn btn-primary btnValidation"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button>
                                                              </div>
                                                              
                                                            </form>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    @endforeach
                                                        @endif
                                                    @endif
                                                @endif

                                            @endif

                                            <!-- --------------------Admin-------------- -->
                                            @if(in_array(Auth::user()->profile, ['Administrador']))
                                                @if($audit->type=="Fase 2" || $audit->type=="Recertificacion")
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#certificate{{$audit->id}}" role="tab" aria-controls="contact" aria-selected="false"><i class="far fa-file-pdf" style="margin-right: 5px"></i> Certificado</a>
                                                    </li>
                                                @endif
                                            @endif

                                            @if(in_array(Auth::user()->profile, ['Administrador']))
                                                <a class="d-none">{{$contadorvalidation = 0}}</a>
                                                @foreach($audit->statuscheck as $validationEnd)
                                                    @if($validationEnd->status=="Cerrado")
                                                        @if($validationEnd->code)
                                                            <a class="d-none">{{$contadorvalidation += 1}}</a>
                                                        @endif
                                                    @endif
                                                @endforeach
                                                  
                                                @if(count($audit->statuscheck)==$contadorvalidation)
                                                    @if($audit->end_expedient==1)
                                                        <li class="nav-item finalizar" style="position: absolute; right: 16px ">
                                                            <button class="btn btn-success " type="button" href="#new" data-toggle="modal" data-animation="effect-sign" ><i class="fas fa-flag-checkered"></i> Finalizar auditoria</button>
                                                        </li>
                                                        @foreach($organizations->certification as $organizationdetail)

                                                          <div class="modal fade" id="start{{$audit->certification->id}}"  role="dialog" aria-hidden="true">
                                                            <div class="modal-dialog-centered modal-dialog moda" role="document">
                                                              <div class="modal-content">
                                                                <div class="modal-header pd-y-20 pd-x-20 pd-sm-x-30">
                                                                  <a href="" role="button" class="close pos-absolute t-15 r-15" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                                                                  <div class="media align-items-center">
                                                                    <div id="preview" class="imagen text-center">
                                                                      <span class="tx-color-03 d-none d-sm-block"><i data-feather="flag" class="fas fa-user-plus wd-60 ht-60"></i></span>
                                                                    </div>
                                                                    <div class="media-body mg-sm-l-20">
                                                                      <h4 class="tx-18 tx-sm-20 mg-b-2">INICIAR AUDITORIA</h4>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                                <form class="form-horizontal" method="post" action="{{ route('saveauditrestart') }}" required="required">
                                                                  {{ csrf_field() }}
                                                                <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30">
                                                                    <div class="form-group">
                                                                      <label >Organizacion</label>
                                                                      <input type="hidden"   class="form-control startValueCertification" name="id" value="{{$audit->certification->id}}">
                                                                      <input type="hidden"   class="form-control" name="idAudit" value="{{$audit->id}}">
                                                                      <input type="text" name="organization" id="organization" readonly disabled class="form-control" value="{{$organizations->name}}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                      <label >Tipo de auditoria</label>
                                                                        <select class="form-control custom-select" style="width: 100%" id="type" name="type" required="required">
                                                                            <option selected value="{{$audits->type}}">{{$audits->type}}</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                      <label >Sectores y Normas</label>
                                                                      <br>
                                                                        <table class="table table-bordered ">
                                                                          <tr>
                                                                            <td>Sector</td>
                                                                            <td>Norma</td>
                                                                          </tr>
                                                                          <tbody>
                                                                              <tr>
                                                                                  <td>
                                                                                    @foreach($organizationdetail->sectorsandnorms as $sectorsandnorms)
                                                                                      @if($organizationdetail)
                                                                                        <li class="{{$organizationdetail->id}}{{$sectorsandnorms->sector->id}}SectorAgregarNombre">{{$sectorsandnorms->sector->name}}</li>
                                                                                    @endif
                                                                                  @endforeach
                                                                                  </td>
                                                                                  <td>
                                                                                  @foreach($organizationdetail->sectorsandnorms as $sectorsandnorms)
                                                                                    @if($organizationdetail)
                                                                                      <li class="{{$organizationdetail->id}}{{$sectorsandnorms->norm->id}}NormaAgregarNombre">{{$sectorsandnorms->norm->name}}</li>
                                                                                    @endif
                                                                                    <input type="hidden" name="sectores[]" value="{{$sectorsandnorms->sector->id}}">
                                                                                    <input type="hidden" name="norms[]" value="{{$sectorsandnorms->norm->id}}">
                                                                                  @endforeach
                                                                                  </td>
                                                                              </tr>
                                                                        </tbody>
                                                                      </table>
                                                                    </div>

                                                                    <div class="validation">
                                                                      <div class="form-group">
                                                                        <label >Fecha de inicio</label>
                                                                        <input type="date" name="date_start" class="form-control date_start fechas" required="required" >
                                                                      </div>
                                                                      <div class="form-group">
                                                                        <label >Fecha de termino</label>
                                                                        <input type="date" name="date_finish" class="form-control date_finish fechas" required="required" >
                                                                      </div>  
                                                                    </div>
                                                                </div>

                                                                <div class="modal-footer pd-x-20 pd-y-15">
                                                                  <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fas fa-undo"></i> Cancelar</button>
                                                                  <button type="submit" class="btn btn-primary btnValidation"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button>
                                                                </div>
                                                                
                                                              </form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      @endforeach
                                                    @endif
                                                @endif
                                            @endif
                                        </ul>

                                        <div class="tab-content bd bd-gray-300 bd-t-0 pd-20" id="myTabContent">
                                           
                                            <div class="tab-pane fade" id="expedientetecnico" role="tabpanel" aria-labelledby="home-tab">
                                                <center>
                                                    <div class="col-sm-6 col-lg-4 col-xl-3 mg-t-10 mg-sm-t-0">
                                                        <div class="media media-folder"><i data-feather="folder"></i>
                                                            <div class="media-body">
                                                              <h6><a href="{{asset('/storage/'.substr($audit->expedient,7))}}"  target="_blank" class="link-02">Expediente.zip</a></h6>
                                                            </div><!-- media-body -->
                                                            <div class="dropdown-file">
                                                                <a href="" class="dropdown-link" data-toggle="dropdown"><i data-feather="more-vertical"></i></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="{{asset('/storage/'.substr($audit->expedient,7))}}"  target="_blank" class="dropdown-item download"><i data-feather="download"></i>Descargar</a>
                                                                </div>
                                                            </div><!-- dropdown -->
                                                        </div><!-- media -->
                                                    </div><!-- col -->
                                                </center>
                                            </div>
                                            <div class="tab-pane fade" id="expediente" role="tabpanel" aria-labelledby="home-tab">
                                                <h6>Expediente</h6>
                                                <div class="col-md-12 row">
                                                    <div class="col-md-6">
                                                        <form method="post" action="{{route('saveexpedient')}}" enctype="multipart/form-data">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="id" value="{{$audit->id}}">
                                                            <label class="btn btn-outline-primary btn-lg btn-block" for="my-file-selectors" >
                                                            <input id="my-file-selectors" type="file" style="display:none" onchange="$('#expedientespan').html(this.files[0].name); $('.subir').css('display', 'block');"  required="required" name="expediente" accept=".zip,.rar,.7zip">Elegir expediente</label>
                                                            <span class='label label-info' id="expedientespan">@if($audit->expedient) <i class="fas fa-check-circle text-success"></i> Expediente.zip  @else No hay archivos @endif </span>
                                                            <button class="btn btn-block btn-primary subir btn-block" style="display:none"><i class="fas fa-file-upload"></i> Subir expediente</button>
                                                        </form>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <form method="post" action="{{route('endexpedient')}}">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="id" value="{{$audit->id}}">
                                                            <button class="btn btn-block btn-success "><i class="fas fa-file-upload"></i> Finalizar expediente</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane fade show active" id="home{{$audit->id}}" role="tabpanel" aria-labelledby="home-tab">
                                                @if(in_array(Auth::user()->profile, ['Planeador','Administrador']))
                                                    <h6>Agregar equipo a la auditoria</h6>
                                                    <form method="post" action="{{route('saveteam')}}" class="saveteam">
                                                        <div class="form-row">
                                                            <div class="form-group col">
                                                                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">Funcion</label>
                                                                <select class="form-control custom-select" name="funcion" id="funcion" required >
                                                                    <option disabled selected="">Escoge una opción</option>
                                                                    <option value="Auditor lider">Auditor lider</option>
                                                                    <option value="Auditor">Auditor</option>
                                                                    <option value="Auditor tecnico">Experto técnico</option>
                                                                    <option value="Auditor entrenamiento">Auditor en entrenamiento</option>
                                                                    <option value="Testificador">Testificador</option>
                                                                    <option value="Registro">Registro</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group col">
                                                                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">Auditor</label>
                                                                <select class="form-control custom-select"  id="auditor" name="auditor" required>
                                                                    <option disabled selected="">Escoge una opción</option>
                                                                    @foreach($auditors as $auditor)
                                                                        <option value="{{$auditor->id}}">{{$auditor->name}} {{$auditor->lastname}} {{$auditor->secondname}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                            <div class="form-group col">
                                                                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">Fecha inicio</label>
                                                                <input class="form-control" type="date" name="fecha" id="fecha" required>
                                                            </div>

                                                            <div class="form-group col">
                                                                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">Fecha termino</label>
                                                                <input class="form-control" type="date" name="fecha_end" id="fecha" required>
                                                            </div>
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="form-group col-md-6">
                                                                <label  class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">¿Cuantas horas?</label>
                                                                <input type="number" class="form-control" id="horas" placeholder="¿Cuantas Horas?" name="horas" required>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label  class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">¿Cuantos minutos?</label>
                                                                <input type="number" class="form-control" id="minutos" placeholder="¿Cuantos minutos?" name="minutos" required>
                                                            </div>

                                                        </div>
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="id" value="{{$audit->id}}">
                                                        <button type="submit" class="btn  btn-dark tx-spacing-1 tx-semibold  col-lg-2 text-white newbutton " style="float: right;"><i class="fas fa-plus-circle"></i> Agregar </button>

                                                        <br>
                                                        <br>
                                                        <br>
                                                  </form>
                                                @endif
                                                <h5 class="tx-20 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03 text-center">Equipo de trabajo</h5>
                                                <table id="tabla" class="table table-bordered text-center">
                                                    <thead>
                                                      <tr>
                                                        <td>Nº</td>
                                                        <td>Funcion</td>
                                                        <td>Auditor</td>
                                                        <td>Fecha inicio</td>
                                                        <td>Fecha termino</td>
                                                        <td>Horas</td>
                                                        <td>Minutos</td>
                                                        @if(!in_array(Auth::user()->profile, ['Auditor']))
                                                          <td>Quitar</td>
                                                        @endif
                                                      </tr>
                                                    </thead>
                                                    <tbody >
                                                       @foreach($audit->teams as $ad)
                                                            <tr>
                                                              <th>{{ ++$loop->index}}</th>
                                                              <th scope="row">{{$ad->function}}</th>
                                                              <th scope="row">{{$ad->user->name}} {{$ad->user->lastname}} {{$ad->user->secondname}}</th>
                                                              <th scope="row">{{$ad->date}}</th>
                                                              <th scope="row">{{$ad->date_end}}</th>
                                                              <th scope="row">{{$ad->hour}}</th>
                                                              <th scope="row">{{$ad->minute}}</th>
                                                              @if(!in_array(Auth::user()->profile, ['Auditor']))
                                                                    <th scope="row">
                                                                      <form method="post" action="{{route('deleteteam')}}">
                                                                        {{csrf_field()}}
                                                                        <input type="hidden" name="id" value="{{$ad->id}}">
                                                                        <button type="submit" style="background-color: white; border:none;">
                                                                          <i class="fas fa-trash-alt" style="color: red"></i>
                                                                        </button>
                                                                      </form>
                                                                    </th>
                                                                  </tr>
                                                              @endif
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="tab-pane fade" id="profile{{$audit->id}}" role="tabpanel" aria-labelledby="profile-tab">
                                                <div class="float-right">
                                                    <input type="checkbox" data-toggle="toggle" class="mr-2 mostrarToggle" style="transform: scale(1.5);"> Mostrar preguntas de AYUDA 
                                                </div>
                                                <h6>Checklist</h6> 
                                                <ul class="nav nav-line" role="tablist">
                                                    @foreach($audit->auditsector as $sectors)
                                                        @if(in_array(Auth::user()->profile, ['Administrador']))
                                                            @foreach($sectors->statusCheck as $statuscheck)
                                                                <input type="hidden" name="resultadocheck[]" value="{{$statuscheck->status}}" class="resultadocheck">
                                                                @if($statuscheck->status=="Cerrado")
                                                                    <li class="nav-item pl-3">
                                                                        <input type="hidden" name="contador" value="1" class="contador">
                                                                        <a class="nav-link cambio" id="profile-tab5" data-toggle="tab" href="#auditxxxx{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->norm->name}} <i class="fas fa-check-circle text-success"></i></a>
                                                                    </li>
                                                                @else
                                                                    <li class="nav-item pl-3">
                                                                        <input type="hidden" name="contador" value="0" class="contador">
                                                                        <a class="nav-link cambio" id="profile-tab5" data-toggle="tab" href="#auditxxxx{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->norm->name}} <i class="fas fa-info-circle text-danger"></i></a>
                                                                    </li>
                                                                @endif
                                                            @endforeach
                                                        @endif

                                                        @if($validationUser)
                                                            @if($validationUser->function=="Auditor lider")
                                                                @foreach($sectors->statusCheck as $statuscheck)
                                                                    <input type="hidden" name="resultadocheck[]" value="{{$statuscheck->status}}" class="resultadocheck">
                                                                    @if($statuscheck->status=="Cerrado")
                                                                        <li class="nav-item pl-3">
                                                                            <input type="hidden" name="contador" value="1" class="contador">
                                                                            <a class="nav-link cambio" id="profile-tab5" data-toggle="tab" href="#auditxxxx{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->norm->name}} <i class="fas fa-check-circle text-success"></i></a>
                                                                        </li>
                                                                    @else
                                                                        <li class="nav-item pl-3">
                                                                            <input type="hidden" name="contador" value="0" class="contador">
                                                                            <a class="nav-link cambio" id="profile-tab5" data-toggle="tab" href="#auditxxxx{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->norm->name}} <i class="fas fa-info-circle text-danger"></i></a>
                                                                        </li>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif

                                                        @if($validationUser)
                                                            @foreach($filterNorm as $norm_id => $normaAuditoria)
                                                                @if(array_key_exists($norm_id,$filterUserNorm))
                                                                    @if($validationUser->function=="Auditor")
                                                                        @foreach($sectors->statusCheck as $statuscheck)
                                                                            @if(array_key_exists($sectors->norm->id,$filterUserNorm))
                                                                                <input type="hidden" name="resultadocheck[]" value="{{$statuscheck->status}}" class="resultadocheck">
                                                                                @if($statuscheck->status=="Cerrado")
                                                                                    <li class="nav-item pl-3">
                                                                                        <input type="hidden" name="contador" value="1" class="contador">
                                                                                        <a class="nav-link cambio" id="profile-tab5" data-toggle="tab" href="#auditxxxx{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->norm->name}} <i class="fas fa-check-circle text-success"></i></a>
                                                                                    </li>
                                                                                @else
                                                                                    <li class="nav-item pl-3">
                                                                                        <input type="hidden" name="contador" value="0" class="contador">
                                                                                        <a class="nav-link cambio" id="profile-tab5" data-toggle="tab" href="#auditxxxx{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->norm->name}} <i class="fas fa-info-circle text-danger"></i></a>
                                                                                    </li>
                                                                                @endif
                                                                            @endif
                                                                        @endforeach
                                                                    @elseif($validationUser->function=="Auditor tecnico" || $validationUser->function=="Testificador" || $validationUser->function=="Auditor entrenamiento" || $validationUser->function=="Experto tecnico")
                                                                        @foreach($sectors->statusCheck as $statuscheck)
                                                                            <input type="hidden" name="resultadocheck[]" value="{{$statuscheck->status}}" class="resultadocheck">
                                                                            @if($statuscheck->status=="Cerrado")
                                                                                <li class="nav-item pl-3">
                                                                                    <input type="hidden" name="contador" value="1" class="contador">
                                                                                    <a class="nav-link cambio" id="profile-tab5" data-toggle="tab" href="#auditxxxx{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->sector->name}}---{{$sectors->norm->name}} <i class="fas fa-check-circle text-success"></i></a>
                                                                                </li>
                                                                            @else
                                                                                <li class="nav-item pl-3">
                                                                                    <input type="hidden" name="contador" value="1" class="contador">
                                                                                    <a class="nav-link cambio" id="profile-tab5" data-toggle="tab" href="#auditxxxx{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->sector->name}}---{{$sectors->norm->name}} <i class="fas fa-exclamation-triangle text-warning"></i> <strong>En captura</strong> <i class="fas fa-exclamation-triangle text-warning"></i> </a>
                                                                                </li>
                                                                            @endif
                                                                        @endforeach
                                                                    @elseif($validationUser->function=="Registro")
                                                                        @foreach($sectors->statusCheck as $statuscheck)
                                                                            <input type="hidden" name="resultadocheck[]" value="{{$statuscheck->status}}" class="resultadocheck">
                                                                                @if($statuscheck->status=="Cerrado")
                                                                                    <li class="nav-item pl-3">
                                                                                        <input type="hidden" name="contador" value="1" class="contador">
                                                                                        <a class="nav-link cambio" id="profile-tab5" data-toggle="tab" href="#auditxxxx{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->sector->name}}---{{$sectors->norm->name}} <i class="fas fa-check-circle text-success"></i></a>
                                                                                    </li>
                                                                                @else
                                                                                    <li class="nav-item pl-3">
                                                                                        <input type="hidden" name="contador" value="1" class="contador">
                                                                                        <a class="nav-link cambio" id="profile-tab5" data-toggle="tab" href="#auditxxxx{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->sector->name}}---{{$sectors->norm->name}} <i class="fas fa-exclamation-triangle text-warning"></i> <strong>En captura</strong> <i class="fas fa-exclamation-triangle text-warning"></i> </a>
                                                                                    </li>
                                                                                @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        @endif

                                                    @endforeach
                                                </ul>

                                                <div class="tab-content mg-t-20" id="myTabContent5">
                                                    @foreach($audit->auditsector as $sectors)
                                                        <div class="tab-pane fade" id="auditxxxx{{$sectors->id}}" role="tabpanel" aria-labelledby="profile-tab5">
                                                            <form class="checklistForm">
                                                                @foreach($audit->statuscheck as $statusCheck)
                                                                    @if($statusCheck->audit_sector_norm_id==$sectors->id)
                                                                        <input type="hidden" name="id"  value="{{$statusCheck->id}}" class="idStatuscheck">
                                                                    @endif
                                                                @endforeach

                                                                <div id="accordion" role="tablist" aria-multiselectable="true">
                                                                    @foreach($audit->sections as $ad)
                                                                        @if($ad->audit_sector_norm_id==$sectors->id)
                                                                            <div class="card mb-4 validationInputCard">
                                                                                <h5 class="card-header" role="tab" >
                                                                                    <a class="collapsed d-block " data-toggle="collapse" data-parent="#accordion" href="#collapse{{$ad->id}}" >
                                                                                    {{$ad->description}} <span class="inforestantes onloadValidation{{$ad->id}} text-danger"></span>
                                                                                    <i class="fa fa-chevron-down pull-right" style="float: right;"></i>
                                                                                  </a>
                                                                              </h5>
                                                                                @foreach($ad->resultChecklist as $checklist)
                                                                                    <input type="hidden" value="{{$checklist->id}}" name="idChecklist[]" class="idChecklist">
                                                                                    <div id="collapse{{$ad->id}}" class="collapse" role="tabpanel" >
                                                                                        <div class="card-body">
                                                                                            <div class="card">
                                                                                                <div class="card-header">{{$checklist->code}}
                                                                                                    {{$checklist->description}} <span class="ayuda d-none">{{$checklist->help}}</span>
                                                                                                    @if($checklist->conform=="NA")
                                                                                                      <div class="row parameters" style="float: right;">
                                                                                                          <label class="text-warning" ><i class="fas fa-info-circle "></i> NA</label>
                                                                                                      </div>
                                                                                                    @elseif($checklist->conform=="C")
                                                                                                      <div class="row" style="float: right;">
                                                                                                          <label class="text-success"> <i class="fas fa-check-circle text-success"></i> Cumple</label>
                                                                                                      </div>
                                                                                                    @elseif($checklist->conform=="NC")
                                                                                                      <div class="row parameters" style="float: right;">
                                                                                                          <label class="text-danger"><i class="fas fa-times-circle"></i> No cumple</label>
                                                                                                      </div>
                                                                                                    @else
                                                                                                        <input type="hidden" value="{{$checklist->id}}" name="idChecklist[]" class="idChecklist13">
                                                                                                        <div class="row parameters" style="float: right;">
                                                                                                            <div class="custom-control custom-checkbox">
                                                                                                              <input type="checkbox" class="custom-control-input notapli NaCheck" id="check{{$checklist->id}}" name="NaCheck[]">
                                                                                                              <label class="custom-control-label " for="check{{$checklist->id}}">NA</label>
                                                                                                            </div>
                                                                                                            <div class="custom-control custom-switch pl-5 conform" >
                                                                                                              <input type="checkbox" class="custom-control-input yesornou conformCheck" id="{{$checklist->id}}" name="conformCheck[]" checked>
                                                                                                              <label class="custom-control-label" for="{{$checklist->id}}">Cumple</label>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    @endif
                                                                                                    <hr>
                                                                                                    @if(strlen($checklist->observation)>=1)
                                                                                                        <textarea class="form-control  pt-3" rows="4" placeholder="Escriba algo..." name="observationCheck[]" disabled>{{$checklist->observation}}</textarea>
                                                                                                    @else
                                                                                                        <textarea class="form-control observationCheck pt-3" rows="4" placeholder="Escriba algo..." name="observationCheck[]">{{$checklist->observation}}</textarea>
                                                                                                        <div class="noconformidadesvalidacion  d-none">
                                                                                                            <hr style="background-color: red">
                                                                                                            <div class="form-group row">
                                                                                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Requisito:</label>
                                                                                                                <div class="col-sm-10">
                                                                                                                  <input type="text" class="form-control requisitosnc" value="{{$checklist->code}}{{$checklist->description}}" readonly name="requisitosnc[]">
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="form-group row">
                                                                                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Descripción de la no conformidad:</label>
                                                                                                                <div class="col-sm-10">
                                                                                                                    <textarea class="form-control descriptionnc" name="descriptionnc[]"  rows="4" placeholder="Escriba algo..." ></textarea>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            <div class="form-group row">
                                                                                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Tipo de NC:</label>
                                                                                                                <div class="col-sm-10">
                                                                                                                  <select class="form-control  custom-select ncnc" name="ncnc[]">
                                                                                                                    <option selected disabled>--Elige alguna --</option>
                                                                                                                    <option value="Mayor">Mayor</option>
                                                                                                                    <option value="Menor">Menor</option>
                                                                                                                    <option value="Critica">Critica</option>
                                                                                                                  </select>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    @endif
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @endforeach
                                                                                <div class="card-body">
                                                                                    <div class="card">
                                                                                        <div class="card-header">Resumen por seccion</div>
                                                                                        <div class="card-body">
                                                                                            <textarea class="form-control observationSection"  rows="4" placeholder="Escriba algo..." name="observationSection[]">{{$ad->observation}}</textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>                                                    
                                                                            </div>  
                                                                        @endif  
                                                                    @endforeach
                                                                </div>

                                                                @if(in_array(Auth::user()->profile, ['Administrador','Auditor']))
                                                                    <div class="col-md-12 row">
                                                                        <div class="col-md-6"><button class="btn btn-block btn-primary send"><i class="fas fa-file-alt"></i> Guardar checklist</button></div>
                                                                        <div class="col-md-6"><button class="btn btn-block btn-success endchecklist"><i class="fas fa-file-alt"></i> Finalizar</button></div>
                                                                    </div>
                                                                @endif
                                                            </form>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>

                                            <div class="tab-pane fade" id="contact{{$audit->id}}" role="tabpanel" aria-labelledby="contact-tab">
                                                <h6>Resultado</h6>
                                                <ul class="nav nav-line" role="tablist">
                                                    @if(in_array(Auth::user()->profile, ['Administrador']))
                                                        @foreach($audit->auditsector as $sectors)
                                                            @foreach($sectors->statusCheck as $statuscheck)
                                                                <input type="hidden" name="resultadocheck[]" value="{{$statuscheck->status}}" class="resultadocheck">
                                                                @if($statuscheck->code)
                                                                    <li class="nav-item pl-3">
                                                                        <input type="hidden" name="contador" value="1" class="contador">
                                                                        <a class="nav-link cambio traerresultado" id="profile-tab5" data-toggle="tab" href="#auditresultado{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->norm->name}} <i class="fas fa-check-circle text-success"></i></a>
                                                                    </li>
                                                                @else
                                                                    <li class="nav-item pl-3">
                                                                        <input type="hidden" name="contador" value="0" class="contador">
                                                                        <a class="nav-link cambio" id="profile-tab5" data-toggle="tab" href="#auditresultado{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->norm->name}} <i class="fas fa-info-circle text-danger"></i></a>
                                                                    </li>
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    @endif

                                                    @if($validationUser)
                                                        @if($validationUser->function=="Auditor lider")
                                                            @foreach($audit->auditsector as $sectors)
                                                                @foreach($sectors->statusCheck as $statuscheck)
                                                                    <input type="hidden" name="resultadocheck[]" value="{{$statuscheck->status}}" class="resultadocheck">
                                                                    @if($statuscheck->code)
                                                                        <li class="nav-item pl-3">
                                                                            <input type="hidden" name="contador" value="1" class="contador">
                                                                            <a class="nav-link cambio traerresultado" id="profile-tab5" data-toggle="tab" href="#auditresultado{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->norm->name}} <i class="fas fa-check-circle text-success"></i></a>
                                                                        </li>
                                                                    @else
                                                                        <li class="nav-item pl-3">
                                                                            <input type="hidden" name="contador" value="0" class="contador">
                                                                            <a class="nav-link cambio" id="profile-tab5" data-toggle="tab" href="#auditresultado{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->norm->name}} <i class="fas fa-info-circle text-danger"></i></a>
                                                                        </li>
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                            
                                                        @elseif($validationUser->function=="Auditor")
                                                            @foreach($filterNorm as $norm_id => $normaAuditoria)
                                                                @if(array_key_exists($norm_id,$filterUserNorm))
                                                                    @foreach($audit->auditsector as $sectors)
                                                                        @foreach($sectors->statusCheck as $statuscheck)
                                                                            <input type="hidden" name="resultadocheck[]" value="{{$statuscheck->status}}" class="resultadocheck">
                                                                            @if(array_key_exists($sectors->norm->id,$filterUserNorm))
                                                                                @if($statuscheck->code)
                                                                                  <li class="nav-item pl-3">
                                                                                      <input type="hidden" name="contador" value="1" class="contador">
                                                                                      <a class="nav-link cambio traerresultado" id="profile-tab5" data-toggle="tab" href="#auditresultado{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->norm->name}} <i class="fas fa-check-circle text-success"></i></a>
                                                                                  </li>
                                                                                @else
                                                                                  <li class="nav-item pl-3">
                                                                                      <input type="hidden" name="contador" value="1" class="contador">
                                                                                      <a class="nav-link cambio" id="profile-tab5" data-toggle="tab" href="#auditresultado{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->norm->name}} <i class="fas fa-info-circle text-danger"></i></a>
                                                                                  </li>
                                                                                @endif
                                                                            @endif
                                                                      @endforeach
                                                                    @endforeach
                                                                @endif
                                                            @endforeach

                                                        @elseif($validationUser->function=="Auditor tecnico" || $validationUser->function=="Testificador" || $validationUser->function=="Auditor entrenamiento" || $validationUser->function=="Experto tecnico")
                                                            @foreach($audit->auditsector as $sectors)
                                                                @foreach($sectors->statusCheck as $statuscheck)
                                                                    <input type="hidden" name="resultadocheck[]" value="{{$statuscheck->status}}" class="resultadocheck">
                                                                    @if($statuscheck->code)
                                                                        <li class="nav-item pl-3">
                                                                            <input type="hidden" name="contador" value="1" class="contador">
                                                                            <a class="nav-link cambio traerresultado" id="profile-tab5" data-toggle="tab" href="#auditresultado{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->norm->name}} <i class="fas fa-check-circle text-success"></i></a>
                                                                        </li>
                                                                    @endif
                                                                @endforeach
                                                            @endforeach

                                                        @elseif($validationUser->function=="Registro")
                                                            @foreach($audit->auditsector as $sectors)
                                                                @foreach($sectors->statusCheck as $statuscheck)
                                                                    <input type="hidden" name="resultadocheck[]" value="{{$statuscheck->status}}" class="resultadocheck">
                                                                    @if($statuscheck->code)
                                                                        <li class="nav-item pl-3">
                                                                            <input type="hidden" name="contador" value="1" class="contador">
                                                                            <a class="nav-link cambio traerresultado" id="profile-tab5" data-toggle="tab" href="#auditresultado{{$sectors->id}}" role="tab" aria-controls="profile" aria-selected="false">{{$sectors->norm->name}} <i class="fas fa-check-circle text-success"></i></a>
                                                                        </li>
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                </ul>

                                                <div class="tab-content mg-t-20" id="myTabContent5">
                                                    @foreach($audit->auditsector as $sectors)
                                                        <div class="tab-pane fade" id="auditresultado{{$sectors->id}}" role="tabpanel" aria-labelledby="profile-tab5">
                                                            <div id="accordion" role="tablist" aria-multiselectable="true">
                                                                @foreach($audit->statuscheck as $statusCheck)
                                                                    @if($statusCheck->audit_sector_norm_id==$sectors->id)
                                                                        @if($statusCheck->alcance || $statusCheck->exclusions)
                                                                            <div class="col-sm-12">
                                                                                <div class="card">
                                                                                    <div class="card-header bg-dark text-white"> 
                                                                                        No conformidades potenciales para la auditoria de certificación 
                                                                                        @if($audit->type=='Fase 1')
                                                                                          Fase 2
                                                                                        @elseif($audit->type=='Fase 2')
                                                                                          Seguimiento 1
                                                                                        @elseif($audit->type=='Seguimiento 1')
                                                                                          Seguimiento 2
                                                                                        @elseif($audit->type=='Seguimiento 2')
                                                                                          Recertificacion
                                                                                        @else($audit->type=='Recertificacion')
                                                                                          Fase 1
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="card-body">
                                                                                          <table class="table table-striped table-bordered table-sm">
                                                                                            <thead>
                                                                                              <tr>
                                                                                                <th scope="col">Requisito</th>
                                                                                                <th scope="col">Descripción de la no conformidad</th>
                                                                                                <th scope="col">Tipo de NC:</th>
                                                                                              </tr>
                                                                                            </thead>
                                                                                            <tbody class="trtabla">
                                                                                               @foreach($statusCheck->conform as $conforms)
                                                                                                <tr>
                                                                                                  <td>{{$conforms->code}}</td>
                                                                                                  <td>{{$conforms->description}}</td>
                                                                                                  <td>{{$conforms->type_nc}}</td>
                                                                                                </tr>
                                                                                              @endforeach
                                                                                            </tbody>
                                                                                          </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <br>
                                                                            <div class="col-sm-12">
                                                                                <div class="card">
                                                                                    <div class="card-header bg-dark text-white"> Detalles</div>
                                                                                    <div class="card-body">
                                                                                            {{ csrf_field() }}
                                                                                            <input type="hidden" name="id"  value="{{$statusCheck->id}}">

                                                                                            <div class="form-group row">
                                                                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Alcance:</label>
                                                                                                <div class="col-sm-10">
                                                                                                    <textarea class="form-control" placeholder="Escriba algo..." name="alcance"  rows="4"  readonly>{{$statusCheck->alcance}}</textarea>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="form-group row">
                                                                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Exclusiones </label>
                                                                                                <div class="col-sm-10">
                                                                                                    <textarea class="form-control" placeholder="Escriba algo..." name="exclusiones"  rows="4"  readonly>{{$statusCheck->exclusions}}</textarea>
                                                                                                </div>
                                                                                            </div>
                                                                                         
                                                                                            @if(count($statusCheck->conform) >=1)
                                                                                                <div class="form-group row">
                                                                                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Acciones de las no conformidades</label>
                                                                                                    <div class="col-sm-10">
                                                                                                        <textarea class="form-control" placeholder="Escriba algo..." name="actionNoConforms"  rows="4" readonly>{{$statusCheck->actionNoConforms}}</textarea>
                                                                                                    </div>
                                                                                                </div>
                                                                                            @endif
                                                                                            <div class="form-group row">
                                                                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Código EAC/IAF:</label>
                                                                                                <div class="col-sm-10">
                                                                                                    <input type="text" class="form-control"  value="{{$statusCheck->detalleNormaSector->sector_id}}" readonly>
                                                                                                    <input type="hidden" class="form-control" name="code" value="{{$statusCheck->detalleNormaSector->sector_id}}">
                                                                                                </div>
                                                                                            </div>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <br>
                                                                            <br>
                                                                            <center>
                                                                                <div class="col-sm-6">
                                                                                    <h6 class="resultadoAjax"></h6>
                                                                                    <div class="card card-file">
                                                                                        <div class="card-file-thumb tx-danger">
                                                                                          <i class="far fa-file-pdf"></i>
                                                                                        </div>
                                                                                        <div class="card-body">
                                                                                            <h6 class="text-center"><a class="link-02 text-secondary">Resultado de Checklist.pdf</a></h6>
                                                                                            <form method="post" action="{{route('file')}}" class="finalizarauditoriavalidation" target="_blank">
                                                                                                  {{ csrf_field() }}
                                                                                                  <input type="hidden" name="id" value="{{$audit->id}}" class="auditIdResult">
                                                                                                  <input type="hidden" name="sector" value="{{$sectors->id}}" class="sectorIdResult">
                                                                                                  <button class="btn btn-success btn-block"><i data-feather="download"></i> Descargar</button>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                          </center>
                                                                        @else
                                                                            @if(count($statusCheck->conform) >=1)
                                                                                <div class="col-sm-12">
                                                                                    <div class="card">
                                                                                        <div class="card-header bg-dark text-white"> 
                                                                                            No conformidades potenciales para la auditoria de certificación 
                                                                                            @if($audit->type=='Fase 1')
                                                                                              Fase 2
                                                                                            @elseif($audit->type=='Fase 2')
                                                                                              Seguimiento 1
                                                                                            @elseif($audit->type=='Seguimiento 1')
                                                                                              Seguimiento 2
                                                                                            @elseif($audit->type=='Seguimiento 2')
                                                                                              Recertificacion
                                                                                            @else($audit->type=='Recertificacion')
                                                                                              Fase 1
                                                                                            @endif
                                                                                        </div>
                                                                                        <div class="card-body">
                                                                                            <form method="post" action="{{route('savenoconform')}}">
                                                                                              <input type="hidden" name="id"  value="{{$statusCheck->id}}" class="idStatuscheck">
                                                                                              <input type="hidden" class="exclusiones" name="exclusiones">
                                                                                              <input type="hidden" class="code" name="code">
                                                                                              <table class="table table-striped table-bordered table-sm">
                                                                                                <thead>
                                                                                                  <tr>
                                                                                                    <th scope="col">Requisito</th>
                                                                                                    <th scope="col">Descripción de la no conformidad</th>
                                                                                                    <th scope="col">Tipo de NC:</th>
                                                                                                  </tr>
                                                                                                </thead>
                                                                                                <tbody class="trtabla">
                                                                                                   @foreach($statusCheck->conform as $conforms)
                                                                                                    <tr>
                                                                                                      <td>{{$conforms->code}}</td>
                                                                                                      <td>{{$conforms->description}}</td>
                                                                                                      <td>{{$conforms->type_nc}}</td>
                                                                                                    </tr>
                                                                                                  @endforeach
                                                                                                </tbody>
                                                                                              </table>
                                                                                              <!-- <button type="submit" class="btn btn-primary btn-block mt-2" style="float: right;"> <i class="fas fa-save"></i> Agregar </button> -->
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            <br>
                                                                        @endif
                                                                        <div class="col-sm-12">
                                                                            <div class="card">
                                                                                <div class="card-header bg-dark text-white"> Detalles</div>
                                                                                <div class="card-body">
                                                                                    <form method="post" action="{{route('savenoconform')}}">
                                                                                        {{ csrf_field() }}
                                                                                        <input type="hidden" name="id"  value="{{$statusCheck->id}}">

                                                                                        <div class="form-group row">
                                                                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Alcance:</label>
                                                                                            <div class="col-sm-10">
                                                                                                <textarea class="form-control" placeholder="Escriba algo..." name="alcance"  rows="4" required >{{$statusCheck->alcance}}</textarea>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group row">
                                                                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Exclusiones </label>
                                                                                            <div class="col-sm-10">
                                                                                                <textarea class="form-control" placeholder="Escriba algo..." name="exclusiones"  rows="4" required >{{$statusCheck->exclusions}}</textarea>
                                                                                            </div>
                                                                                        </div>

                                                                                        @if(count($statusCheck->conform) >=1)
                                                                                            <div class="form-group row">
                                                                                                <label for="inputEmail3" class="col-sm-2 col-form-label">Acciones de las no conformidades</label>
                                                                                                <div class="col-sm-10">
                                                                                                    <textarea class="form-control" placeholder="Escriba algo..." name="actionNoConforms"  rows="4" required >{{$statusCheck->actionNoConforms}}</textarea>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif
                                                                                        
                                                                                        <div class="form-group row">
                                                                                            <label for="inputEmail3" class="col-sm-2 col-form-label">Código EAC/IAF:</label>
                                                                                            <div class="col-sm-10">
                                                                                                <input type="text" class="form-control"  value="{{$statusCheck->detalleNormaSector->sector_id}}" readonly >
                                                                                                <input type="hidden" class="form-control" name="code" value="{{$statusCheck->detalleNormaSector->sector_id}}">
                                                                                            </div>
                                                                                        </div>


                                                                                        <button type="submit" class="btn btn-success btn-block mb-2" style="float: right;"> <i class="fas fa-save"></i> Cerrar checklist</button>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                @endif  
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            @extends('certification.endaudit')
                                        </div>


                                        @if(count($audit->certificates)==0)
                                            @if($audit->type=="Fase 2")
                                                @if(in_array(Auth::user()->profile, ['Administrador']))
                                                    <div class="tab-pane fade " id="certificate{{$audit->id}}" role="tabpanel" aria-labelledby="contact-tab">
                                                        <form method="post" action="{{route('savefilecertificate')}}" enctype="multipart/form-data">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="id" value="{{$audit->id}}">
                                                            <input type="hidden" name="type" value="Fase 2">
                                                            <div class="form-group">
                                                              <label >Fecha inicio</label>
                                                              <input type="date" class="form-control" name="datestart" required="required">
                                                            </div>
                                                            <div class="form-group">
                                                              <label >Fecha termino</label>
                                                              <input type="date" class="form-control" name="dateend" required="required">
                                                            </div>
                                                              <h6>Certificado</h6>
                                                                <label class="btn btn-outline-primary btn-lg btn-block" for="my-file-selector" >
                                                                    <input id="my-file-selector" type="file" style="display:none"
                                                                    onchange="$('#upload-file-info').html(this.files[0].name); $('.subir').css('display', 'block');"  required="required" name="certificadopdf" accept="application/pdf">
                                                                    Elegir certificado
                                                                </label>
                                                                <span class='label label-info' id="upload-file-info"></span>
                                                                <button class="btn btn-block btn-primary subir" style="display:none"><i class="fas fa-file-upload"></i> Subir certificado</button>
                                                        </form>
                                                    </div>        
                                                @endif
                                                
                                                @foreach($audit->teams as $ad)
                                                    @if(Auth::user()->id==$ad->user_id)
                                                        @if($ad->function=="Auditor tecnico"|| $ad->function=="Testificador" || $ad->function=="Auditor entrenamiento" || $ad->function=="Experto tecnico")
                                                            <div class="tab-pane fade " id="certificate{{$audit->id}}" role="tabpanel" aria-labelledby="contact-tab">
                                                              <form method="post" action="{{route('savefilecertificate')}}" enctype="multipart/form-data">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="id" value="{{$audit->id}}">
                                                                <input type="hidden" name="type" value="Fase 2">
                                                                <div class="form-group">
                                                                  <label >Fecha inicio</label>
                                                                  <input type="date" class="form-control" name="datestart" required="required">
                                                                </div>
                                                                <div class="form-group">
                                                                  <label >Fecha termino</label>
                                                                  <input type="date" class="form-control" name="dateend" required="required">
                                                                </div>
                                                                  <h6>Certificado</h6>
                                                                    <label class="btn btn-outline-primary btn-lg btn-block" for="my-file-selector" >
                                                                        <input id="my-file-selector" type="file" style="display:none"
                                                                        onchange="$('#upload-file-info').html(this.files[0].name); $('.subir').css('display', 'block');"  required="required" name="certificadopdf" accept="application/pdf">
                                                                        Elegir certificado
                                                                    </label>
                                                                    <span class='label label-info' id="upload-file-info"></span>
                                                                    @if($ad->function=="Auditor tecnico")
                                                                        <button class="btn btn-block btn-primary subir" style="display:none"><i class="fas fa-file-upload"></i> Subir certificado</button>
                                                                    @endif
                                                                </form>
                                                            </div>        
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @elseif($audit->type=="Recertificacion")
                                                @if(in_array(Auth::user()->profile, ['Administrador']))
                                                    <div class="tab-pane fade " id="certificate{{$audit->id}}" role="tabpanel" aria-labelledby="contact-tab">
                                                      <form method="post" action="{{route('savefilecertificate')}}" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" value="{{$audit->id}}">
                                                        <input type="hidden" name="type" value="Recertificacion">
                                                        <div class="form-group">
                                                          <label >Fecha inicio</label>
                                                          <input type="date" class="form-control" name="datestart" required="required">
                                                        </div>
                                                        <div class="form-group">
                                                          <label >Fecha termino</label>
                                                          <input type="date" class="form-control" name="dateend" required="required">
                                                        </div>
                                                          <h6>Certificado</h6>
                                                            <label class="btn btn-outline-primary btn-lg btn-block" for="my-file-selector" >
                                                                <input id="my-file-selector" type="file" style="display:none"
                                                                onchange="$('#upload-file-info').html(this.files[0].name); $('.subir').css('display', 'block');" required="required" name="certificadopdf" accept="application/pdf">
                                                                Elegir certificado
                                                            </label>
                                                            <span class='label label-info' id="upload-file-info"></span>
                                                            <button class="btn btn-block btn-primary subir" style="display:none"><i class="fas fa-file-upload"></i> Subir certificado</button>
                                                        </form>
                                                    </div>
                                                @endif
                                                @foreach($audit->teams as $ad)
                                                    @if(Auth::user()->id==$ad->user_id)
                                                        @if($ad->function=="Auditor tecnico"|| $ad->function=="Testificador" || $ad->function=="Auditor entrenamiento" || $ad->function=="Experto tecnico")
                                                            <div class="tab-pane fade " id="certificate{{$audit->id}}" role="tabpanel" aria-labelledby="contact-tab">
                                                                <form method="post" action="{{route('savefilecertificate')}}" enctype="multipart/form-data">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="id" value="{{$audit->id}}">
                                                                    <input type="hidden" name="type" value="Recertificacion">
                                                                    <div class="form-group">
                                                                      <label >Fecha inicio</label>
                                                                      <input type="date" class="form-control" name="datestart" required="required">
                                                                    </div>

                                                                    <div class="form-group">
                                                                      <label >Fecha termino</label>
                                                                      <input type="date" class="form-control" name="dateend" required="required">
                                                                    </div>

                                                                    <h6>Certificado</h6>

                                                                    <label class="btn btn-outline-primary btn-lg btn-block" for="my-file-selector" >
                                                                        <input id="my-file-selector" type="file" style="display:none"
                                                                        onchange="$('#upload-file-info').html(this.files[0].name); $('.subir').css('display', 'block');" required="required" name="certificadopdf" accept="application/pdf">
                                                                        Elegir certificado
                                                                    </label>

                                                                    <span class='label label-info' id="upload-file-info"></span>
                                                                    @if($ad->function=="Auditor tecnico")
                                                                        <button class="btn btn-block btn-primary subir" style="display:none"><i class="fas fa-file-upload"></i> Subir certificado</button>
                                                                    @endif
                                                                </form>
                                                            </div>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endif

                                        @foreach($audit->certificates as $file)
                                            @if($file->type=="Fase 2")
                                                <div class="tab-pane fade " id="certificate{{$audit->id}}" role="tabpanel" aria-labelledby="contact-tab">
                                                    <center>
                                                        <form method="post" action="{{route('savefilecertificate')}}" enctype="multipart/form-data">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="certificatePath" value="{{$file->id}}">
                                                                <h6>Certificado</h6>
                                                                <label class="btn btn-outline-primary btn-lg " for="my-file-selector" >
                                                                    <input id="my-file-selector" type="file" style="display:none" 
                                                                  onchange="$('#upload-file-info').html(this.files[0].name); $('.subir').css('display', 'block');"  required="required" name="certificadopdf" accept="application/pdf">
                                                                  Elegir certificado</label>
                                                              <span class='label label-info' id="upload-file-info"></span>
                                                              <button class="btn btn-block btn-primary subir" style="display:none"><i class="fas fa-file-upload"></i> Subir certificado</button>
                                                        </form> 
                                                        <div class="col-sm-4">
                                                            <div class="card card-file">
                                                              <div class="card-file-thumb tx-danger">
                                                                <i class="far fa-file-pdf"></i>
                                                              </div>
                                                              <div class="card-body">
                                                                <h6><a href="{{Storage::url($file->file_path)}}" target="_blank" class="link-02">Certificado.pdf</a></h6>
                                                              </div>
                                                            </div>
                                                        </div>
                                                    </center>
                                                </div>
                                            @elseif($file->type=="Recertificacion")
                                                <div class="tab-pane fade " id="certificate{{$audit->id}}" role="tabpanel" aria-labelledby="contact-tab">
                                                    <form method="post" action="{{route('savefilecertificate')}}" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="certificatePath" value="{{$file->id}}">
                                                        <h6>Certificado</h6>
                                                        <label class="btn btn-outline-primary btn-lg btn-block" for="my-file-selector" >
                                                          <input id="my-file-selector" type="file" style="display:none" 
                                                          onchange="$('#upload-file-info').html(this.files[0].name); $('.subir').css('display', 'block');"  required="required" name="certificadopdf" accept="application/pdf">
                                                          Elegir certificado
                                                        </label>
                                                        <span class='label label-info' id="upload-file-info"></span>
                                                        <button class="btn btn-block btn-primary subir" style="display:none"><i class="fas fa-file-upload"></i> Subir certificado</button>
                                                    </form> 
                                                    <center>
                                                        <div class="col-sm-4">
                                                            <div class="card card-file">
                                                                <div class="card-file-thumb tx-danger"><i class="far fa-file-pdf"></i></div>
                                                                <div class="card-body">
                                                                    <h6><a href="{{Storage::url($file->file_path)}}" target="_blank" class="link-02">Certificado.pdf</a></h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </center>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>





   

                    </div><!-- row -->
                  </div><!-- col -->
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection

@section('scripts')
    @foreach($audit->sections as $ad)
        <script type="text/javascript">
          $(document).ready(function() {
            var count = 0;
            $('.onloadValidation{{$ad->id}}').closest('.validationInputCard').find("textarea[name^='observationCheck']").each(function(){
              if($(this).val().trim() == '') {
                  count = count + 1;
              }
            });
            if(count == 0){
              $('.onloadValidation{{$ad->id}}').html('');
            }else{
              $('.onloadValidation{{$ad->id}}').html(' '+ count + ' preguntas restantes');
            }
          });
        </script>
    @endforeach
<script>
  $(document).ready(function(){

    $(".mostrarToggle").click(function(){
        if($(this).prop('checked') )
            $('.ayuda').removeClass('d-none');
        else
            $('.ayuda').addClass('d-none');
    });
    


  $("#ocul").css("display","none");
  $("#success").css("display","block");
   $("#finish").click(function(){
      $("#ocul").each(function() {
        displaying = $(this).css("display");
        if(displaying == "block") {
          $(this).fadeOut('slow',function() {
           $(this).css("display","none");
          });
           $('#success').css("display","block");
        } else {
          $(this).fadeIn('slow',function() {
          $(this).css("display","block");
          });
          $('#success').css("display","none");
        }
      });
    });

   $("#cancel").click(function(){
      $("#success").each(function() {
        displaying = $(this).css("display");
        if(displaying == "block") {
          $(this).fadeOut('slow',function() {
           $(this).css("display","none");
          });
           $('#ocul').css("display","block");
        } else {
          $(this).fadeIn('slow',function() {
          $(this).css("display","block");
          });
          $('#ocul').css("display","none");
        }
      });
    });
  });



  $(document).ready(function(){
    $('#bt_add').click(function(){
      agregar();
    });
    $('#bt_del').click(function(){
      eliminar(id_fila_selected);
    });
    $('#bt_delall').click(function(){
      eliminarTodasFilas();
    });

    var validation = 0;
    $("ul").find('.resultadocheck').each(function(index, val) {
        if($(this).val()=="Cerrado"){
            validation = validation + 0;
        }else{
            validation = validation + 1;
        }
    });
    if(validation==0){
        $('.resultadotab').css('display', 'block');
    }else{
        $('.resultadotab').css('display', 'none');
    }
  });
  var cont=0;
  var id_fila_selected=[];
  var auditores = [];
  var pfecha = [];
  var phoras = [];
  var pminutos = [];
  var pfuncion = [];
  var bodys = [];
  function agregar(){
    cont++;
    var auditor = $('#auditor :selected').text();
    var auditorid = $('#auditor :selected').val();
    var fecha   = $('#fecha').val();
    var horas   = $('#horas').val();
    var minutos = $('#minutos').val();
    var funcion = $('#funcion').val();
    var fila='<tr class="selected" id="fila'+cont+'" onclick="seleccionar(this.id);"><td>'+cont+'</td><td>'+funcion+'</td><td>'+auditor+'</td><td>'+fecha+'</td><td>'+horas+'</td><td>'+minutos+'</td></tr>';
    var input = document.createElement('input');
    var inputfecha = document.createElement('input');
    var inputhoras = document.createElement('input');
    var inputminutos = document.createElement('input');
    var inputfuncion = document.createElement('input');
    var padre = document.getElementById("padre");
      input.type = "hidden";
      input.name = "auditor[]";
      input.id   = "auditor"+cont;
      input.value = auditorid;

      inputfecha.type = "hidden";
      inputfecha.name = "fecha[]";
      inputfecha.id   = "fecha"+cont;
      inputfecha.value = fecha;

      inputhoras.type = "hidden";
      inputhoras.name = "horas[]";
      inputhoras.id   = "horas"+cont;
      inputhoras.value = horas;

      inputminutos.type = "hidden";
      inputminutos.name = "minutos[]";
      inputminutos.id   = "minutos"+cont;
      inputminutos.value = minutos;

      inputfuncion.type = "hidden";
      inputfuncion.name = "funcion[]";
      inputfuncion.id   = "funcion"+cont;
      inputfuncion.value = funcion;
    padre.appendChild(input);
    padre.appendChild(inputfecha);
    padre.appendChild(inputhoras);
    padre.appendChild(inputminutos);
    padre.appendChild(inputfuncion);

    $('#tabla').append(fila);
    $('#auditor').val('');
    $('#fecha').val('');
    $('#horas').val('');
    $('#minutos').val('');
    $('#funcion').val('');

    reordenar();
  }

  function enviar(){
    // bodys.push(auditores)
    // console.log(bodys)
  }

  function seleccionar(id_fila){
    if($('#'+id_fila).hasClass('seleccionada')){
      $('#'+id_fila).removeClass('seleccionada');
    }
    else{
      $('#'+id_fila).addClass('seleccionada');
    }
    //2702id_fila_selected=id_fila;
    id_fila_selected.push(id_fila);
  }

  function eliminar(id_fila){
    /*$('#'+id_fila).remove();
    reordenar();*/
    for(var i=0; i<id_fila.length; i++){
      var dinput = id_fila[i];
      dinput = dinput.slice(-1);
      $('#'+id_fila[i]).remove();
      $('#'+"auditor"+dinput).remove();
      $('#'+"fecha"+dinput).remove();
      $('#'+"horas"+dinput).remove();
      $('#'+"minutos"+dinput).remove();
      $('#'+"funcion"+dinput).remove();
    }
    reordenar();
  }

  function reordenar(){
    var num=1;
    $('#tabla tbody tr').each(function(){
      $(this).find('td').eq(0).text(num);
      num++;
    });
  }
  function eliminarTodasFilas(){
    $('#tabla tbody tr').each(function(){
      $(this).remove();
    });
  }


  $('.notapli').on('change',function(){
    var oculted  = $(this).closest(".parameters").find('.conform').find('.yesornou');
    if ($(this).is(':checked') ) {
        oculted.prop( "disabled", true );
    } else {
        oculted.prop( "disabled", false );
    }
  })

  $('.cambio').on('click',function(e){
    var id    = $(this).attr('href');
    var cont  = $(this).closest("li").find('.contador').val();

    if(cont==1){
    var tab   = $('#myTabContent5').find(id).find('button').css('display', 'none');
    var form  = $('#myTabContent5').find(id).find('input').prop('disabled', true);
    var text  = $('#myTabContent5').find(id).find('textarea').prop('disabled', true);
    }
  })

    $('.yesornou').on('click',function(e){
        if( $(this).prop('checked') ) {
            $(this).closest('.collapse').find('.noconformidadesvalidacion').addClass('d-none')
            $(this).closest('.collapse').find('.noconformidadesvalidacion').removeClass('validation')
        }else{
            $(this).closest('.collapse').find('.noconformidadesvalidacion').removeClass('d-none')
            $(this).closest('.collapse').find('.noconformidadesvalidacion').addClass('validation')
        }
    })


  $(document).ready(function() {
    $('.endchecklist').closest("form").find('textarea').each(function(index, val) {
      if($(this).val().trim().length==0){
        $(this).closest("form").find('.endchecklist').addClass('d-none')
      }else{
        $(this).closest("form").find('.endchecklist').removeClass('d-none')
      }
    });

  });

$('.endchecklist').on('click',function(e){
    e.preventDefault();
    var validation = false ;

    $(this).closest("form").find('textarea').each(function(index, val) {
        if($(this).val().trim().length==0){
          validation = true ;
        }
    });

    if(validation==true){
      Swal.fire({
        position: 'top',
        icon: 'error',
        title: 'Llena todo el checklist para continuar',
        showConfirmButton: false,
        timer: 2000
      });
    }else{
        var id           = [];
        var formulario   = $(this).closest("form").find(document.getElementsByClassName('idChecklist'));

        for (let x = 0; x < formulario.length; x++) {
          id.push(formulario[x].value);
        }
        Swal.fire({
            title: "Estas seguro de continuar",
            text: "Esta accion no tendra retroceso",
            icon: "info",
            confirmButtonText: "Si",
            showCancelButton: true,
            cancelButtonColor: '#d33',
            cancelButtonText: "No",
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' } });

                $.ajax({
                  type: 'POST',
                  url: '{{route('endchecklist')}}',
                  data:{id:id},
                  success: function (data){
                    if(data.status=='ok'){
                      Swal.fire({
                        position: 'top',
                        icon: 'success',
                        title: 'Datos guardados',
                        showConfirmButton: false,
                        timer: 2000
                      });
                    }
                    location.reload();
                  }
                });
            }
          })
    }


  })



  $('.send').on('click',function(e){
    e.preventDefault();
    var id                 = [];
    var idSection          = [];
    var aplicacheck        = [];
    var conformcheck       = [];
    var observationCheck   = [];
    var observationSection = [];
    var formulario   = $(this).closest("form").find(document.getElementsByClassName('idChecklist'));
    var aplica       = $(this).closest("form").find(document.getElementsByClassName('NaCheck'));
    var conform      = $(this).closest("form").find(document.getElementsByClassName('conformCheck'));
    var observation  = $(this).closest("form").find(document.getElementsByClassName('observationCheck'));
    var observations = $(this).closest("form").find(document.getElementsByClassName('observationSection'));




    var requisitos  = $(this).closest('form').find($( ".requisitosnc" )).val();
    var description = $(this).closest('form').find($( ".descriptionnc" )).val();
    var nc          = $(this).closest('form').find($( ".ncnc option:selected" )).val();
    var ids         = $(this).closest('form').find($( '.idStatuscheck')).val()

    var validation = $(this).closest("form").find(document.getElementsByClassName('validation'));

    for (var i = 0; i < validation.length; i++) {

        if($(validation[i]).find($( ".requisitosnc" )).val().length<=1){
            Swal.fire({
                position: 'top',
                icon: 'error',
                title: 'Llena todo el checklist para continuar',
                showConfirmButton: false,
                timer: 2000
            });
        }
        if($(validation[i]).find($( ".descriptionnc" )).val().length<=1){
            
            Swal.fire({
                position: 'top',
                icon: 'error',
                title: 'Llena todo el checklist para continuar',
                showConfirmButton: false,
                timer: 2000
            });
        }
        if($(validation[i]).find($( ".ncnc option:selected" )).val().length<=1){
            Swal.fire({
                position: 'top',
                icon: 'error',
                title: 'Llena todo el checklist para continuar',
                showConfirmButton: false,
                timer: 2000
            });
        }
    }

    for (var i = 0; i < validation.length; i++) {
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' } });
        $.ajax({
          type: 'POST',
          url: '{{route('savenoconform')}}',
          data:{requisitos:$(validation[i]).find($( ".requisitosnc" )).val(),description:$(validation[i]).find($( ".descriptionnc" )).val(),nc:$(validation[i]).find($( ".ncnc option:selected" )).val(),id:ids}
        });
    }
    var formulariorecorrer   = $(this).closest("form").find('.idChecklist13');

            console.log(formulariorecorrer.length)//69
            console.log(aplica.length)//69
            console.log(conform.length)//69
            console.log(observation.length)//69
            console.log(observations.length)//7

    for (let x = 0; x < formulariorecorrer.length; x++) {
        id.push(formulariorecorrer[x].value);
        if(conform.length>=1){
            if( $(aplica[x]).is(':checked') ) {
                aplicacheck.push('NA')
            }else{
                aplicacheck.push('A')
            }

            if( $(conform[x]).is(':checked') ) {
                conformcheck.push('C')
            }else{
                conformcheck.push('NC')
            }
      }

      observationCheck.push(observation[x].value)
    }
    for (let x = 0; x < formulario.length; x++) {
        idSection.push(formulario[x].value);
    }



    for (let x = 0; x < observations.length; x++) {
      
      observationSection.push(observations[x].value)
    }

    // for (let x = 0; x < formulario.length; x++) {
    //   id.push(formulario[x].value);

    //   if(conform.length>=1){
    //     if( $(aplica[x]).is(':checked') ) {
    //         aplicacheck.push('NA')
    //     }else{
    //         aplicacheck.push('A')
    //     }

    //     if( $(conform[x]).is(':checked') ) {
    //         conformcheck.push('C')
    //     }else{
    //         conformcheck.push('NC')
    //     }
    //   }

    //   observationCheck.push(observation[x].value)
    // }

    // for (let x = 0; x < observations.length; x++) {
      
    //   observationSection.push(observations[x].value)
    // }


  Swal.fire({
    title: "Estas seguro",
    text: "Guardar resultados",
    icon: "info",
    confirmButtonText: "Si",
    showCancelButton: true,
    cancelButtonColor: '#d33',
    cancelButtonText: "No",
  }).then((result) => {
    if (result.value) {
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' } });

        $.ajax({
          type: 'POST',
          url: '{{route('generateFile')}}',
          data:{id:id,idSection,idSection,aplicacheck:aplicacheck,conformcheck:conformcheck,observationCheck:observationCheck,observationSection:observationSection},
          success: function (data){
            location.reload();
          }
        });
    }else{
    }
  })
  })





  $(document).ready(function() {
    
    function validarReactvacion(){
		var ids      = $('.auditIdResult').val();
		$(".sectorIdResult").each(function() {
			$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' } });
			$.ajax({
				type: 'POST',
				url: '{{route('result')}}',
				data:{id:ids,sector:$(this).val()},
				success: function (data){
					if(data=="No"){
						$('.finalizar').after(`<li class="nav-item " style="position: absolute; right: 16px ">
							<button class="btn btn-warning " type="button"  href="#start` + '{{$audit->certification->id}}' + `" data-toggle="modal" data-animation="effect-sign" ><i class="fas fa-flag-checkered"></i> Reactivar</button>
						</li>`);
						$('.finalizar').hide();
					}   
				}
			});
		});
    }    
    @if($audit->status == "En progreso")
      validarReactvacion()
    @endif

    if($('.validacionbotones').val()==1){
          $('button').remove();
          $('.saveteam').remove()
          $('.finalizarauditoriavalidation').append('<button class="btn btn-success btn-block"><i data-feather="download"></i> Descargar</button>')
          $('.nodisabled').append('<button type="submit" class="btn btn-primary text-white nodisabled" style="display: block !important;"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button><button type="button" class="btn btn-secondary  text-white nodisabledcancelar" style="display: block !important;"  data-dismiss="modal"><i class="fas fa-undo"></i>  Cancelar</button>')


    }

      $('.agregarotro').on('click',function(e){
          e.preventDefault();
          var requisitos  = $(this).closest('form').find($( ".requisitosnc" )).val();
          var description = $(this).closest('form').find($( ".descriptionnc" )).val();
          var nc          = $(this).closest('form').find($( ".ncnc option:selected" )).val();
          var id          = $(this).closest('form').find($('.idStatuscheck')).val()
           var htmlTags = '<tr class="tr">'+
              '<td><input type="hidden" value="'+requisitos+'" name="requisitos[]"/>' + requisitos + '</td>'+
              '<td><input type="hidden" value="'+description+'" name="description[]"/>' + description +'</td>'+
              '<td><input type="hidden" value="'+nc+'" name="nc[]"/>' + nc + '</td>'+'</tr>';

            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' } });
            $.ajax({
              type: 'POST',
              url: '{{route('savenoconform')}}',
              data:{requisitos:requisitos,description:description,nc:nc,id:id}
            });

          $('.trtabla').append(htmlTags);

          $( ".requisitosnc" ).val("");
          $( ".descriptionnc" ).val("");
          $( ".ncnc" ).val("");

          // $('.eliminar').on('click',function(e){
          //     $(this).closest('tr').remove();
          // });
          Swal.fire({
            position: 'top',
            icon: 'success',
            title: 'Datos guardados',
            showConfirmButton: false,
            timer: 2000
          });
      });

      $('.codigoenviarform').on('change',function(e){
        var valor = $(this).val();
         $('.code').val(valor)
      });

      $('.exclusionenviarform').on('change',function(e){
        var valor = $(this).val();
         $('.exclusiones').val(valor)
      });


  });


      $('.traerresultado').on('click',function(e){

        let href = $(this).attr('href');
        let div  = $(this).closest('body').find(href);

        let id     = div.find($('.auditIdResult')).val();
        let sector = div.find($('.sectorIdResult')).val();

        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' } });
          $.ajax({
              type: 'POST',
              url: '{{route('result')}}',
              data:{id:id,sector:sector},
              success: function (data){
                if(data=="Si"){
                    div.find($('.resultadoAjax')).html(data+' aprobó <i class="fas fa-check-circle text-success"></i>')
                }else{
                    div.find($('.resultadoAjax')).html(data+' aprobó <i class="fas fa-info-circle text-danger"></i>')
                }
              }   
          });


      })

    $('.observationCheck').on('change',function(e){
        if($(this).val().length >=3){
          $(this).addClass('validateTextAreaForm');
        }else{
          $(this).removeClass('validateTextAreaForm');
        }
          var restantes = $(this).closest('.validationInputCard').find('.observationCheck').length;
          var hechos    = $(this).closest('.validationInputCard').find('.validateTextAreaForm').length;
          $(this).closest('.validationInputCard').find('.inforestantes').html(' '+restantes-hechos + ' preguntas restantes')
    });


</script>
    @foreach($audit->auditsector as $ad)
        <script type="text/javascript">
            $(document).ready(function() {
                $('.{{$ad->sector->id}}Sector').addClass('d-none');
                $('.{{$ad->sector->id}}Sector').first().removeClass('d-none');
                $('.{{$ad->norm->id}}Norma').addClass('d-none');
                $('.{{$ad->norm->id}}Norma').first().removeClass('d-none');
            });
        </script>
    @endforeach


    @if($audit->certification->status == 'Cancelado')
        <script type="text/javascript">
            $(document).ready(function() {
                $('button').addClass('d-none');
                $('input').prop('disabled', 'true');
                $('.saveteam').addClass('d-none')
            });
        </script>
    @endif
@endsection


