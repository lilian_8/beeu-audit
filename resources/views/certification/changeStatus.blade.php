@foreach($organizations as $organization)
    @foreach($organization->certification as $organizationdetail)
        @if($organization->certification)
            <div class="modal fade" id="changeStatus{{$organizationdetail->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Cambiar estatus de certificación</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="post" action="{{route('changeStatus')}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$organizationdetail->id}}">
                            <div class="modal-body">
                                    <div class="form-group">
                                        <label >Status</label>
                                        <select class="form-control custom-select"  style="width: 100%" id="statusChange" name="statusChange" required="required">
                                              <option disabled selected>Elige ...</option>
                                              <option value="Activo">Activo</option>
                                              <option value="Finalizado">Finalizado</option>
                                              <option value="Cancelado">Cancelado</option>
                                        </select>
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-primary"><i class="fas fa-paper-plane"></i> Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@endforeach