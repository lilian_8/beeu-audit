@extends('layouts.app')
@section('content')
       <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-0">
            <li class="breadcrumb-item"><a href="/">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Progreso</li>
          </ol>
          <br>
        </nav>

        <div class="col-sm-6 col-lg-12">

          <div class="card card-body">

            <div class="container-fluid row">
              <h6 class="tx-uppercase tx-20 tx-spacing-1 tx-color-02 tx-semibold mg-b-8 col-lg-10"><i data-feather="flag"></i> Progreso de certificacion</h6>
            </div>

            <br>

          <div class="container-fluid">
              <div class="row">
                <div class="col-sm-4 ">

                  <div class="card">
                      <div class="card-header bg-dark text-white"> <i data-feather="home"></i> Organización</div>
                      <div class="card-body">
                          <strong>Código</strong>: {{$organizations->code}}
                          <br>
                          <strong>Organización</strong>: {{$organizations->name}}
                          <br>
                          <strong>Rfc</strong>: {{$organizations->rfc}}
                          <br>
                          <strong>Sitios</strong>: {{$organizations->sites}}
                          <br>
                          <strong>Empleados</strong>: {{$organizations->employees}}
                      </div>
                  </div>

                </div>

                <div class="col-sm-4">
                  <div class="card">
                      <div class="card-header bg-dark text-white"> <i data-feather="phone"></i> Contacto</div>
                      <div class="card-body">
                          <strong>Nombre</strong>: {{$organizations->directionCertification->contact_name}} 
                          <br>
                          <strong>Puesto</strong>: {{$organizations->directionCertification->contact_job}}
                          <br>
                          <strong>Teléfono</strong>: {{$organizations->directionCertification->extension}}  {{$organizations->directionCertification->phone}}
                          <br>
                          <strong>Email</strong>:{{$organizations->directionCertification->email}}
                      </div>
                  </div>
                </div>

                <div class="col-sm-4 ">
                  <div class="card">
                      <div class="card-header bg-dark text-white"> <i data-feather="map-pin"></i> Direccion</div>
                      <div class="card-body">
                          <!-- <strong>País</strong>: {{$organizations->directionCertification->country}} -->
                          <strong>Estado</strong>: {{$organizations->directionCertification->state}}, {{$organizations->directionCertification->municipality}}
                          <br>
                          <strong>Ciudad</strong>: {{$organizations->directionCertification->city}}, {{$organizations->directionCertification->suburb}}
                          <br>
                          <strong>Calle</strong>: {{$organizations->directionCertification->street}} #{{$organizations->directionCertification->external_number}} #int:{{$organizations->directionCertification->internal_number}}
                          <br>
                          <strong>CP</strong>: {{$organizations->directionCertification->zip_code}}
                          <!-- <strong>Referencia</strong>: {{$organizations->directionCertification->reference}} -->
                      </div>
                  </div>
                </div>

              </div>
          </div>

          <div class="content">
            <div class="container-fluid pd-x-0 pd-lg-x-10 pd-xl-x-0">
              <div class="row">
                <div class="col-lg-12">
                  <div class="row row-xs mg-b-25">
                      <ul class="steps">
                        @foreach($audits as $audit )
                        @if($audit->status=='En progreso')
                          <li class="step-item complete">
                        @elseif($audit->status=='Terminado')
                          <li class="step-item active">
                        @endif
                          <a href="{{route('auditdetails',$audit->id)}}" class="step-link">
                            <span class="step-icon"><i class="fas fa-dice-d6"></i></span>
                            <div>
                              <span class="step-title">
                                  @if($audit->type=='Especial')
                                    {{$audit->type}} <i class="fas fa-star"></i>
                                  @else
                                    {{$audit->type}} 
                                  @endif
                              </span>
                              <span class="step-desc">{{$audit->status}}</span>
                            </div>

                          </a>
                        </li>
                        @endforeach
                      </ul>
                    </div><!-- row -->
                  </div><!-- col -->
                </div>
              </div>
            </div>
        </div>
      </div>
@endsection

@section('scripts')

<script>
  $(document).ready(function(){
    $('#bt_add').click(function(){
      agregar();
    });
    $('#bt_del').click(function(){
      eliminar(id_fila_selected);
    });
    $('#bt_delall').click(function(){
      eliminarTodasFilas();
    });
    

  });
  var cont=0;
  var id_fila_selected=[];
  var auditores = [];
  var pfecha = [];
  var phoras = [];
  var pminutos = [];
  var pfuncion = [];
  var bodys = [];
  function agregar(){
    cont++;
    var auditor = $('#auditor :selected').text();
    var auditorid = $('#auditor :selected').val();
    var fecha   = $('#fecha').val();
    var horas   = $('#horas').val();
    var minutos = $('#minutos').val();
    var funcion = $('#funcion').val();
    var fila='<tr class="selected" id="fila'+cont+'" onclick="seleccionar(this.id);"><td>'+cont+'</td><td>'+funcion+'</td><td>'+auditor+'</td><td>'+fecha+'</td><td>'+horas+'</td><td>'+minutos+'</td></tr>';
    var input = document.createElement('input');
    var inputfecha = document.createElement('input');
    var inputhoras = document.createElement('input');
    var inputminutos = document.createElement('input');
    var inputfuncion = document.createElement('input');
    var padre = document.getElementById("padre");
      input.type = "hidden";
      input.name = "auditor[]";
      input.id   = "auditor"+cont;
      input.value = auditorid;

      inputfecha.type = "hidden";
      inputfecha.name = "fecha[]";
      inputfecha.id   = "fecha"+cont;
      inputfecha.value = fecha;

      inputhoras.type = "hidden";
      inputhoras.name = "horas[]";
      inputhoras.id   = "horas"+cont;
      inputhoras.value = horas;

      inputminutos.type = "hidden";
      inputminutos.name = "minutos[]";
      inputminutos.id   = "minutos"+cont;
      inputminutos.value = minutos;

      inputfuncion.type = "hidden";
      inputfuncion.name = "funcion[]";
      inputfuncion.id   = "funcion"+cont;
      inputfuncion.value = funcion;
    padre.appendChild(input);
    padre.appendChild(inputfecha);
    padre.appendChild(inputhoras);
    padre.appendChild(inputminutos);
    padre.appendChild(inputfuncion);

    $('#tabla').append(fila);
    $('#auditor').val('');
    $('#fecha').val('');
    $('#horas').val('');
    $('#minutos').val('');
    $('#funcion').val('');

    reordenar();
  }

  function enviar(){
    // bodys.push(auditores)
    // console.log(bodys)
  }

  function seleccionar(id_fila){
    if($('#'+id_fila).hasClass('seleccionada')){
      $('#'+id_fila).removeClass('seleccionada');
    }
    else{
      $('#'+id_fila).addClass('seleccionada');
    }
    //2702id_fila_selected=id_fila;
    id_fila_selected.push(id_fila);
  }

  function eliminar(id_fila){
    /*$('#'+id_fila).remove();
    reordenar();*/
    for(var i=0; i<id_fila.length; i++){
      var dinput = id_fila[i];
      dinput = dinput.slice(-1);
      $('#'+id_fila[i]).remove();
      $('#'+"auditor"+dinput).remove();
      $('#'+"fecha"+dinput).remove();
      $('#'+"horas"+dinput).remove();
      $('#'+"minutos"+dinput).remove();
      $('#'+"funcion"+dinput).remove();
    }
    reordenar();
  }

  function reordenar(){
    var num=1;
    $('#tabla tbody tr').each(function(){
      $(this).find('td').eq(0).text(num);
      num++;
    });
  }
  function eliminarTodasFilas(){
$('#tabla tbody tr').each(function(){
      $(this).remove();
    });

  }


</script>

@endsection 


