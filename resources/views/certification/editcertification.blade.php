@foreach($certificates as $certificate)
    <div class="modal fade" id="editcertification{{$certificate->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Editar certificacion</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            <form method="post" action="{{route('editcertification')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Status</label>
                        <select class="form-control" id="exampleFormControlSelect1" name="status">
                            @if($certificate->status=="Valido")
                              <option selected value="Valido">Valido</option>
                              <option value="Suspendido">Suspendido</option>
                              <option value="Cancelado">Cancelado</option>
                            @elseif($certificate->status=="Suspendido")
                              <option value="Valido">Valido</option>
                              <option selected value="Suspendido">Suspendido</option>
                              <option value="Cancelado">Cancelado</option>
                            @else
                              <option value="Valido">Valido</option>
                              <option value="Suspendido">Suspendido</option>
                              <option selected value="Cancelado">Cancelado</option>
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlFile1">Subir nuevo certificado</label>
                        <input type="file" class="form-control-file" id="exampleFormControlFile1" name="certificadopdf">
                    </div>
                    <input type="hidden" name="certificatePath" value="{{$certificate->id}}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
      </div>
    </div>
@endforeach