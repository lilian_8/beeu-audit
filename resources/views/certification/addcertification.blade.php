@if(in_array(Auth::user()->profile,['Administrador','Comercial']))
    <div class="modal fade" id="new"  role="dialog" aria-hidden="true">
      <div class="modal-dialog-centered modal-dialog moda" role="document">
        <div class="modal-content">
          
          <div class="modal-header pd-y-20 pd-x-20 pd-sm-x-30">
            <a href="" role="button" class="close pos-absolute t-15 r-15" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </a>
            <div class="media align-items-center">
                <div id="preview" class="imagen text-center">
                  <span class="tx-color-03 d-none d-sm-block"><i data-feather="folder-plus" class="fas fa-user-plus wd-60 ht-60"></i></span>
                </div>
                  
              <div class="media-body mg-sm-l-20">
                <h4 class="tx-18 tx-sm-20 mg-b-2">NUEVA CERTIFICACIÓN</h4>
              </div>
            </div><!-- media -->
          </div><!-- modal-header -->

          <form class="form-horizontal" method="post" action="{{ route('savecertification') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

          <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30">
              
              <div class="form-group">
                <label >NO. CERTIFICADO</label>
                <input type="text" class="form-control" name="no_certificate" required="required" placeholder="NO. CERTIFICADO">
              </div>   

              <div class="form-group">
                <label >ORGANIZACIONES</label>
                  <select class="form-control custom-select"  style="width: 100%" id="organizationId" name="idorganization" required="required">
                      <option disabled selected>Elige ...</option>
                    @foreach($organizations as $organization)
                      <option value="{{$organization->id}}">{{$organization->name}}</option>
                    @endforeach
                  </select>
              </div>

              <div class="form-group">
                <label >FECHA INICIO</label>
                <input type="date" class="form-control" name="date" required="required">
              </div>

              <div class="form-group">
                <label >FECHA TERMINO</label>
                <input type="date" class="form-control" name="date_end" required="required">
              </div>

               <div class="form-group">
                <label >SECTORES Y NORMAS</label>
                  <table class="table table-bordered table-xs">
                    <tr>
                      <th>Sector</th>
                      <th>Normas</th>
                    </tr>
                    <tbody>
                        <tr>
                          <td class="normsandsectors"></td>
                          <td class="normsandsectors2"></td>
                        </tr>
                    </tbody>
                  </table>
              </div>
   
          </div>

          <div class="modal-footer pd-x-20 pd-y-15">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fas fa-undo"></i> Cancelar</button>
            <button type="submit" class="btn btn-primary"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button>
          </div>

        </form>
      </div>
    </div>
  </div>
@endif


