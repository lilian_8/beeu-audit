@extends('layouts.app')
    @section('css')
        <style type="text/css">
            .imagen img{
                width: 80px !important;
                border-radius: 10px;
              }
              .pagination .page-link{
                background-color:#7987a1;
                border: none;
                color: white;
              }
              .pagination  .page-link:hover{
                background-color:#64738f;
                border: none;
                color: white;
              }

              .pagination .disabled{
                display: none;
              }
              .page-item.active .page-link {
                background-color:#3b4863 !important;
                }


        </style>
    @endsection

@section('content')
        @if ($errors->any())
          <div class="errors">
              <p><strong>Por favor corrige los siguientes errores<strong></p>
              <ul class="text-danger">
                  @foreach ($errors->all() as $error)
                      <li><div class="alert alert-danger" role="alert">{{ $error }}</div></li>
                  @endforeach
              </ul>
          </div>
      @endif
       <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-0">
            <li class="breadcrumb-item"><a href="#">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Usuarios</li>
          </ol>
          <br>
        </nav>

        <div class="col-sm-12 col-lg-12 col-md-12">
          <div class="card card-body">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-6">
                      <h6 class="tx-uppercase tx-20 tx-spacing-1 tx-color-02 tx-semibold mg-b-8 col-lg-10">
                            <i data-feather="user"></i> Usuarios
                      </h6>
                    </div>
                    @if(in_array(Auth::user()->profile, ['Administrador']))
                      <div class="col-md-6">
                          <a href="#new"  class="btn  btn-dark tx-spacing-1 tx-semibold col-lg-4 col-md-4 col-sm-12 text-white newbutton" style="float: right;" data-toggle="modal" data-animation="effect-sign" > <i class="fas fa-plus"></i> Nuevo</a>
                      </div>
                    @endif
                </div>

            </div>

            <hr>

            <div class="container-fluid row">
                <h1 class="tx-20 tx-spacing-1 tx-color-03 tx-semibold mg-b-8 col-lg-10">Filtros</h1>
            </div>

            <form>
               <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-md-12">
                        <input type="search" name="search" class="form-control" placeholder="Buscar por nombre,apellido o correo" aria-label="Search" aria-describedby="btnGroupAddon">
                    </div>

                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-lg-12 col-sm-12 col-md-12">
                          <br>
                          <button class="btn bg-dark text-white" type="submit"><i class="fa fa-search"></i> Buscar</button>
                          <button class="btn bg-dark text-white" type="submit"> Ver todos</button>
                        </div>
                      </div>
                    </div>
                    
                </div>
              </div>
            </form>
            <hr>

            <div class="container-fluid ">
                <div class="table-responsive ">
                  <table class="table table-striped table-sm  table-bordered ">
                    <thead>
                     <tr>
                      <th>Código</th>
                      <th>Perfil</th>
                      <th>Nombre</th>
                      <th class="d-none d-md-table-cell">Apellidos</th>
                      <th class="d-none d-md-table-cell">Correo</th>
                      <th class="d-none d-md-table-cell">Telefono</th>
                      @if(in_array(Auth::user()->profile, ['Administrador']))
                        <th>Acciones</th>
                      @endif
                    </tr>
                    </thead>
                    <tbody>
                       @foreach($users as $user)
                          @if($user->profile!="Auditor")

                              <td>{{$user->code}}</td>
                              <td>{{$user->profile}}</td>
                              <td>{{$user->name}}</td>
                              <td class="d-none d-md-table-cell">{{$user->lastname}} {{$user->secondname}}</td>
                              <td class="d-none d-md-table-cell">{{$user->email}}</td>
                              <td class="d-none d-md-table-cell">{{$user->cellphone}}</td>
                              @if(in_array(Auth::user()->profile, ['Administrador']))
                                <td style="text-align: center;">
                                  <a href="edituser/{{$user->id}}"class="btn btn-light btn-icon" ><i data-feather="edit" style="color:#48bb78" ></i></a>
                                  <button class="btn btn-light danger-btn btn-icon" data-toggle="modal" href="#delete" data-animation="effect-sign" data-catid="{{$user->id}}"><i data-feather="trash-2" style="color: red"></i></button>
                                </td>
                              @endif

                          </tr>
                          @endif
                      @endforeach
                    </tbody>
                  </table>

                </div><!-- table-responsive -->
                @if (count($users))
                  <div name="paginador" style="float: left;">
                    {{ $users->links() }}
                  </div>
                @endif
          </div>
        </div>
     </div>

@extends('user.createusers')
@extends('user.deleteusers')
@endsection

@section('scripts')
    <script>
      $('#delete').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var cat_id = button.data('catid')
        var modal  = $(this)
        modal.find('.modal-body #user_id').val(cat_id);
      })
    </script>
    <script>
      document.getElementById("file").onchange = function (e) {
        let reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);
            reader.onload     = function () {
            let preview       = document.getElementById("preview"),
            image             = document.createElement("img");
            image.src         = reader.result;
            preview.innerHTML = "";
            preview.append(image);
        };
      };
    </script>
@endsection


