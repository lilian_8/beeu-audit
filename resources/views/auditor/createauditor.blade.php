    <div class="modal fade bd-example-modal-lg" id="new" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog  modal-xl " role="document">
          <form class="form-horizontal" method="post" action="{{ route('saveauditors') }}" accept-charset="UTF-8" enctype="multipart/form-data" >
          {{ csrf_field() }}
        <div class="modal-content">

          <div class="modal-header pd-y-20 pd-x-20 pd-sm-x-30">
            <a href="" role="button" class="close pos-absolute t-15 r-15" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </a>
            <div class="media align-items-center">
                  <div id="preview" class="imagen text-center">
                    <span class="tx-color-03 d-none d-sm-block"><i data-feather="user-plus" class="fas fa-user-plus wd-60 ht-60"></i></span>
                  </div>
              <div class="media-body mg-sm-l-20">
                <h4 class="tx-18 tx-sm-20 mg-b-2">INGRESE LOS DATOS DEL AUDITOR</h4>
              </div>
            </div><!-- media -->
          </div><!-- modal-header -->

          <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30">
            <ul class="nav nav-tabs nav-justified" id="myTab3" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="home-tab3" data-toggle="tab" href="#home3" role="tab" aria-controls="home" aria-selected="true">Datos Generales</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="profile-tab3" data-toggle="tab" href="#sectores" role="tab" aria-controls="profile" aria-selected="false">Sectores </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="profile-tab3" data-toggle="tab" href="#norma" role="tab" aria-controls="profile" aria-selected="false">Normas</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="contact-tab3" data-toggle="tab" href="#academico" role="tab" aria-controls="contact" aria-selected="false">Educación</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="contact-tab4" data-toggle="tab" href="#laboral" role="tab" aria-controls="contact" aria-selected="false">Competencia</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="contact-tab5" data-toggle="tab" href="#history" role="tab" aria-controls="contact" aria-selected="false">His. Auditorias</a>
              </li>              
            </ul>
            <div class="tab-content bd bd-gray-300 bd-t-0 pd-20" id="myTabContent3">
              <div class="tab-pane fade show active" id="home3" role="tabpanel" aria-labelledby="home-tab3">
                   <h6>Generales</h6>

                    <div class="form-group">
                      <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">FOTO DE PERFIL</label>
                        <div class="custom-file">
                            <input type="file" name="foto" size="150" maxlength="150" accept="image/jpeg,image/png" class="form-group custom-file-input" id="file" capture="camera"/>
                        <label class="custom-file-label" for="customFile">Escoger Foto</label>
                      </div>
                    </div>

                    <div class="row row-sm">
                        <div class="col-sm">
                          <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03 validation">NOMBRE</label>
                          <input type="text" class="form-control" placeholder="NOMBRE" name="nombre" required="required" value="{{ old('nombre') }}">
                        </div>
                        <div class="col-sm mg-t-20 mg-sm-t-0">
                          <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03 validation">APELLIDO PATERNO</label>
                          <input type="text" class="form-control" placeholder="APELLIDO PATERNO" name="apellidopaterno" required="required" value="{{ old('apellidopaterno') }}">
                        </div>
                        <div class="col-sm mg-t-20 mg-sm-t-0">
                          <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">APELLIDO MATERNO</label>
                          <input type="text" class="form-control" placeholder="APELLIDO MATERNO" name="apellidomaterno" required="required" value="{{ old('apellidomaterno') }}">
                        </div>
                    </div>

                    <div class="row row-sm mt-3">
                        <div class="col-sm">
                          <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CORREO</label>
                          <input type="email" class="form-control" placeholder="CORREO" name="email" required="required" value="{{ old('email') }}">
                        </div>
                        <div class="col-sm mg-t-20 mg-sm-t-0">
                          <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CONTRASEÑA</label>
                          <input type="password" class="form-control" placeholder="********" name="password" required="required" value="{{ old('password') }}">
                        </div>
                    </div>

                    <div class="row row-sm mt-3">
                        <div class="col-sm">
                          <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">TELÉFONO</label>
                          <input type="number" class="form-control" placeholder="TELÉFONO" name="telefono" required="required" value="{{ old('telefono') }}">
                        </div>

                        <div class="col-sm mg-t-20 mg-sm-t-0">
                          <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CÓDIGO</label>
                          <input type="text" class="form-control" placeholder="CÓDIGO" name="codigo" required="required" value="{{ old('codigo') }}">
                        </div>

                        <div class="col-sm mg-t-20 mg-sm-t-0">
                          <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">PERFIL</label>
                          <select class="form-control custom-select" name="perfil" required="required" readonly>
                              <option value="Auditor" selected>Auditor</option>
                          </select>
                        </div>

                        <div class="col-sm mg-t-20 mg-sm-t-0">
                          <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">ESTADO</label>
                          <select class="form-control custom-select" name="status" selected required="required">
                              <option value="Activo" >Activo</option>
                              <option value="Inactivo">Inactivo</option>
                              <option value="Suspendido">Suspendido</option>
                          </select>
                        </div>

                    </div>
            </div>

            <div class="tab-pane fade" id="sectores" role="tabpanel" aria-labelledby="profile-tab3">
              <h6>Sectores</h6>

                <div id="select_sectores"></div>

                <form class="form-horizontal">
                  <div class="modal-body" >
                    <div class="tx-13 mg-b-25 " style="height:350px; overflow: scroll;">
                      <table class="table">
                        <thead>
                          <tr>
                            <th scope="col"></th>
                            <th scope="col">Sector</th>
                            <th scope="col">Habilitar</th>
                          </tr>
                        </thead>
                        <tbody>
                         @foreach($sectors as $sector)
                          <tr>
                            <th>{{$sector->id}}</th>
                            <th>{{$sector->name}}</th>
                            <td> 
                              <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="sector{{$sector->id}}" value="{{$sector->id}}" name="sector[]" >
                                <label class="custom-control-label" for="sector{{$sector->id}}"></label>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </form>

            </div>

            <div class="tab-pane fade" id="norma" role="tabpanel" aria-labelledby="profile-tab3">
              <h6>Normas</h6>

                <div id="select_sectores"></div>

                  <div class="modal-body" >
                    <div class="tx-13 mg-b-25 " >
                      <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">Norma</th>
                            <th scope="col">Habilitar</th>
                          </tr>
                        </thead>
                        <tbody>
                         @foreach($norms as $norm)
                          <tr>
                            <td>{{$norm->name}}</td>
                            <td> 
                              <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="norm{{$norm->id}}" value="{{$norm->id}}" name="norma[]" >
                                <label class="custom-control-label" for="norm{{$norm->id}}"></label>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>

            </div>
              <div class="tab-pane fade" id="academico" role="tabpanel" aria-labelledby="contact-tab3">
                <h6>Académico</h6>
                <button type="button" class="btn btn-dark "  data-toggle="modal" data-target="#topModal" ><i class="fas fa-save"></i> Agregar</button>
                  <div id="select_academic"></div>

                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">Curso</th>
                        <th scope="col">Fecha</th>
                        <th scope="col">Acciones</th>
                      </tr>
                    </thead>
                    <tbody class="parametrosparaenviar">
                    </tbody>
                  </table>
              </div>

              <div class="tab-pane fade" id="laboral" role="tabpanel" aria-labelledby="contact-tab3">
                <h6>Laboral</h6>
                <table class="table table-striped table-sm">
                    <tr>
                        <th>Periodo</th>
                        <th>Empresa</th>
                        <th>Puesto</th>
                        <th>Actividad desarrollada</th>
                        <th><button type="button" class="btn btn-xs btn-primary jobhistoryadd" style="float:right"><i class="fas fa-plus-circle"></i></button></th>
                    </tr>
                    <tbody class="jobhistory">
                    </tbody>
                </table>
                <!-- <hr>
                <h6>Auditorías realizadas</h6>
                <table class="table table-striped table-sm">
                    <tr>
                        <th>Empresa</th>
                        <th>Tipo de auditoria</th>
                        <th>Norma</th>
                        <th>Fecha</th>
                        <th>Días</th>
                        <th>Sector</th>
                        <th><button type="button" class="btn btn-xs btn-primary auditrealizatedadd" style="float:right"><i class="fas fa-plus-circle"></i> </button></th>
                    </tr>
                    <tbody class="auditrealizated">
                        
                    </tbody>
                </table> -->
                <hr>
                <h6>Formación</h6>
                <table class="table table-striped table-sm">
                    <tr>
                        <th>Nombre del curso</th>
                        <th>Horas</th>
                        <th>Fecha</th>
                        <th>Días</th>
                        <th>Impartido por</th>
                        <th><button type="button" class="btn btn-xs btn-primary formationadd" style="float:right"><i class="fas fa-plus-circle"></i> </button></th>
                    </tr>
                    <tbody class="formation">
                        
                    </tbody>
                </table>
                <hr>

                <h6>Evaluación de desempeño</h6>
                <table class="table table-striped table-sm">
                    <tr>
                        <th>Empresa</th>
                        <th>Fecha</th>
                        <th>Tipo de auditoria</th>
                        <th>Periodo</th>
                        <th>Calificación</th>
                        <th>Testificación</th>
                        <th>Calificación cliente</th>
                        <th><button type="button" class="btn btn-xs btn-primary evaluationadd" style="float:right"><i class="fas fa-plus-circle"></i></button></th>
                    </tr>
                    <tbody class="evaluation">
                        
                    </tbody>
                </table>

              </div>

            <div class="tab-pane fade" id="history" role="tabpanel" aria-labelledby="contact-tab3">
            <h6>Historial de auditorias</h6>
            <button type="button" class="btn btn-dark "  data-toggle="modal" data-target="#modalhistory" ><i class="fas fa-save"></i> Agregar</button>
                  <!-- <div id="select_academic"></div> -->

                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">Organizacion</th>
                        <th scope="col">Sector</th>
                        <th scope="col">Norma</th>
                        <th scope="col">Rol</th>
                        <th scope="col"></th>
                      </tr>
                    </thead>
                    <tbody class="parametroshistory">
                    </tbody>
                  </table>
            </div>
            </div>
          </div>
          <div class="modal-footer pd-x-20 pd-y-15">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fas fa-undo"></i> Cancelar</button>
            <button type="submit" class="btn btn-primary" id="sendEdit"><i class="fa fa-paper-plane" aria-hidden="true" ></i> Guardar</button>
          </div>
        </form>
        </div>
      </form>
      </div>
    </div>

<div id="topModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <div class="media-body mg-sm-l-20">
            <h4 class="tx-18 tx-sm-20 mg-b-2">EDUCACIÓN</h4>
          </div>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>

        <div class="form-horizontal">
          <div class="modal-body">
              <div class="form-group">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">INSTITUCIÓN</label>
                  <input type="text" class="form-control" placeholder="INSTITUCIÓN" id="name_course">
              </div>

              <div class="form-group">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">PERIODO</label>
                  <input type="text" class="form-control" placeholder="PERIODO" id="date_course">
              </div>

              <div class="form-group">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CARRERA/POSGRADO</label>
                  <input type="text" class="form-control" id="carrier_certification" placeholder="CARRERA/POSGRADO">
              </div>

              <div class="form-group">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CALIFICACIÓN</label>
                  <input type="text" class="form-control" id="passselect" placeholder="CALIFICACIÓN">
              </div>

              <div class="form-group">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">TITULO,DIPLOMA.CERTIFICACO,ETC</label>
                  <input type="text" class="form-control" id="titlecertificate" placeholder="TITULO,DIPLOMA.CERTIFICACO,ETC">
              </div>

              <div class="custom-file">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">Archivos </label>
                  <form id="frmFormulario" enctype="multipart/form-data">
                      <input type="file" class="custom-file-input" id="customFileLang" name="filecourse" lang="es">
                  </form>
                  <label class="custom-file-label" for="customFileLang">Seleccionar Archivo</label>
              </div>
              
          </div>

          <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="add"> <i class="fa fa-paper-plane" aria-hidden="true"></i>Guardar</button>
          </div>

      </div>
    </div>
  </div>
</div>

<div id="modalhistory" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <div class="media-body mg-sm-l-20">
            <h4 class="tx-18 tx-sm-20 mg-b-2">INGRESE LOS DATOS </h4>
          </div>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>

        <div class="form-horizontal">
          <div class="modal-body">
            <div class="form-group">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NOMBRE DE LA ORGANIZACION</label>
                <input type="text" class="form-control" placeholder="NOMBRE DE LA ORGANIZACION" id="name_organizationmh">
            </div>

            <div class="form-group">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">FECHA INICIO</label>
                <input type="date" class="form-control" placeholder="FECHA INICIO" id="date_firstmh">
            </div>

            <div class="form-group">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">FECHA TERMINO</label>
                <input type="date" class="form-control" placeholder="FECHA TERMINO" id="date_endmh">
            </div>

            <div class="form-group">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">SECTOR</label>
                <select class="form-control custom-select"  required="required"  id="sectormh">
                    @foreach($sectors as $sector)
                          <option value="{{$sector->name}}">{{$sector->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NORMA</label>
                <select class="form-control custom-select"  required="required"  id="normamh">
                    @foreach($norms as $norm)
                          <option value="{{$norm->name}}">{{$norm->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">ROL</label>
                <input type="text" class="form-control" placeholder="Rol" id="rolmh">
            </div>                       

              
          </div>

          <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="add"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button>
          </div>

      </div>
    </div>
  </div>
</div>