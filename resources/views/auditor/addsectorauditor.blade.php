<div class="modal fade" id="secoresynormas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Asignacion de normas y sectores a un auditor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form class="form-horizontal" method="post" action="{{ route('updatesector') }}">
      {{ csrf_field() }}
      <input type="hidden" name="id" id="user_id" value="{{$u->id}}">
      <div class="modal-body">
        <div class="tx-13 mg-b-25" style="height:450px; overflow: scroll;">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col"></th>
                  <th scope="col">Sector</th>
                  <th scope="col">Habilitar</th>
                </tr>
              </thead>
              <tbody>
               @foreach($sectors as $sector)
                      <tr>
                        <th>{{$sector->id}}</th>
                        <th>{{$sector->name}}</th>
                        <td> 
                            @if(in_array($sector->id,$sectorIds))
                                <div class="custom-control custom-switch">
                                  <input type="checkbox" class="custom-control-input" id="sectores{{$sector->id}}" value="{{$sector->id}}" name="sector[]" checked disabled>
                                  <label class="custom-control-label" for="sectores{{$sector->id}}"></label>
                                </div> 
                            @else
                                <div class="custom-control custom-switch">
                                  <input type="checkbox" class="custom-control-input" id="sectorNew{{$sector->id}}" value="{{$sector->id}}" name="sector[]">
                                  <label class="custom-control-label" for="sectorNew{{$sector->id}}"></label>
                                </div> 
                            @endif
                        </td>
                      </tr>
                @endforeach

                @foreach($buscadornorma as $norma)
                  <input type="hidden" name="norms[]" value="{{$norma->id}}">
                @endforeach
              </tbody>
            </table>
        </div>
      </div>

      <div class="modal-footer pd-x-20 pd-y-15">
        <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fas fa-undo"></i> Cancelar</button>
        <button type="submit" class="btn btn-primary"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button>
      </div>
    </form>
    
    </div>
  </div>
</div>