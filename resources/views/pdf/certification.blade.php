<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Mexcer</title>

        <style type="text/css">
            * {
                font-family: Verdana, Arial, sans-serif;
                font-size: 8px;
            }
            table.table-title tr{
                background-color: black;
            }
            table.table-title tr td{
                padding: 0px;
                border: none;
                padding-top: 5px;
            }
            .title{
                font-family: Verdana, Arial, sans-serif;
                font-size: 15px;
                font-weight: bold;
                color: white;
                padding-left: 10px;
            }
            .subtitle{
                font-family: Verdana, Arial, sans-serif;
                font-weight: bold;
            }
            table tr td{
                padding: 2px;
                border: 1px solid lightgray;
            }
            td{
              /*height: 35px;*/
            }
            .gray {
                background-color: lightgray
            }
            .page-break {
                font-family: Verdana, Arial, sans-serif;
            }
        </style>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
        <table width="100%">
            <tr>
                {{-- <td><img  src="{{asset("storage/$company->avatar")}}"></td> --}}
                <td><strong>Organización Auditada:</strong> {{$audits->certification->certification->name}}</td>
                <td style="float: right;"><strong>Fecha de la Auditoria:</strong> {{$audits->init_date}}</td>
            </tr>
        </table>

        <br/>

        <table width="100%" class="table-title">
            <tr>
                <td><h1 class="title">1. Datos generales</h1></td>
            </tr>
        </table>

        <br/>

        <table width="100%">
            {{-- <tr>
                <td style="background-color: lightgray;">Organización Auditada:</td>
                <td colspan="3">{{$audits->certification->certification->name}}</td>
            </tr> --}}
            <tr>
                <td style="background-color: lightgray;">Dirección:</td>
                <td>
                    Calle: {{$audits->certification->certification->directionCertification->street}} <br>
                    Colonia: {{$audits->certification->certification->directionCertification->suburb}}<br>
                    Municipio: {{$audits->certification->certification->directionCertification->municipality}}<br>
                    Ciudad: {{$audits->certification->certification->directionCertification->city}}<br>
                    Estado:{{$audits->certification->certification->directionCertification->state}}<br>
                </td>
            
                <td style="background-color: lightgray;">Responsable del Sistema de Gestión de Calidad:</td>
                <td>{{$status[0]->user->name}} {{$status[0]->user->lastname}} {{$status[0]->user->secondname}}</td>
            </tr>
            <tr>
                <td style="background-color: lightgray;">Teléfono:</td>
                <td>{{$status[0]->user->cellphone}}</td>
            
                <td style="background-color: lightgray;">Correo:</td>
                <td>{{$status[0]->user->email}}</td>
            </tr>    
            <tr>
                <td style="background-color: lightgray;">No de empleados:</td>
                <td>{{$audits->certification->certification->employees}}</td>
            
                <td style="background-color: lightgray;">No de sitios:</td>
                <td>{{$audits->certification->certification->sites}}</td>
            </tr>             
            <tr>
                <td style="background-color: lightgray;">Alcance:</td>
                <td>{{$audits->auditsector[0]->sector->name}}</td>
            
                <td style="background-color: lightgray;">Estándar:</td>
                <td>{{$audits->auditsector[0]->norm->name}}</td>
            </tr>     
            <tr>
                <td style="background-color: lightgray;">Exclusiones:</td>
                <td>{{$status[0]->exclusions}}</td>
            
                <td style="background-color: lightgray;">Código EAC/IAF***:</td>
                <td>{{$status[0]->code}}</td>
            </tr>
            <tr>
                <td style="background-color: lightgray;">Fecha de inicio:</td>
                <td>{{$audits->init_date}}</td>
            
                <td style="background-color: lightgray;">Fecha de término:</td>
                <td>{{$audits->finish_date}}</td>
            </tr> 
            <tr>
                <td style="background-color: lightgray;">Auditor Líder:</td>
                <td>
                    @foreach($audits->teams as $ad)
                        @if($ad->function=="Auditor lider")
                            {{$ad->user->name}} {{$ad->user->lastname}} {{$ad->user->secondname}}
                        @endif
                    @endforeach
                </td>
            
                <td style="background-color: lightgray;">Auditor/ Experto técnico:</td>
                <td>
                    @foreach($audits->teams as $ad)
                        @if($ad->function=="Auditor tecnico")
                            {{$ad->user->name}} {{$ad->user->lastname}} {{$ad->user->secondname}}
                        @endif
                    @endforeach
                </td>
            </tr>
        </table>

        <br>

        <table width="100%" class="table-title">
            <tr>
                <td><h1 class="title">2. Estado del Sistema de Gestión de Calidad</h1></td>
            </tr>
        </table>

        <br/>

        @foreach($audits->auditsector as $sectors)
            @if($sectors->id==$idvalidation)
                <table width="100%">
                    <tr>
                        <th style="background-color: lightgray;">Aspecto a evaluar:</th>
                        <th style="background-color: lightgray;">Documentación del SG:</th>
                        <th style="background-color: lightgray;">Evaluación del auditor:</th>
                    </tr>
                    <tbody>
                        @foreach($audits->sections as $ad)
                            @if($ad->audit_sector_norm_id==$sectors->id)
                                @foreach($ad->resultChecklist as $checklist)
                                    <tr>
                                        <td>{{$checklist->code}}.{{$checklist->order}} {{$checklist->description}}</td>
                                        <td>{{$checklist->observation}}</td>
                                        <td style="text-align:center;">
                                            <strong>
                                                @if($checklist->conform=="C")
                                                    <a style="color:green ">C</a>
                                                @elseif($checklist->conform=="NC")
                                                    <a style="color:red">NC</a>
                                                @else
                                                    <a style="color:blue">NA</a>
                                                @endif
                                            </strong>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        @endforeach
                    </tbody>
                </table>
            @endif  
        @endforeach

        <div class="page-break"></div>

        <table width="100%" class="table-title">
            <tr>
                <td><h1 class="title">3. No conformidades potenciales para la auditoria</h1></td>
            </tr>
        </table>

        <br/>

        <table width="100%">
            <tr>
                <th style="background-color: lightgray;">Requisito:</th>
                <th style="background-color: lightgray;">Descripción de la no conformidad:</th>
                <th style="background-color: lightgray;">Tipo de NC:</th>
            </tr>
            <tbody>
                @foreach($status[0]->conform as $conform)
                    <tr>
                        <td>{{$conform->code}}</td>
                        <td>{{$conform->description}}</td>
                        <td style="text-align:center;">
                            @if($conform->type_nc=="Mayor")
                                <a style="color:red">Mayor</a>
                            @elseif($conform->type_nc=="Menor")
                                <a style="color:green">Menor</a>
                            @else
                                <a style="color:blue">Critica</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>    

        <br>

        <table width="100%" class="">
            <tr>
                <td><h2 class="subtitle">Observaciones y oportunidades de mejora</h2></td>
            </tr>
        </table>

        <table width="100%">
            <tr>
                <th style="background-color: lightgray;">Seccion:</th>
                <th style="background-color: lightgray;">Observaciones:</th>
            </tr>
            <tbody>
                @foreach($resultadosection as $result)
                    <tr>
                        <td>{{$result->description}}</td> 
                        <td>{{$result->observation}}</td> 
                    </tr>
                @endforeach
            </tbody>
        </table>  

        <br>

        <table width="100%" class="table-title">
            <tr>
                <td><h1 class="title">4. Conclusión</h1></td>
            </tr>
        </table>

        <br/>

        <table width="100%">
            <tr>
                <td>
                    <strong>
                        ¿La empresa está preparada para la Auditoria de Certificación?
                    </strong>
                </td>
                @if($resultado=="Si")
                    <td style="text-align: center; color: white; background-color: green; font-size:18px">
                        {{$resultado}}
                    </td>
                @else
                    <td style="text-align: center; color: white; background-color: red; font-size:18px">
                        {{$resultado}}
                    </td>
                @endif
            </tr>
        </table>

        <br>

        <table width="100%" class="table-title">
            <tr>
                <td><h1 class="title">5. Información al Representante del SG sobre del proceso de Certificación</h1></td>
            </tr>
        </table>

        <br>

        <table width="100%">
            <tr>
                <td>
                    <ul>
                        <li>El seguimiento a los hallazgos aquí reportados es responsabilidad de la organización auditada.</li>
                        <li>Para la programación de la Etapa 2, se requiere un mínimo de 15 días y un máximo de 3 meses, partiendo de la fecha de terminación de esta Etapa 1.</li>
                        <li>Se debe enviar la documentación del SG indicada en el aviso de Auditoria al auditor líder asignado dos semanas antes de la fecha de auditoria de certificación Fase 2.</li>
                        <li>Si en esta evaluación, el auditor determina que existen razones para solicitar una modificación en el tiempo de auditoria, estas serán reportadas a la Gerencia Técnica de Mexcer para que les dé seguimiento antes de la 2da. Fase</li>
                        <li>Este reporte debe ser entregado al Representante de la Dirección al terminar la 1ra. Fase de la Certificación.</li>
                    </ul>
                </td>
            </tr>
        </table>

        <script type="text/php">
        if ( isset($pdf) ) {
            $pdf->page_script('
                $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                $pdf->text(520, 20, "Pág $PAGE_NUM de $PAGE_COUNT", $font, 6);
            ');
        }
        </script>

    </body>
</html>