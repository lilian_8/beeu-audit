<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="author" content="ThemePixels">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicons.png') }}">

    <title>Beeuaudit</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- <link href="{{ asset('lib/jqvmap/jqvmap.min.css') }}" rel="stylesheet"> -->

    <link href="{{ asset('lib/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('lib/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('lib/prismjs/themes/prism-vs.css') }}" rel="stylesheet" />
    <link href="{{ asset('lib/typicons.font/typicons.css') }}" rel="stylesheet" />
    <link href="{{ asset('lib/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/skin.cool.css') }}" rel="stylesheet" />
    <link href="{{ asset('lib/quill/quill.core.css') }}" rel="stylesheet" />
    <link href="{{ asset('lib/quill/quill.snow.css') }}" rel="stylesheet" />
    <link href="{{ asset('lib/quill/quill.bubble.css') }}" rel="stylesheet" />
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">



    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"> -->
    <!-- DashForge CSS -->
    <link href="{{ asset('assets/css/dashforge.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/dashforge.dashboard.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/dashforge.css">
    <link rel="stylesheet" href="../assets/css/dashforge.demo.css">
    <link rel="stylesheet" href="../assets/css/dashforge.filemgr.css">


    @yield('css')
    <style type="text/css">
      .fade {
        transition: opacity 0.4s linear !important;
        }
      .logo{
        width: 150px;
        height: 50px;
      }

    body{
      background-color: #f4f5f8;
    }

    .newbuttton{
      background-color: #2c5282;
    }


    </style>

  </head>
  <body>

    <aside class="aside aside-fixed">

      <div class="aside-header">
        <a href="{{route('home')}}" class="aside-logo"><img src="{{asset('logo_beeusoft.png')}}" class="logo"></a>
        <a href="" class="aside-menu-link">
          <i data-feather="menu"></i>
          <i data-feather="x"></i>
        </a>
      </div>

      <div class="aside-body">
        <div class="aside-loggedin">

          <div class="d-flex align-items-center justify-content-start">
            @if (!is_null(Auth::user()))
              <a href="" class="avatar"><img src="{{asset('/storage/'.Auth::user()->avatar)}}" class="rounded-circle" alt=""></a>
            @else
                <a href="" class="avatar"><img src="/assets/img/avatar-user.png" class="rounded-circle" alt=""></a>
            @endif
            <div class="aside-alert-link">
              <a href="" data-toggle="tooltip" title="Salir" href="{{ route('logout') }}" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();"><i data-feather="log-out"></i></a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
            </div>
          </div>

          <div class="aside-loggedin-user">
            <a href="#loggedinMenu" class="d-flex align-items-center justify-content-between mg-b-2" data-toggle="collapse">
                <h6 class="tx-semibold mg-b-0">{{ Auth::user()->name }} {{ Auth::user()->lastname }} {{ Auth::user()->secondname }}</h6>
              <i data-feather="chevron-down"></i>
            </a>
            <p class="tx-color-03 tx-12 mg-b-0">{{ Auth::user()->profile }}</p>
          </div>

          <div class="collapse" id="loggedinMenu">
            <ul class="nav nav-aside mg-b-0">
              <li class="nav-item"><a title="Salir" href="{{ route('logout') }}" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();" class="nav-link"><i data-feather="log-out"></i> <span>Salir</span></a></li>
            </ul>
          </div>

        </div><!-- aside-loggedin -->
        <ul class="nav nav-aside">
          <li class="nav-label">Catalogos</li>
          @if(in_array(Auth::user()->profile, ['Administrador','Comercial']))
            <li class="nav-item "><a href="{{route('users')}}" class="nav-link"><i data-feather="user"></i> <span>Usuarios</span></a></li>
          @endif

          @if(in_array(Auth::user()->profile, ['Administrador', 'Planeador']))
            <li class="nav-item "><a href="{{route('auditors')}}" class="nav-link"><i data-feather="users"></i> <span>Auditores</span></a></li>
          @endif
          
          @if(in_array(Auth::user()->profile, ['Administrador', 'Comercial','Planeador']))
            <li class="nav-item "><a href="{{route('organization')}}" class="nav-link"><i data-feather="briefcase"></i> <span>Organizaciones</span></a></li>
          @endif
          <br>
          
          <li class="nav-label">Operaciones</li>

          @if(in_array(Auth::user()->profile,['Administrador', 'Planeador','Comercial','Auditor','Registro','Tecnico']))
            <li class="nav-item "><a href="{{route('certification')}}" class="nav-link"><i data-feather="folder-plus"></i> <span>Certificación</span></a></li>
          @endif

          @if(in_array(Auth::user()->profile, ['Administrador', 'Planeador']))
            <li class="nav-item "><a href="{{route('certificates')}}" class="nav-link"><i data-feather="inbox"></i> <span>Certificados</span></a></li>
          @endif

          @if(in_array(Auth::user()->profile, ['Oculto']))
            <li class="nav-item "><a href="{{route('audit')}}" class="nav-link"><i data-feather="clipboard"></i> <span>Cotizaciones</span></a></li>
            <li class="nav-item "><a href="{{route('audit')}}" class="nav-link"><i data-feather="file-text"></i> <span>Facturación</span></a></li>
          @endif
        </ul>
      </div>
    </aside>

    <div class="content ht-100v pd-0">
      <div class="content-header">
        <nav class="nav">
          @if(in_array(Auth::user()->profile, ['Administrador']))
            <a href="{{route('company')}}" class="nav-link" style="position: absolute; right: 30px; top: 25px; font-size: 15px; "><i class="fas fa-cogs"></i>
             <strong>Ajustes de la empresa</strong>
            </a>
          @endif
        </nav>
      </div><!-- content-header -->
      <div class="content-body">
            @yield('content')
      </div>
    </div>

  </body>
    <script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('lib/feather-icons/feather.min.js')}}"></script>
    <!-- <script src="{{asset('lib/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script> -->
    <script src="{{asset('lib/js-cookie/js.cookie.js')}}"></script>
    <script src="{{asset('lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
    <script src="{{asset('lib/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script src="{{asset('lib/prismjs/prism.js')}}"></script>

    <!-- <script src="{{asset('asset/js/sweetalert.js')}}"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src="{{asset('lib/quill/quill.min.js')}}"></script>
    <script src="{{asset('lib/ckeditor/ckeditor.js') }}"></script>
    <script src="{{asset('lib/jquery-steps/build/jquery.steps.min.js') }}"></script>
    <script src="{{asset('lib/select2/js/select2.min.js') }}"></script>
    <script src="{{asset('lib/jqueryui/jquery-ui.min.js') }}"></script>
    <script src="{{asset('lib/select2/js/select2.min.js') }}"></script>

    <script src="{{asset('assets/js/dashforge.js')}}"></script>
    <script src="{{asset('assets/js/dashforge.settings.js')}}"></script>
    <script src="{{asset('assets/js/dashforge.aside.js')}}"></script>

    <script type="text/javascript">
      $(function(){
        'use strict'

        $('#secoresynormas').on('show.bs.modal', function (event) {
          var animation = $(event.relatedTarget).data('animation');
          $(this).addClass(animation);
        })

        $('#secoresynormas').on('hidden.bs.modal', function (e) {
          $(this).removeClass (function (index, className) {
              return (className.match (/(^|\s)effect-\S+/g) || []).join(' ');
          });
        });

        $('#secoresynormasedit').on('show.bs.modal', function (event) {
          var animation = $(event.relatedTarget).data('animation');
          $(this).addClass(animation);
        })

        $('#secoresynormasedit').on('hidden.bs.modal', function (e) {
          $(this).removeClass (function (index, className) {
              return (className.match (/(^|\s)effect-\S+/g) || []).join(' ');
          });
        });

        $('#secoresynormasdelete').on('show.bs.modal', function (event) {
          var animation = $(event.relatedTarget).data('animation');
          $(this).addClass(animation);
        })

        $('#secoresynormasdelete').on('hidden.bs.modal', function (e) {
          $(this).removeClass (function (index, className) {
              return (className.match (/(^|\s)effect-\S+/g) || []).join(' ');
          });
        });
        $('#new').on('show.bs.modal', function (event) {
          var animation = $(event.relatedTarget).data('animation');
          $(this).addClass(animation);
        })

        $('#new').on('hidden.bs.modal', function (e) {
          $(this).removeClass (function (index, className) {
              return (className.match (/(^|\s)effect-\S+/g) || []).join(' ');
          });
        });
        $('#delete').on('show.bs.modal', function (event) {
          var animation = $(event.relatedTarget).data('animation');
          $(this).addClass(animation);
        })

        $('#delete').on('hidden.bs.modal', function (e) {
          $(this).removeClass (function (index, className) {
              return (className.match (/(^|\s)effect-\S+/g) || []).join(' ');
          });
        });
        $('#job').on('show.bs.modal', function (event) {
          var animation = $(event.relatedTarget).data('animation');
          $(this).addClass(animation);
        })

        $('#job').on('hidden.bs.modal', function (e) {
          $(this).removeClass (function (index, className) {
              return (className.match (/(^|\s)effect-\S+/g) || []).join(' ');
          });
        });
        $('#start').on('show.bs.modal', function (event) {
          var animation = $(event.relatedTarget).data('animation');
          $(this).addClass(animation);
        })

        $('#start').on('hidden.bs.modal', function (e) {
          $(this).removeClass (function (index, className) {
              return (className.match (/(^|\s)effect-\S+/g) || []).join(' ');
          });
        });

      });

      $(function(){

        // $('.select2').select2();

        'use strict'
        $("#table").DataTable({
            responsive: true,
            ordering: false,
            info: true,
            autoWidth: true,
            autoWidth: false,
            language:{
              url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json",
            },
          });
            // $("#table_wrapper").addClass('form-group');
            // $("#paginate_button").addClass('btn btn-primary');

      });

        $( document ).ready(function() {
            $('body').addClass('active');
            $('#setFontRoboto').removeClass('active');
            $('.select2').select2({
              placeholder: 'Escoge uno',
              allowClear: true,
              searchInputPlaceholder: 'Search options'
            });

        });


          $(function(){
        'use strict'


        $('#accordion7').accordion({
          heightStyle: 'content'
        });

      });

      </script>

      @yield('scripts')
      @if(session()->has('dangerdelete'))
      <script>
        Swal.fire({
              title: "Datos no eliminados",
              text: "Algunos datos dependen de este registro",
              icon: "error",
              confirmButtonText: "Ok",
        });
      </script>
      @endif

      @if(session()->has('error'))
      <script>
        Swal.fire({
          title: "Hubo un error",
          text: "Error",
          icon: "error",
          confirmButtonText: "Ok",
        });
      </script>
      @endif
      @if(session()->has('success'))
      <script>
        Swal.fire({
              title: "Datos guardados",
              text: "Correctamente",
              icon: "success",
              confirmButtonText: "Ok",
        });
      </script>
      @endif
      @if(session()->has('updates'))
      <script>
        Swal.fire({
            title: "Datos actualizados",
            text: "Correctamente",
            icon: "success",
            confirmButtonText: "Ok",
        });
      </script>
      @endif
      @if(session()->has('delete'))
      <script>
        Swal.fire({
          title: "Datos eliminados",
          text: "Correctamente",
          icon: "success",
          confirmButtonText: "Ok",
        });
      </script>
      @endif
      @if(session()->has('erroruser'))
        <script>
          Swal.fire({
            title: "El usuario no se puede eliminar",
            text: "Debido a que tiene relacion con otras tablas, su estatus cambio a inactivo",
            icon: "error",
            confirmButtonText: "Ok",
          });
        </script>
      @endif
      @if(session()->has('auditend'))
        <script>
          Swal.fire({
            title: "Auditoria finalizada",
            text: "La auditoria finalizo correctamente",
            icon: "success",
            confirmButtonText: "Ok",
          });
        </script>
      @endif
      @if(session()->has('permisos'))
        <script>
          Swal.fire({
            title: "Error de permisos",
            text: "No tienes permisos para ingrear a esta opción",
            icon: "error",
            confirmButtonText: "Ok",
          });
        </script>
      @endif
      @if(session()->has('sectornorm'))
        <script>
          Swal.fire({
            title: "Error al eliminar",
            text: "El registro ya esta siendo utilizado en el sistema y no se puede eliminar",
            icon: "error",
            confirmButtonText: "Ok",
          });
        </script>
      @endif
      @if(session()->has('validationsectornorm'))
        <script>
          Swal.fire({
            title: "Error al agregar",
            text: "El sector y norma ya estan agregados, solo se agregaran los datos que no esten repetidos",
            icon: "error",
            confirmButtonText: "Ok",
          });
        </script>
      @endif

</html>
