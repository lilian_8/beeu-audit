<div class="modal fade bd-example-modal-lg" id="new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nueva organización</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form class="form-horizontal" method="post" action="{{ route('saveorganization') }}">
      {{ csrf_field() }}
        <div class="modal-body pd-sm-t-30 pd-sm-b-40 pd-sm-x-30" style="height: 600px; overflow-y: scroll;">
          
          <ul class="nav nav-tabs nav-justified" id="myTab3" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="home-tab3" data-toggle="tab" href="#home3" role="tab" aria-controls="home" aria-selected="true">Datos Generales</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="profile-tab3" data-toggle="tab" href="#sectores" role="tab" aria-controls="profile" aria-selected="false">Sectores </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="profile-tab3" data-toggle="tab" href="#normas" role="tab" aria-controls="profile" aria-selected="false">Normas</a>
            </li>
          </ul>

          <div class="tab-content bd bd-gray-300 bd-t-0 pd-20" id="myTabContent3">
            <div class="tab-pane fade show active" id="home3" role="tabpanel" aria-labelledby="profile-tab3">
              <div class="divider-text " style="color:#2c5282"><h6>Organizacion</h6></div>
                
                <div class="row row-sm">
                  <div class="col-sm">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CÓDIGO</label>
                    <input type="text" class="form-control" placeholder="CÓDIGO" name="codigo" required="required" value="{{ old('codigo') }}">
                  </div>

                  <div class="col-sm">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NOMBRE</label>
                    <input type="text" class="form-control" placeholder="NOMBRE" name="nombre" required="required" value="{{ old('nombre') }}">
                  </div>
                  <div class="row row-sm">
                    <div class="col-sm">
                      <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">RFC</label>
                      <input type="text" class="form-control" placeholder="RFC" name="rfc"  required="required" value="{{ old('rfc') }}">
                    </div>
                  </div>
                </div>

                <br>

                <div class="row row-sm">
                  
                  <div class="col-sm">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NO. EMPLEADOS</label>
                    <input type="text" class="form-control" placeholder="NO. EMPLEADOS" name="sitios" required="required" value="{{ old('sitios') }}">
                  </div>

                  <div class="row row-sm">
                        <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NO. DE SITIOS</label>
                        <input type="text" class="form-control" placeholder="NO. DE SITIOS" name="empleados" required="required" value="{{ old('empleados') }}">
                  </div>                            
                </div>

            <br>

            <div class="divider-text" style="color:#2c5282"><h6>Ubicación</h6></div>
              
              <div class="row row-sm">
                <div class="col-sm">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CALLE</label>
                  <input type="text" class="form-control" placeholder="CALLE" name="calle"   id="calle" required="required" value="{{ old('calle') }}">
                </div>
                <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NUM:EXTERIOR</label>
                  <input type="text" class="form-control" placeholder="NUM:EXTERIOR" name="exterior"   id="exterior" required="required" value="{{ old('exterior') }}">
                </div>
                <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NUM:INTERIOR</label>
                    <input type="text" class="form-control" placeholder="NUM:INTERIOR" name="interior"   id="interior" value="{{ old('interior') }}">
                  </div>
                  <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CP</label>
                    <input type="text" class="form-control" placeholder="CP" name="cp"   id="cp" required="required" value="{{ old('cp') }}">
                  </div>
              </div>

              <br>

                <div class="row row-sm">
                  <div class="col-sm">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">COLONIA</label>
                    <input type="text" class="form-control" placeholder="COLONIA" name="colonia"   id="colonia" required="required" value="{{ old('colonia') }}">
                  </div>
                  <div class="col-sm-3 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CIUDAD</label>
                    <input type="text" class="form-control" placeholder="CIUDAD" name="ciudad"   id="ciudad"required="required" value="{{ old('ciudad') }}">
                  </div>
                </div>
                <br>
                <div class="row row-sm">
                  <div class="col-sm-4 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">ESTADO</label>
                    <input type="text" class="form-control" placeholder="ESTADO" name="estado"   id="estado" required="required" value="{{ old('estado') }}">
                  </div>
                  <div class="col-sm-4">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">MUNICIPIO</label>
                    <input type="text" class="form-control" placeholder="MUNICIPIO" name="municipio"   id="municipio" required="required" value="{{ old('municipio') }}">
                  </div>
                  <div class="col-sm-4 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">PAIS</label>
                    <input type="text" class="form-control" placeholder="PAIS" name="pais" value="México" id="pais" required="required" value="{{ old('pais') }}">
                  </div>
                </div>

              <br>

                <div class="row row-sm">
                  <div class="col-sm-12 mg-t-20 mg-sm-t-0">
                      <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">REFERENCIAS</label>
                      <textarea class="form-control" rows="2" name="reference" id="reference" placeholder="REFERENCIAS" value="{{ old('reference') }}"></textarea>
                  </div>
                </div>

              <br>
              
                <div class="col-sm-12 mg-t-20 mg-sm-t-0" >
                  <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="invoice" name="invoice" value="1">
                    <label class="custom-control-label" for="invoice">El Domicilio es el mismo a facturar</label>
                  </div>          
                </div>

              <br>
            <div class="divider-text" style="color:#2c5282"><h6>Domicilio Fiscal</h6></div>
              <div class="row row-sm">
                <div class="col-sm">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CALLE</label>
                  <input type="text" class="form-control" placeholder="CALLE" name="steets"   id="streets" value="{{ old('steets') }}">
                </div>
                <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NUM:EXTERIOR</label>
                  <input type="text" class="form-control" placeholder="NUM:EXTERIOR" name="numberOuts"   id="numberOuts" value="{{ old('numberOuts') }}">
                </div>
                <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">NUM:INTERIOR</label>
                    <input type="text" class="form-control" placeholder="NUM:INTERIOR" name="numberInts"   id="numberInts" value="{{ old('numberInts') }}">
                  </div>
                  <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CP</label>
                    <input type="text" class="form-control" placeholder="CP" name="cps"   id="cps" value="{{ old('cps') }}">
                  </div>
              </div>
              <br>
              <div class="row row-sm">

                <div class="col-sm">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">COLONIA</label>
                  <input type="text" class="form-control" placeholder="COLONIA" name="suburbs"   id="suburbs" value="{{ old('suburbs') }}">
                </div>

                <div class="col-sm-6 mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CIUDAD</label>
                  <input type="text" class="form-control" placeholder="CIUDAD" name="citys"   id="citys" value="{{ old('citys') }}">
                </div>

                </div>
                <br>
                <div class="row row-sm">

                <div class="col-sm-4 mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">ESTADO</label>
                  <input type="text" class="form-control" placeholder="ESTADO" name="states"   id="states" value="{{ old('states') }}">
                </div>

                <div class="col-sm-4">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">MUNICIPIO</label>
                  <input type="text" class="form-control" placeholder="MUNICIPIO" name="municipalitys"   id="municipalitys" value="{{ old('municipalitys') }}">
                </div>

                <div class="col-sm-4 mg-t-20 mg-sm-t-0">
                  <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">PAIS</label>
                  <input type="text" class="form-control" placeholder="PAIS" name="countrys" value="México" id="countrys" value="{{ old('countrys') }}">
                </div>

              </div>
              <br>

              <div class="row row-sm">
                  <div class="col-sm-12 mg-t-20 mg-sm-t-0">
                      <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">REFERENCIAS</label>
                      <textarea class="form-control" rows="2" name="references" id="references" placeholder="REFERENCIAS" value="{{ old('references') }}"></textarea>
                  </div>
              </div>          

              <div class="divider-text " style="color:#2c5282"><h6>Contacto</h6></div>

              <div class="row row-sm">
                  <div class="col-sm">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">A CARGO</label>
                    <input type="text" class="form-control" placeholder="A CARGO" name="contacto"   value="{{ old('contacto') }}" required="required">
                  </div>
                  <div class="col-sm-2 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">PUESTO</label>
                    <input type="text" class="form-control" placeholder="PUESTO" name="puesto"   value="{{ old('puesto') }}" required="required">
                  </div>
                  <div class="col-sm-3 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">CORREO</label>
                    <input type="text" class="form-control" placeholder="CORREO" name="email"   value="{{ old('email') }}" required="required">
                  </div>
                  <div class="col-sm-1 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">EXTENSION</label>
                    <input type="text" class="form-control" placeholder="EXTENSION" name="extension"   value="{{ old('extension') }}">
                  </div>
                  <div class="col-sm-3 mg-t-20 mg-sm-t-0">
                    <label class="tx-10 tx-uppercase tx-medium tx-spacing-1 mg-b-5 tx-color-03">TEL MÓVIL</label>
                    <input type="text" class="form-control" placeholder="CELULAR" name="celular"   value="{{ old('celular') }}" required="required">
                  </div>
              </div>

          </div>
          
          <div class="tab-pane fade" id="sectores" role="tabpanel" aria-labelledby="profile-tab3">
            <h6>Sectores </h6>
            <div class="modal-body" >
              <div class="tx-13 mg-b-25 ">
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col"></th>
                      <th scope="col">Sector</th>
                      <th scope="col">Habilitar</th>
                    </tr>
                  </thead>
                  <tbody id="secnor">
                   @foreach($sectors as $companysector)
                    <tr>
                      <td>{{$companysector->id}}</td>
                      <td>{{$companysector->name}}</td>
                      <td> 
                        <div class="custom-control custom-switch">
                          <input type="checkbox" class="custom-control-input" id="sector{{$companysector->id}}" value="{{$companysector->id}}" name="sector[]">
                          <label class="custom-control-label" for="sector{{$companysector->id}}"></label>
                        </div>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="tab-pane fade" id="normas" role="tabpanel" aria-labelledby="profile-tab3">
            <h6>Normas</h6>
            <!-- <div id="select_sectores"></div> -->
            
            <div class="modal-body" >
              <div class="tx-13 mg-b-25 ">
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Norma</th>
                      <th scope="col">Habilitar</th>
                    </tr>
                  </thead>
                  <tbody id="secnor">
                   @foreach($norms as $companysector)
                    <tr>
                      <td>{{$companysector->name}}</td>
                      <td> 
                        <div class="custom-control custom-switch">
                          <input type="checkbox" class="custom-control-input" id="norma{{$companysector->id}}" value="{{$companysector->id}}" name="norma[]">
                          <label class="custom-control-label" for="norma{{$companysector->id}}"></label>
                        </div>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="modal-footer pd-x-20 pd-y-15">
        <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fas fa-undo"></i> Cancelar</button>
        <button type="submit" class="btn btn-primary"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button>
      </div>
      
    </form>
    </div>
  </div>
</div>