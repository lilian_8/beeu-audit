<div class="modal fade" id="secoresynormasedit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Asignacion de normas a sectores</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-horizontal" method="post" action="{{ route('editsector') }}">
      {{ csrf_field() }}
      
      <div class="modal-body">
        
        <div class="tx-13 mg-b-25">
            <div class="form-group">
                <h5>Agregar sector</h5>
                <input type="hidden" name="id" id="id" >
              <select class="custom-select" required name="sectors" id="sector_id" >
                <option value="" disabled selected="">Selecciona un sector</option>
                @foreach($sectors as $sector)
                  <option value="{{$sector->id}}">{{$sector->name}}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <h5>Agregar norma</h5>
              <select class="custom-select" required name="norms" id="norm_id">
                <option value="" disabled selected="">Selecciona una norma</option>
                @foreach($norms as $norm)
                  <option value="{{$norm->id}}">{{$norm->name}}</option>
                @endforeach
              </select>
            </div>

        </div>

      </div>

      <div class="modal-footer pd-x-20 pd-y-15">
        <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fas fa-undo"></i> Cancelar</button>
        <button type="submit" class="btn btn-primary"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Guardar</button>
      </div>

    </form>
    </div>
  </div>
</div>