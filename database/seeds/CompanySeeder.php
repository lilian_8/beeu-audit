<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = DB::table('companies')->insert([
			'name'              => '--',
			'website'           => '--',
			'rfc'               => '--',
			'acreditation_body' => '--',
			'avatar'            => 'company/avatar.png'
        ]);

        DB::table('directions')->insert([
			'street'                  =>'--',
			'internal_number'         =>'--',
			'external_number'         =>'--',
			'zip_code'                =>'--',
			'suburb'                  =>'--',
			'city'                    =>'--',
			'state'                   =>'--',
			'municipality'            =>'--',
			'country'                 =>'--',
			'reference'               =>'--',
			'phone'                   =>'--',
			'extension'               =>'--',
			'email'                   =>'--',
			'contact_name'            =>'--',
			'contact_job'             =>'--',
			'company_organization_id' =>'1',
			'type'                    =>'Company'
        ]);

    }
}
