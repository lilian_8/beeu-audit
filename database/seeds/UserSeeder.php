<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = DB::table('users')->insert([
			'name'       => '---',
			'lastname'   => '---',
			'secondname' => '---',
			'email'      => 'soporte@beeusoft.mx',
			'password'   => bcrypt('Admin123'),
			'avatar'     => 'users/avatar.png',
			'profile'    => 'Administrador',
			'cellphone'  => '---',
			'code'       => '---',
			'status'     => 'Activo',
        ]);
    }
}
