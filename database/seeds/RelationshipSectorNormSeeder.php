<?php

use Illuminate\Database\Seeder;
use App\Sector;
use App\Norm;
use App\CompanySector;
use App\Classes\Selection;

class RelationshipSectorNormSeeder extends Seeder
{
    public function run()
    {
        $sectors = Sector::all();
        $norms   = Norm::all();

		for ($i = 0; $i < count($sectors); $i++) {
            for ($c = 0; $c < count($norms); $c++) {
                $companysector=CompanySector::create([
						'sector_id'    => $sectors[$i]->id,
						'norm_id'      => $norms[$c]->id,
						'companies_id' => 1,
                ]);

            	$norm = Norm::find($norms[$c]->id)->first();
            	if($norm->name=="ISO 9001:2015"){
            		$normId = '9';
            	}elseif($norm->name=="ISO 14001:2015"){
            		$normId = '14';
            	}elseif($norm->name=="ISO 22000:2005"){
            		$normId = '22';
            	}elseif($norm->name=="ISO 45001:2018"){
            		$normId = '45';
            	}

                $checklist = Selection::check($companysector->id,$normId);

            }
    	}

    }
}
