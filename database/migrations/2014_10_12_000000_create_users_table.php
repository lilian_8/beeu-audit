<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('lastname');
            $table->string('secondname');
            $table->string('email')->unique();
            $table->string('password');
            $table->enum('profile', ['Administrador', 'Auditor']);
            $table->string('avatar');
            $table->string('cellphone');
            $table->string('code');
            $table->enum('status', ['Activo', 'Inactivo','Suspendido']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
