<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditorSectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditor_sectors', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_sector_id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('company_sector_id')->references('id')->on('company_sectors');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auditor_sectors');
    }
}
