<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_checklists', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('audit_sector_norm_id');
            $table->unsignedBigInteger('audit_id');
            $table->enum('status', ['Activo', 'Cerrado','Reactivo'])->nullable();
            $table->foreign('audit_id')->references('id')->on('audits');
            $table->foreign('audit_sector_norm_id')->references('id')->on('audit_sector_norms');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_checklists');
    }
}
