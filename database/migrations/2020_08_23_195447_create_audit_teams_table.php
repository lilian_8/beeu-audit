<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_teams', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('audit_id');
            $table->enum('function', ['Auditor lider', 'Auditor','Auditor tecnico', 'Traductor','Aprendiz','Testigo']);
            $table->date('date')->nullable();
            $table->string('hour')->nullable();
            $table->string('minute')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('audit_id')->references('id')->on('audits');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_teams');
    }
}
