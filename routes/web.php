<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

Route::get('/', function () {
    return view('home');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/company', 'CompanyController@index')->name('company');
Route::put('/company/{id}', 'CompanyController@update')->name('company.update');
Route::post('/addsector', 'CompanyController@store')->name('addsector');
Route::post('/editsector', 'CompanyController@editSector')->name('editsector');
Route::post('/deletesector', 'CompanyController@deleteSector')->name('deletesector');

Route::get('/users', 'UserController@index')->name('users');
Route::post('/saveuser',['uses' => 'UserController@store','as' => 'saveuser']);
Route::get('/edituser/{id}', 'UserController@edit')->name('user.edit');
Route::post('/updateuser/{id}', 'UserController@update')->name('user.update');
Route::delete('/deleteuser', 'UserController@destroyd')->name('user.delete');

Route::get('/auditors', 'AuditorsController@index')->name('auditors');
Route::post('/saveauditors', 'AuditorsController@store')->name('saveauditors');
Route::post('/savefilesevaluation', 'AuditorsController@saveFilesEvaluation')->name('savefilesevaluation');
Route::post('/savefilesevaluationcliente', 'AuditorsController@saveFilesEvaluationCliente')->name('savefilesevaluationcliente');
Route::post('/savecourse', 'AuditorsController@saveCourse')->name('savecourse');
Route::post('/savefilecourse', 'AuditorsController@saveFilecourse')->name('savefilecourse');
Route::get('/editauditor/{id}', 'AuditorsController@edit')->name('auditors.edit');
Route::post('/jobupdate/{id}', 'AuditorsController@jobUpdate')->name('auditors.jobupdate');
Route::post('/deletedsectorauditor', 'AuditorsController@deleteSector')->name('deletedsectorauditor');
Route::post('/deletednormauditor', 'AuditorsController@deleteNorm')->name('deletednormauditor');
Route::post('/updatesector', 'AuditorsController@sectorUpdate')->name('updatesector');
Route::post('/updatenorm', 'AuditorsController@normUpdate')->name('updatenorm');

Route::post('/deletecourse', 'AuditorsController@deleteCourse')->name('deletecourse');
Route::post('/deletehistory', 'AuditorsController@deleteHistory')->name('deletehistory');
Route::post('/savehistory', 'AuditorsController@saveHistory')->name('savehistory');

Route::post('/deletejobdetail', 'AuditorsController@deleteJobDetail')->name('deletejobdetail');
Route::post('/newdetailjob', 'AuditorsController@addJobDetail')->name('newdetailjob');



Route::get('/organization', 'OrganizationController@index')->name('organization');
Route::post('/saveorganization', 'OrganizationController@store')->name('saveorganization');
Route::get('/editorganization/{id}', 'OrganizationController@edit')->name('editorganization.edit');

Route::post('/updatesectororganization', 'OrganizationController@sectorUpdate')->name('updatesectororganization');
Route::post('/updatenormorganization', 'OrganizationController@normUpdate')->name('updatenormorganization');

Route::post('/updateorganization', 'OrganizationController@update')->name('updateorganization');
Route::post('/deletesectororganization', 'OrganizationController@deleteSectorOrganization')->name('deletesectororganization');
Route::post('/deletenormorganization', 'OrganizationController@deleteNormOrganization')->name('deletenormorganization');
Route::post('/deleteorganization', 'OrganizationController@delete')->name('deleteorganization');

Route::get('/certification', 'CertificationController@index')->name('certification');
Route::post('/changeStatus', 'CertificationController@changeStatus')->name('changeStatus');
Route::post('/deletecertification', 'CertificationController@delete')->name('deletecertification');
Route::post('/savecertification', 'CertificationController@store')->name('savecertification');
Route::post('/changesection', 'CertificationController@changeSection')->name('changesection');
Route::post('/saveaudit', 'CertificationController@saveAudit')->name('saveaudit');
Route::post('/saveauditrestart', 'CertificationController@saveAuditRestart')->name('saveauditrestart');
Route::get('/progress/{id}', 'CertificationController@progress')->name('progress');
Route::get('/auditdetails/{id}', 'CertificationController@auditDetails')->name('auditdetails');
Route::post('/saveteam', 'CertificationController@saveTeam')->name('saveteam');
Route::post('/deleteteam', 'CertificationController@deleteTeam')->name('deleteteam');
Route::post('/newaudit', 'CertificationController@endAndStarAuditori')->name('newaudit');
Route::post('/endaudituser', 'CertificationController@endAudit')->name('endaudituser');
Route::post('/generateFile', 'CertificationController@generateFile')->name('generateFile');
Route::post('/endchecklist', 'CertificationController@endChecklist')->name('endchecklist');
Route::post('/savenoconform', 'CertificationController@saveNoConform')->name('savenoconform');
Route::post('/editcertification', 'CertificationController@editCertification')->name('editcertification');

Route::get('/certificates', 'CertificationController@certificates')->name('certificates');
Route::post('/editcertification', 'CertificationController@editCertificationfile')->name('editcertification');


Route::post('/savefilecertificate', 'CertificationController@saveCertificateFile')->name('savefilecertificate');
Route::post('/saveexpedient', 'CertificationController@saveFileExpedient')->name('saveexpedient');
Route::post('/endexpedient', 'CertificationController@endExpedient')->name('endexpedient');


Route::post('/file', 'CertificationController@exportFile')->name('file');
Route::post('/result', 'CertificationController@result')->name('result');



Route::get('/storageLink', function(){
  Artisan::call('storage:link');
});




Route::get('/audit', 'AuditContronller@index')->name('audit');

});








